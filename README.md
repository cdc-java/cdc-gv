# cdc-gv

Strongly typed API dedicated to generation of Graphviz files.
This API makes writing Graphviz files much easier.