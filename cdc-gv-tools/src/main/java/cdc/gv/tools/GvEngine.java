package cdc.gv.tools;

import cdc.util.cli.OptionEnum;

/**
 * Enumeration of GraphViz layout engines.
 *
 * @author Damien Carbonne
 *
 */
public enum GvEngine implements OptionEnum {
    CIRCO,
    DOT,
    FDP,
    NEATO,
    SFDP,
    TWOPI;

    @Override
    public String getName() {
        return "engine-" + name().toLowerCase();
    }

    public String getEngineName() {
        return name().toLowerCase();
    }

    @Override
    public String getDescription() {
        return "Use " + name().toLowerCase() + " layout engine.";
    }
}