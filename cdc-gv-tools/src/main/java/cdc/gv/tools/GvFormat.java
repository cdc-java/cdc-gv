package cdc.gv.tools;

import cdc.util.cli.OptionEnum;

/**
 * Enumeration of supported output files types.
 *
 * @author Damien Carbonne
 *
 */
public enum GvFormat implements OptionEnum {
    // TODO Add other formats
    /** Windows BMP format */
    BMP,
    CANON,
    DOT,
    GV,
    XDOT,
    /** Encapsulated PostScript */
    EPS,
    /** OpenEXR format */
    EXR,
    /** FIG Graphic language */
    FIG,
    JSON,
    JSON0,
    DOT_JSON,
    XDOT_JSON,
    ICO,
    /** Server side image map. */
    IMAP,
    /** Client side image map */
    CMAPX,
    /** JPEG 2000 format */
    JP2,
    /** GIF bitmap images. */
    GIF,
    /** JPEG format */
    JPG,
    PDF,
    /** Text-based PIC language developed for troff */
    PIC,
    PLAIN,
    PLAIN_EXT,
    /** Portable Network Graphics */
    PNG,
    /** PostScript */
    PS,
    /** PostScript output with PDF notations */
    PS2,
    /** Adobe PhotoShop PSD */
    PSD,
    SVG,
    /** Compressed SVG */
    SVGZ,
    TGA,
    TIFF;

    @Override
    public String getName() {
        return "format-" + getFormatName();
    }

    public String getFormatName() {
        if (this == PLAIN_EXT) {
            return "plain-ext";
        } else {
            return name().toLowerCase();
        }
    }

    @Override
    public String getDescription() {
        return "Generate " + getFormatName() + " file(s).";
    }
}