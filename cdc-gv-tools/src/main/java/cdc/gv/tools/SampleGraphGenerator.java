package cdc.gv.tools;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.gv.GvWriter;

public class SampleGraphGenerator {
    private static final Logger LOGGER = LogManager.getLogger(SampleGraphGenerator.class);

    public static void main(String[] args) throws IOException {
        final File file = new File("target/sample.gv");
        LOGGER.info("Generate {}", file);
        final int N1 = 100;
        final int N2 = 100;

        try (final GvWriter writer = new GvWriter(file)) {
            writer.beginGraph("Sample", true, null);
            for (int index1 = 0; index1 < N1; index1++) {
                for (int index2 = 0; index2 < N2; index2++) {
                    writer.addEdge("s" + index1, "t" + index2, null);
                }
            }
            writer.endGraph();
        }
        LOGGER.info("DONE");
    }
}