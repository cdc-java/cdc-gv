package cdc.gv;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.gv.support.GvSupport;

class GvSupportTest {
    @Test
    void testIsBasic() {
        assertFalse(GvSupport.isBasic(null));
        assertFalse(GvSupport.isBasic(""));
        assertTrue(GvSupport.isBasic("a"));
        assertTrue(GvSupport.isBasic("abc__"));
        assertTrue(GvSupport.isBasic("_"));
        assertTrue(GvSupport.isBasic("_a"));
        assertTrue(GvSupport.isBasic("_0"));
        assertFalse(GvSupport.isBasic("0"));
        assertFalse(GvSupport.isBasic("-"));
    }

    @Test
    void testIsNumeral() {
        assertFalse(GvSupport.isNumeral(null));
        assertFalse(GvSupport.isNumeral(""));
        assertFalse(GvSupport.isNumeral("a"));
        assertTrue(GvSupport.isNumeral("0"));
        assertTrue(GvSupport.isNumeral("1"));
        assertTrue(GvSupport.isNumeral("123"));
        assertTrue(GvSupport.isNumeral("123.1"));
        assertTrue(GvSupport.isNumeral("-123.1"));
        assertFalse(GvSupport.isNumeral("12 3"));
        assertFalse(GvSupport.isNumeral("-123."));
        assertFalse(GvSupport.isNumeral("-"));
        assertFalse(GvSupport.isNumeral("."));
        assertFalse(GvSupport.isNumeral("-."));
        assertFalse(GvSupport.isNumeral("-1."));
    }

    @Test
    void testIsDoubleQuoted() {
        assertFalse(GvSupport.isDoubleQuoted(null));
        assertFalse(GvSupport.isDoubleQuoted(""));
        assertTrue(GvSupport.isDoubleQuoted("\"a\""));
        assertTrue(GvSupport.isDoubleQuoted("\"aa\""));
        assertTrue(GvSupport.isDoubleQuoted("\"aaa\""));
        assertFalse(GvSupport.isDoubleQuoted("\"aaa"));
        assertFalse(GvSupport.isDoubleQuoted("aaa\""));
        assertTrue(GvSupport.isDoubleQuoted("\"a\\\"aa\""));
        assertFalse(GvSupport.isDoubleQuoted("\"a\"aa\""));
    }
}