package cdc.gv;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.gv.labels.GvHtmlLabel;
import cdc.gv.labels.GvTextModifier;
import cdc.util.lang.InvalidStateException;

class GvHtmlLabelTest {
    @Test
    void testTextInLabel() {
        final GvHtmlLabel label = new GvHtmlLabel();
        label.addText("Hello");
        assertEquals("<Hello>", label.toString());
    }

    @Test
    void testInvalidBeginInLabel() {
        final GvHtmlLabel label = new GvHtmlLabel();
        assertThrows(InvalidStateException.class, () -> {
            label.beginRow();
        });

        assertThrows(InvalidStateException.class, () -> {
            label.beginCell(null);
        });
    }

    @Test
    void testFontInLabel() {
        final GvHtmlLabel label = new GvHtmlLabel();
        label.beginFont("black", "Arial", 10);
        label.addText("Hello");
        label.endFont();
        assertEquals("<<font color=\"black\" face=\"Arial\" point-size=\"10\">Hello</font>>", label.toString());
    }

    @Test
    void testTextModifierInLabel() {
        final GvHtmlLabel label = new GvHtmlLabel();
        label.beginTextModifiers(GvTextModifier.BOLD,
                                 GvTextModifier.ITALIC,
                                 GvTextModifier.OVERLINE,
                                 GvTextModifier.STRIKE_THROUGH);
        label.addText("Hello");
        label.endTextModifiers();
        assertEquals("<<b><i><o><s>Hello</s></o></i></b>>", label.toString());
    }

    @Test
    void testTableInLabel() {
        final GvHtmlLabel label = new GvHtmlLabel();
        label.beginTable(null);
        label.beginRow();
        label.beginCell(null);
        label.addText("Hello");
        label.endCell();
        label.endRow();
        label.endTable();
        assertEquals("<<table><tr><td>Hello</td></tr></table>>", label.toString());
    }

    @Test
    void testFontInCell() {
        final GvHtmlLabel label = new GvHtmlLabel();
        label.beginTable();
        label.beginRow();
        label.beginCell();
        label.beginFont("black", null, -1);
        label.addText("Hello");
        label.endFont();
        label.endCell();
        label.endRow();
        label.endTable();
        assertEquals("<<table><tr><td><font color=\"black\">Hello</font></td></tr></table>>", label.toString());
    }

    @Test
    void testTableInFont() {
        final GvHtmlLabel label = new GvHtmlLabel();
        label.beginFont("black", null, -1);
        label.beginTable();
        label.beginRow();
        label.beginCell();
        label.addText("Hello");
        label.endCell();
        label.endRow();
        label.endTable();
        label.endFont();
        assertEquals("<<font color=\"black\"><table><tr><td>Hello</td></tr></table></font>>", label.toString());
    }
}