package cdc.gv.labels;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible Table or Cell styles.
 *
 * @author Damien Carbonne
 *
 */
public enum GvStyle implements GvEncodable {
    /** Cells. */
    RADIAL,
    /** Tables and Cells. */
    ROUNDED;

    @Override
    public String encode() {
        return name();
    }
}