package cdc.gv.labels;

public final class GvTableAttributes extends GvAttributes<GvTableAttributes> {
    public GvTableAttributes() {
        super(GvAttributeUsage.TABLE);
    }

    @Override
    protected GvTableAttributes self() {
        return this;
    }

    @Override
    protected boolean isSupported(GvAttributeName name,
                                  GvAttributeUsage usage) {
        return name.isSupportedFor(usage);
    }

    @Override
    public GvTableAttributes setCellBorder(int value) {
        return super.setCellBorder(value);
    }

    @Override
    public GvTableAttributes setColumns(int value) {
        return super.setColumns(value);
    }

    @Override
    public GvTableAttributes setRows(String value) {
        return super.setRows(value);
    }
}