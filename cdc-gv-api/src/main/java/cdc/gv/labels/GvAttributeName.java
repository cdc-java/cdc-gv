package cdc.gv.labels;

import static cdc.gv.labels.GvAttributeUsage.C;
import static cdc.gv.labels.GvAttributeUsage.T;

import cdc.util.lang.IntMasks;

/**
 * Enumeration of HTML labels cell attributes.
 *
 * @author Damien Carbonne
 *
 */
public enum GvAttributeName {
    ALIGN(T, C),
    BALIGN(C),
    BG_COLOR(T, C),
    BORDER(T, C),
    CELL_BORDER(T),
    CELL_PADDING(T, C),
    CELL_SPACING(T, C),
    COLOR(T, C),
    COLUMNS(T),
    COL_SPAN(C),
    FIXED_SIZE(T, C),
    GRADIENT_ANGLE(T, C),
    HEIGHT(T, C),
    HREF(T, C),
    ID(T, C),
    PORT(T, C),
    ROWS(T),
    ROW_SPAN(C),
    SIDES(T, C),
    STYLE(T, C),
    TARGET(T, C),
    TITLE(T, C),
    TOOLTIP(T, C),
    VALIGN(T, C),
    WIDTH(T, C);

    private final String code;
    private final int usages;

    private GvAttributeName(GvAttributeUsage... usages) {
        code = name().replaceAll("_", "").toLowerCase();
        this.usages = IntMasks.toMask(usages);
    }

    /**
     * Returns whether the attribute can be used for a given usage.
     *
     * @param usage The tested usage.
     * @return whether the attribute can be used for usage.
     */
    public final boolean isSupportedFor(GvAttributeUsage usage) {
        return IntMasks.isEnabled(usages, usage);
    }

    public String encode() {
        return code;
    }
}