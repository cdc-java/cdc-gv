package cdc.gv.labels;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible Vertical Alignments.
 *
 * @author Damien Carbonne
 *
 */
public enum GvVAlign implements GvEncodable {
    TOP,
    MIDDLE,
    BOTTOM;

    @Override
    public String encode() {
        return name();
    }
}