package cdc.gv.labels;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible Horizontal Alignments.
 *
 * @author Damien Carbonne
 *
 */
public enum GvAlign implements GvEncodable {
    /** Tables and Cells. */
    LEFT,
    /** Tables and Cells. */
    CENTER,
    /** Tables and Cells. */
    RIGHT,
    /** Cells. */
    TEXT;

    @Override
    public String encode() {
        return name();
    }
}