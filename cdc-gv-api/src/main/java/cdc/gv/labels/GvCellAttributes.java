package cdc.gv.labels;

public final class GvCellAttributes extends GvAttributes<GvCellAttributes> {
    public GvCellAttributes() {
        super(GvAttributeUsage.CELL);
    }

    @Override
    protected GvCellAttributes self() {
        return this;
    }

    @Override
    protected boolean isSupported(GvAttributeName name,
                                  GvAttributeUsage usage) {
        return name.isSupportedFor(usage);
    }

    @Override
    public GvCellAttributes setBAlign(GvAlign value) {
        return super.setBAlign(value);
    }

    @Override
    public GvCellAttributes setColSpan(int value) {
        return super.setColSpan(value);
    }

    @Override
    public GvCellAttributes setRowSpan(int value) {
        return super.setRowSpan(value);
    }
}