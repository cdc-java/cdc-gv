package cdc.gv.labels;

import cdc.gv.colors.GvColor;
import cdc.gv.labels.GvHtmlLabelContext.Status;
import cdc.io.xml.XmlUtils;
import cdc.util.lang.InvalidStateException;

/**
 * Builder of HTML labels.
 *
 * <pre>{@code
 * label ::=
 *     text
 *   | table
 *
 * text ::=
 *     textitem
 *   | text textitem
 *
 * textitem ::=
 *     string
 *   | <BR/>
 *   | <FONT> text </FONT>
 *   | <I> text </I>
 *   | <B> text </B>
 *   | <U> text </U>
 *   | <O> text </O>
 *   | <SUB> text </SUB>
 *   | <SUP> text </SUP>
 *   | <S> text </S>
 *
 * table ::=
 *     [<FONT>] <TABLE> rows </TABLE> [</FONT>]
 *
 * rows ::=
 *     row
 *   | rows row
 *   | rows <HR/> row
 *
 * row ::=
 *     <TR> cells </TR>
 *
 * cells ::=
 *     cell
 *   | cells cell
 *   | cells <VR/> cell
 *
 * cell ::=
 *     <TD> label </TD>
 *   | <TD> <IMG/> </TD>
 * }</pre>
 *
 * @author Damien Carbonne
 */
public class GvHtmlLabel {
    private GvHtmlLabelContext context = new GvHtmlLabelContext();
    private final StringBuilder builder = new StringBuilder();

    // BOLD : <B>...</B>
    // ITALIC : <I>...</I>
    // UNDERLINE: <U>...</U>
    // Subscript: <SUB>...</SUB>
    // Superscript: <SUP>...</SUP>
    // Line break: <BR ALIGN="CENTER|LEFT|RIGHT"/>
    // Horizontal rule: <HR/>
    // Vertical rule: <VR/>
    // Strike through <S>...</S>
    // Font specification:
    // <FONT COLOR="..." FACE="..." // POINT-SIZE="...">...</FONT>
    // Image inclusion: <IMG SCALE="FALSE|TRUE|WIDTH|HEIGHT|BOTH" SRC="...">

    private void invalidState(String message) {
        throw new InvalidStateException("Invalid state: " + context.getStatus() + " (" + message + ")");
    }

    public GvHtmlLabel clear() {
        builder.setLength(0);
        context = context.getRoot();
        return this;
    }

    public GvHtmlLabel addText(String text) {
        switch (context.getStatus()) {
        case IN_LABEL:
        case IN_TEXT:
        case IN_CELL:
        case IN_FONT:
        case IN_TEXT_MODIFIER:
            XmlUtils.appendEscaped(builder,
                                   text,
                                   XmlUtils.Context.ELEMENT,
                                   XmlUtils.EscapingPolicy.ESCAPE_ALWAYS);
            if (context.getStatus() != Status.IN_TEXT) {
                context = context.pushContext(Status.IN_TEXT);
            }
            break;

        default:
            invalidState("addText");
            break;
        }
        return this;
    }

    /**
     *
     * @param align Specifies horizontal placement.<br>
     *            When an object is allocated more space than required, this
     *            value determines where the extra space is placed left and
     *            right of the object.
     *            <ul>
     *            <li>CENTER aligns the object in the center. (Default)</li>
     *            <li>LEFT aligns the object on the left.</li>
     *            <li>RIGHT aligns the object on the right.</li>
     *            </ul>
     * @return This object.
     */
    public GvHtmlLabel addLineBreak(GvAlign align) {
        switch (context.getStatus()) {
        case IN_LABEL:
        case IN_TEXT:
        case IN_CELL:
        case IN_FONT:
        case IN_TEXT_MODIFIER:
            builder.append("<br");
            if (align != null) {
                if (align == GvAlign.TEXT) {
                    assert false;
                } else {
                    builder.append(" align=\"" + align.encode() + "\"");
                }
            }
            builder.append("/>");
            if (context.getStatus() != Status.IN_TEXT) {
                context = context.pushContext(Status.IN_TEXT);
            }
            break;

        default:
            invalidState("addLineBreak");
            break;
        }
        return this;
    }

    public GvHtmlLabel beginTextModifier(GvTextModifier modifier) {
        switch (context.getStatus()) {
        case IN_LABEL:
        case IN_TEXT:
        case IN_TEXT_MODIFIER:
        case IN_FONT:
        case IN_CELL:
            builder.append("<" + modifier.encode() + ">");
            context = context.pushContext(Status.IN_TEXT_MODIFIER, modifier);
            break;

        default:
            invalidState("addTextModifier");
            break;
        }
        return this;
    }

    public GvHtmlLabel beginTextModifiers(GvTextModifier... modifiers) {
        for (final GvTextModifier modifier : modifiers) {
            beginTextModifier(modifier);
        }
        return this;
    }

    public GvHtmlLabel endTextModifier() {
        if (context.getStatus() == Status.IN_TEXT) {
            context = context.popContext();
        }

        if (context.getStatus() == Status.IN_TEXT_MODIFIER) {
            builder.append("</");
            builder.append(context.getData(GvTextModifier.class).encode());
            builder.append(">");
            context = context.popContext();
        } else {
            invalidState("endTextModifier");
        }
        return this;
    }

    public GvHtmlLabel endTextModifiers() {
        if (context.getStatus() == Status.IN_TEXT) {
            context = context.popContext();
        }

        while (context.getStatus() == Status.IN_TEXT_MODIFIER) {
            endTextModifier();
        }
        return this;
    }

    public GvHtmlLabel beginFont(GvColor color,
                                 String face,
                                 int pointSize) {
        return beginFont(color.encode(), face, pointSize);
    }

    public GvHtmlLabel beginFont(String color,
                                 String face,
                                 int pointSize) {
        switch (context.getStatus()) {
        case IN_LABEL:
        case IN_CELL:
        case IN_FONT:
        case IN_ROW:
        case IN_TEXT_MODIFIER:
        case IN_TEXT:
            builder.append("<font");
            if (color != null) {
                builder.append(" color=\"" + color + "\"");
            }
            if (face != null) {
                builder.append(" face=\"" + face + "\"");
            }
            if (pointSize >= 0) {
                builder.append(" point-size=\"" + pointSize + "\"");
            }
            builder.append(">");
            context = context.pushContext(Status.IN_FONT);
            break;

        default:
            invalidState("beginFont");
            break;
        }
        return this;
    }

    public GvHtmlLabel beginFont(String face,
                                 int pointSize) {
        return beginFont((String) null, face, pointSize);
    }

    public GvHtmlLabel beginFont(String face) {
        return beginFont(face, -1);
    }

    public GvHtmlLabel beginFont(int pointSize) {
        return beginFont(null, pointSize);
    }

    public GvHtmlLabel endFont() {
        if (context.getStatus() == Status.IN_TEXT) {
            context = context.popContext();
        }

        if (context.getStatus() == Status.IN_FONT) {
            builder.append("</font>");
            context = context.popContext();
        } else {
            invalidState("endFont");
        }
        return this;
    }

    public GvHtmlLabel beginTable(GvTableAttributes atts) {
        switch (context.getStatus()) {
        case IN_LABEL:
        case IN_CELL:
        case IN_FONT:
            builder.append("<table");
            if (atts != null) {
                for (final GvAttributeName att : GvAttributeName.values()) {
                    final String value = atts.getValue(att);
                    if (value != null) {
                        builder.append(" ");
                        builder.append(att.encode());
                        builder.append("=\"");
                        XmlUtils.appendEscaped(builder,
                                               value,
                                               XmlUtils.Context.ATTRIBUTE_DOUBLE_QUOTE,
                                               XmlUtils.EscapingPolicy.ESCAPE_ALWAYS);
                        builder.append("\"");
                    }
                }
            }
            builder.append(">");
            context = context.pushContext(Status.IN_TABLE);
            break;

        default:
            invalidState("beginTable");
            break;
        }
        return this;
    }

    public GvHtmlLabel beginTable() {
        return beginTable(null);
    }

    public GvHtmlLabel endTable() {
        if (context.getStatus() == Status.IN_TABLE) {
            builder.append("</table>");
            context = context.popContext();
        } else {
            invalidState("endTable");
        }
        return this;
    }

    public GvHtmlLabel beginRow() {
        if (context.getStatus() == Status.IN_TABLE) {
            builder.append("<tr>");
            context = context.pushContext(Status.IN_ROW);
        } else {
            invalidState("beginRow");
        }
        return this;
    }

    public GvHtmlLabel endRow() {
        if (context.getStatus() == Status.IN_ROW) {
            builder.append("</tr>");
            context = context.popContext();
        } else {
            invalidState("endRow");
        }
        return this;
    }

    public GvHtmlLabel addHorizontalRule() {
        if (context.getStatus() == Status.IN_TABLE) {
            builder.append("<hr/>");
            // Don't change context
        } else {
            invalidState("addHorizontalRule");
        }
        return this;
    }

    public GvHtmlLabel beginCell(GvCellAttributes atts) {
        if (context.getStatus() == Status.IN_ROW) {
            builder.append("<td");
            if (atts != null) {
                for (final GvAttributeName att : GvAttributeName.values()) {
                    final String value = atts.getValue(att);
                    if (value != null) {
                        builder.append(" ");
                        builder.append(att.encode());
                        builder.append("=\"");
                        XmlUtils.appendEscaped(builder,
                                               value,
                                               XmlUtils.Context.ATTRIBUTE_DOUBLE_QUOTE,
                                               XmlUtils.EscapingPolicy.ESCAPE_ALWAYS);
                        builder.append("\"");
                    }
                }
            }
            builder.append(">");
            context = context.pushContext(Status.IN_CELL);
        } else {
            invalidState("beginCell");
        }
        return this;
    }

    public GvHtmlLabel beginCell() {
        return beginCell(null);
    }

    public GvHtmlLabel endCell() {
        if (context.getStatus() == Status.IN_TEXT) {
            context = context.popContext();
        }

        if (context.getStatus() == Status.IN_CELL) {
            builder.append("</td>");
            context = context.popContext();
        } else {
            invalidState("endCell");
        }
        return this;
    }

    /**
     *
     * @param scale Specifies how an image will use any extra space available in
     *            its cell. Allowed values are:
     *            <ul>
     *            <li>FALSE: keep image its natural size. (Default)</li>
     *            <li>TRUE: scale image uniformly to fit.</li>
     *            <li>WIDTH: expand image width to fill</li>
     *            <li>HEIGHT: expand image height to fill</li>
     *            <li>BOTH: expand both image width height to fill</li>
     *            </ul>
     *
     * @param src The image.
     * @return This object.
     */
    public GvHtmlLabel addCellImage(String scale,
                                    String src) {
        beginCell(null);
        builder.append("<img");
        if (scale != null) {
            builder.append(" scale=\"" + scale + "\"");
        }
        builder.append(" src=\"" + src + "\"/>");
        endCell();
        return this;
    }

    public GvHtmlLabel addVerticalRule() {
        if (context.getStatus() == Status.IN_ROW) {
            builder.append("<vr/>");
            // Don't change context
        } else {
            invalidState("addVerticalRule");
        }
        return this;
    }

    @Override
    public String toString() {
        return "<" + builder.toString() + ">";
    }
}