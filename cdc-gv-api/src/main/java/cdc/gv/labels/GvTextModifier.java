package cdc.gv.labels;

import cdc.gv.support.GvEncodable;

public enum GvTextModifier implements GvEncodable {
    BOLD("b"),
    ITALIC("i"),
    UNDERLINE("u"),
    OVERLINE("o"),
    SUB_SCRIPT("sub"),
    SUPER_SCRIPT("sup"),
    STRIKE_THROUGH("s");

    private final String code;

    private GvTextModifier(String code) {
        this.code = code;
    }

    @Override
    public String encode() {
        return code;
    }
}