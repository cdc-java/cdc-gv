package cdc.gv.labels;

/**
 * Class used to handle html label state stack.
 *
 * @author Damien Carbonne
 *
 */
class GvHtmlLabelContext {
    public enum Status {
        IN_LABEL,
        LOCKED,
        IN_TEXT,
        IN_TEXT_MODIFIER,
        IN_FONT,
        IN_TABLE,
        IN_ROW,
        IN_CELL
    }

    private Status status;
    private final GvHtmlLabelContext parent;
    private GvHtmlLabelContext child = null;
    private final int level;
    private Object data;

    private GvHtmlLabelContext(Status status,
                               GvHtmlLabelContext parent) {
        this.status = status;
        this.parent = parent;
        if (parent == null) {
            this.level = -1;
        } else {
            this.level = parent.level + 1;
            parent.child = this;
        }
    }

    GvHtmlLabelContext() {
        this(Status.IN_LABEL, null);
    }

    final int getLevel() {
        return level;
    }

    final GvHtmlLabelContext pushContext(Status type,
                                         Object data) {
        if (child == null) {
            new GvHtmlLabelContext(type, this);
            assert child != null;
        } else {
            child.setStatus(type);
        }
        child.data = data;
        return child;
    }

    final GvHtmlLabelContext pushContext(Status type) {
        return pushContext(type, null);
    }

    final GvHtmlLabelContext popContext() {
        status = null;
        return parent;
    }

    final GvHtmlLabelContext getRoot() {
        GvHtmlLabelContext index = this;
        while (index.parent != null) {
            index = index.parent;
        }
        return index;
    }

    final Status getStatus() {
        return status;
    }

    final void setStatus(Status status) {
        this.status = status;
    }

    final Object getData() {
        return data;
    }

    final <D> D getData(Class<D> klass) {
        return klass.cast(data);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(status);
        return builder.toString();
    }
}