package cdc.gv.labels;

/**
 * Enumeration of possible attribute usages (Table or Cell).
 *
 * @author Damien Carbonne
 *
 */
public enum GvAttributeUsage {
    /** The attribute can be used for tables. */
    TABLE,
    /** The attribute can be used for cells. */
    CELL;

    public static final GvAttributeUsage T = TABLE;
    public static final GvAttributeUsage C = CELL;
}