package cdc.gv.labels;

import cdc.gv.colors.GvColor;
import cdc.gv.support.GvBaseAttributes;

abstract class GvAttributes<A extends GvAttributes<A>> extends GvBaseAttributes<A, GvAttributeName, GvAttributeUsage> {
    /**
     * Create a set of GvAttributes for a given usage.
     *
     * @param usage The usage for which this set of attributes is intended.
     */
    protected GvAttributes(GvAttributeUsage usage) {
        super(GvAttributeName.class, GvAttributeUsage.class, usage);
    }

    /**
     * Specifies horizontal placement. When an object is allocated more space
     * than required, this value determines where the extra space is placed left
     * and right of the object.
     * <ul>
     * <li>CENTER aligns the object in the center. (Default)</li>
     * <li>LEFT aligns the object on the left.</li>
     * <li>RIGHT aligns the object on the right.</li>
     * </ul>
     * CENTER, LEFT, RIGHT, TEXT
     *
     * @param value The align value.
     * @return This set of attributes.
     */
    public final A setAlign(GvAlign value) {
        setValue(GvAttributeName.ALIGN, value.encode());
        return self();
    }

    /**
     * Specifies the default alignment of BR elements contained in the cell.
     * That
     * is, if a BR element has no explicit ALIGN attribute, the attribute value
     * is specified by the value of BALIGN.
     *
     * @param value The align value.
     * @return This set of attributes.
     */
    protected A setBAlign(GvAlign value) {
        setValue(GvAttributeName.BALIGN, value.encode());
        return self();
    }

    /**
     * Sets the color of the background. This color can be overridden by a
     * BG_COLOR attribute in descendants. The value can be a single color or two
     * colors separated by a colon, the latter indicating a gradient fill.
     *
     * @param value The color.
     * @return This set of attributes.
     */
    public final A setBgColor(GvColor value) {
        setValue(GvAttributeName.BG_COLOR, value.encode());
        return self();
    }

    /**
     * Sets the color of the background. This color can be overridden by a
     * BG_COLOR attribute in descendants. The value can be a single color or two
     * colors separated by a colon, the latter indicating a gradient fill.
     *
     * @param value1 The first color.
     * @param value2 The second color.
     * @return This set of attributes.
     */
    public final A setBgColor(GvColor value1,
                              GvColor value2) {
        setValue(GvAttributeName.BG_COLOR, value1.encode() + ":" + value2.encode());
        return self();
    }

    /**
     * Specifies the width of the border around the object in points. A value of
     * zero indicates no border. The default is 1. The maximum value is 255. If
     * set in a table, and CELL_BORDER is not set, this value is also used for
     * all cells in the table. It can be overridden by a BORDER tag in a cell.
     *
     * @param value The border.
     * @return This set of attributes.
     */
    public final A setBorder(int value) {
        setValue(GvAttributeName.BORDER, value);
        return self();
    }

    /**
     * Specifies the width of the border for all cells in a table. It can be
     * overridden by a BORDER tag in a cell. The maximum value is 255.
     *
     * @param value The cell border.
     * @return This set of attributes.
     */
    protected A setCellBorder(int value) {
        setValue(GvAttributeName.CELL_BORDER, value);
        return self();
    }

    /**
     * Specifies the space, in points, between a cell's border and its content.
     * The default is 2. The maximum value is 255.
     *
     * @param value The cell padding.
     * @return This set of attributes.
     */
    public final A setCellPadding(int value) {
        setValue(GvAttributeName.CELL_PADDING, value);
        return self();
    }

    /**
     * Specifies the space, in points, between cells in a table and between a
     * cell and the table's border. The default is 2. The maximum value is 127.
     *
     * @param value The cell soacing.
     * @return This set of attributes.
     */
    public final A setCellSpacing(int value) {
        setValue(GvAttributeName.CELL_SPACING, value);
        return self();
    }

    /**
     * Sets the color of the font within the scope of FONT.../FONT, or the
     * border color of the table or cell within the scope of TABLE.../TABLE, or
     * TD.../TD. This color can be overridden by a COLOR attribute in
     * descendants. By default, the font color is determined by the fontcolor
     * attribute of the corresponding node, edge or graph, and the border color
     * is determined by the color attribute of the corresponding node, edge or
     * graph.
     *
     * @param value The color.
     * @return This set of attributes.
     */
    public final A setColor(GvColor value) {
        setValue(GvAttributeName.COLOR, value.encode());
        return self();
    }

    /**
     * Set col span.
     *
     * @param value The col span.
     * @return This set of attributes.
     */
    protected A setColSpan(int value) {
        setValue(GvAttributeName.COL_SPAN, value);
        return self();
    }

    /**
     * Specifies the number of columns spanned by the cell. The default is 1.
     * The maximum value is 65535.
     *
     * @param value The number of columns.
     * @return This set of attributes.
     */
    protected A setColumns(int value) {
        setValue(GvAttributeName.COLUMNS, value);
        return self();
    }

    /**
     * Specifies whether the values given by the WIDTH and HEIGHT attributes are
     * enforced.
     * <ul>
     * <li>FALSE allows the object to grow so that all its contents will fit.
     * (Default)</li>
     * <li>TRUE fixes the object size to its given WIDTH and HEIGHT. Both of
     * these attributes must be supplied.</li>
     * </ul>
     *
     * @param value The fixed size hint.
     * @return This set of attributes.
     */
    public final A setFixedSize(boolean value) {
        setValue(GvAttributeName.FIXED_SIZE, value ? "TRUE" : "FALSE");
        return self();
    }

    /**
     * Gives the angle used in a gradient fill if the BG_COLOR is a color list.
     * For the default linear gradient, this specifies the angle of a line
     * through the center along which the colors transform. Thus, an angle of 0
     * will cause a left-to-right progression. For radial gradients (see STYLE),
     * the angle specifies the position of the center of the coloring. An angle
     * of 0 places the center at the center of the table or cell; an non-zero
     * angle places the fill center along that angle near the boundary.
     *
     * @param value The gradiant angle.
     * @return This set of attributes.
     */
    public final A setGradiantAngle(int value) {
        setValue(GvAttributeName.GRADIENT_ANGLE, value);
        return self();
    }

    /**
     * Specifies the minimum height, in points, of the object. The height
     * includes the contents, any spacing and the border. Unless FIXEDSIZE is
     * true, the height will be expanded to allow the contents to fit. The
     * maximum value is 65535.
     *
     * @param value The height.
     * @return This set of attributes.
     */
    public final A setHeight(int value) {
        setValue(GvAttributeName.HEIGHT, value);
        return self();
    }

    /**
     * Attaches a URL to the object.
     *
     * @param value The URL.
     * @return This set of attributes.
     */
    public final A setHRef(String value) {
        setValue(GvAttributeName.HREF, value);
        return self();
    }

    /**
     * Allows the user to specify a unique ID for a table or cell.
     *
     * @param value The id.
     * @return This set of attributes.
     */
    public final A setId(String value) {
        setValue(GvAttributeName.ID, value);
        return self();
    }

    /**
     * Attaches a portname to the object. This can be used to modify the head or
     * tail of an edge, so that the end attaches directly to the object.
     *
     * @param value The port name.
     * @return This set of attributes.
     */
    public final A setPort(String value) {
        setValue(GvAttributeName.PORT, value);
        return self();
    }

    /**
     * Provides general formatting information concerning the rows. At present,
     * the only legal value is "*", which causes a horizontal rule to appear
     * between every row.
     *
     * @param value The row formatting hint.
     * @return This set of attributes.
     */
    protected A setRows(String value) {
        setValue(GvAttributeName.ROWS, value);
        return self();
    }

    /**
     *
     * @param value The row span.
     * @return This set of attributes.
     */
    protected A setRowSpan(int value) {
        setValue(GvAttributeName.ROW_SPAN, value);
        return self();
    }

    /**
     * Specifies which sides of a border in a cell or table should be drawn, if
     * a border is drawn. By default, all sides are drawn. The "value" string can
     * contain any collection of the (case-insensitive) characters 'L', 'T',
     * 'R', or 'B', corresponding to the left, top, right and, bottom sides of the
     * border, respectively. For example, SIDES="LB" would indicate only the
     * left and bottom segments of the border should be drawn.
     *
     * @param values The sides.
     * @return This set of attributes.
     */
    public final A setSides(GvSide... values) {
        setValue(GvAttributeName.SIDES, encode("", values));
        return self();
    }

    /**
     * Specifies style characteristics of the table or cell. Style
     * characteristics are given as a comma or space separated list of style
     * attributes. At present, the only legal attributes are "ROUNDED" and
     * "RADIAL" for tables, and "RADIAL" for cells. If "ROUNDED" is specified,
     * the table will have rounded corners. This probably works best if the
     * outmost cells have no borders, or their CELLSPACING is sufficiently
     * large.<br>
     * If it is desirable to have borders around the cells, use HR and VR
     * elements, or the COLUMNS and ROWS attributes of TABLE.
     *
     * @param values The styles.
     * @return This set of attributes.
     */
    public final A setStyle(GvStyle... values) {
        setValue(GvAttributeName.STYLE, encode(",", values));
        return self();
    }

    /**
     * Determines which window of the browser is used for the URL if the object
     * has one.
     *
     * @param value The target.
     * @return This set of attributes.
     */
    public final A setTarget(String value) {
        setValue(GvAttributeName.TARGET, value);
        return self();
    }

    /**
     * Sets the tooltip annotation attached to the element. This is used only if
     * the element has a HREF attribute.
     *
     * @param value The title.
     * @return This set of attributes.
     */
    public final A setTitle(String value) {
        setValue(GvAttributeName.TITLE, value);
        return self();
    }

    /**
     * Is an alias for TITLE.
     *
     * @param value The tooltip.
     * @return This set of attributes.
     */
    public final A setTooltip(String value) {
        setValue(GvAttributeName.TOOLTIP, value);
        return self();
    }

    /**
     * Specifies vertical placement. When an object is allocated more space than
     * required, this value determines where the extra space is placed above and
     * below the object.
     * <ul>
     * <li>MIDDLE aligns the object in the center. (Default)</li>
     * <li>BOTTOM aligns the object on the bottom.</li>
     * <li>TOP aligns the object on the top.</li>
     * </ul>
     *
     * @param value The vertical alignment.
     * @return This set of attributes.
     */
    public final A setVAlign(GvVAlign value) {
        setValue(GvAttributeName.VALIGN, value.encode());
        return self();
    }

    /**
     * Specifies the minimum width, in points, of the object. The width includes
     * the contents, any spacing and the border. Unless FIXEDSIZE is true, the
     * width will be expanded to allow the contents to fit. The maximum value is
     * 65535.
     *
     * @param value The width.
     * @return This set of attributes.
     */
    public final A setWidth(int value) {
        setValue(GvAttributeName.WIDTH, value);
        return self();
    }
}