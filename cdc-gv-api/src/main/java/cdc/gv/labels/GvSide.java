package cdc.gv.labels;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible Cell / Row / Table Sides.
 *
 * @author Damien Carbonne
 *
 */
public enum GvSide implements GvEncodable {
    L,
    R,
    B,
    T;

    @Override
    public String encode() {
        return name();
    }
}