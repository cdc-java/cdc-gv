package cdc.gv.support;

/**
 * Interface implemented by classes that can be string encoded in a GraphViz
 * compliant way.
 *
 * @author Damien Carbonne
 *
 */
@FunctionalInterface
public interface GvEncodable {
    /**
     * Encode the object as a string compliant with GraphViz conventions.
     *
     * @return The string representation of the object.
     */
    public String encode();
}