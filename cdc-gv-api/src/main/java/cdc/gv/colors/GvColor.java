package cdc.gv.colors;

import java.util.Locale;

import cdc.gv.support.GvEncodable;

/**
 * Definition of a color. A color can be defined as RGB, RGBA, HSV or by name.
 *
 * @author Damien Carbonne
 *
 */
public class GvColor implements GvEncodable {
    final String code;

    /**
     * Create a color from its name or encoded value. Used by X11, Svg and
     * Brewer schemes.
     *
     * @param name Name of the color.
     */
    public GvColor(String name) {
        code = name;
    }

    public GvColor(String scheme,
                   String name) {
        code = "/" + scheme + "/" + name;
    }

    /**
     * Create a color from (R, G, B) values. Each component must be in range
     * 0 .. 255.
     *
     * @param r The red component
     * @param g The green component
     * @param b The blue component
     */
    public GvColor(int r,
                   int g,
                   int b) {
        code = String.format("#%02x%02x%02x", saturate(r), saturate(g), saturate(b));
    }

    /**
     * Create a color from (R, G, B, A) values. Each component must be in range
     * 0 .. 255.
     *
     * @param r The red component
     * @param g The green component
     * @param b The blue component
     * @param a The alpha component
     */
    public GvColor(int r,
                   int g,
                   int b,
                   int a) {
        code = String.format("#%02x%02x%02x%02x", saturate(r), saturate(g), saturate(b), saturate(a));
    }

    /**
     * Create a color from (H, S, V) values. Each component must be in range
     * 0.0 .. 1.0.
     *
     * @param h The Hue part.
     * @param s The saturation part.
     * @param v The value part.
     */
    public GvColor(double h,
                   double s,
                   double v) {
        code = String.format(Locale.ENGLISH, "%4.3f %4.3f %4.3f", saturate(h), saturate(s), saturate(v));
    }

    private static int saturate(int x) {
        if (x < 0) {
            return 0;
        } else if (x > 255) {
            return 255;
        } else {
            return x;
        }
    }

    private static double saturate(double x) {
        if (x < 0.0) {
            return 0.0;
        } else if (x > 1.0) {
            return 1.0;
        } else {
            return x;
        }
    }

    @Override
    public String encode() {
        return code;
    }
}