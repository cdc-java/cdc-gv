package cdc.gv.colors;

import cdc.gv.support.GvEncodable;

/**
 * Support of Brewer colors.
 *
 * This product includes color specifications and designs developed by Cynthia
 * Brewer (http://colorbrewer.org/).
 *
 * @author Damien Carbonne
 *
 */
public final class GvBrewerColors {
    private GvBrewerColors() {
    }

    /**
     * Enumeration of possible scheme types.
     *
     * @author Damien Carbonne
     *
     */
    public enum SchemeType {
        /**
         * The color scheme is sequential. It may have a single hue or several
         * hues.
         */
        SEQUENTIAL,

        /**
         * The color scheme is diverging.
         */
        DIVERGING,

        /**
         * The color scheme is qualitative.
         */
        QUALITATIVE
    }

    /**
     * Enumeration of Brewer scheme families. Each family corresponds to a set
     * of schemas, indexed from 3 to getMaxIndex().
     *
     * @author Damien Carbonne
     *
     */
    public enum SchemeFamily implements GvEncodable {
        ACCENT(SchemeType.QUALITATIVE, 8),
        BLUES(SchemeType.SEQUENTIAL, 9),
        BRBG(SchemeType.DIVERGING, 11),
        BUGN(SchemeType.SEQUENTIAL, 9),
        BUPU(SchemeType.SEQUENTIAL, 9),
        DARK2(SchemeType.QUALITATIVE, 8),
        GNBU(SchemeType.SEQUENTIAL, 9),
        GREENS(SchemeType.SEQUENTIAL, 9),
        GREYS(SchemeType.SEQUENTIAL, 9),
        ORANGES(SchemeType.SEQUENTIAL, 9),
        ORRD(SchemeType.SEQUENTIAL, 9),
        PAIRED(SchemeType.QUALITATIVE, 12),
        PASTEL1(SchemeType.QUALITATIVE, 9),
        PASTEL2(SchemeType.QUALITATIVE, 8),
        PIYG(SchemeType.DIVERGING, 11),
        PRGN(SchemeType.DIVERGING, 11),
        PUBU(SchemeType.SEQUENTIAL, 9),
        PUBUGN(SchemeType.SEQUENTIAL, 9),
        PUOR(SchemeType.DIVERGING, 11),
        PURD(SchemeType.SEQUENTIAL, 9),
        PURPLES(SchemeType.SEQUENTIAL, 9),
        RDBU(SchemeType.DIVERGING, 11),
        RDGY(SchemeType.DIVERGING, 11),
        RDPU(SchemeType.SEQUENTIAL, 9),
        REDS(SchemeType.SEQUENTIAL, 9),
        RDYLBU(SchemeType.DIVERGING, 11),
        RDYLGN(SchemeType.DIVERGING, 11),
        SET1(SchemeType.QUALITATIVE, 9),
        SET2(SchemeType.QUALITATIVE, 8),
        SET3(SchemeType.QUALITATIVE, 12),
        SPECTRAL(SchemeType.DIVERGING, 11),
        YLGN(SchemeType.SEQUENTIAL, 9),
        YLGNBU(SchemeType.SEQUENTIAL, 9),
        YLORBR(SchemeType.SEQUENTIAL, 9),
        YLORRD(SchemeType.SEQUENTIAL, 9);

        /**
         * Type of the family.
         */
        private final SchemeType type;

        /**
         * Number of schemes in the family.
         */
        private final int maxIndex;

        public static final int MIN_INDEX = 3;

        private SchemeFamily(SchemeType type,
                             int maxIndex) {
            this.type = type;
            this.maxIndex = maxIndex;
        }

        /**
         * @return The scheme type of the scheme family.
         */
        public final SchemeType getType() {
            return type;
        }

        /**
         * @return The family maximum index;
         */
        public final int getMaxIndex() {
            return maxIndex;
        }

        @Override
        public String encode() {
            return name().toLowerCase();
        }
    }

    /**
     * Creates a GvColor corresponding to Brewer colors specifications.
     *
     * @param family Name of the scheme family.
     * @param familyIndex Index of the family. Must be in range
     *            [3..family.getMaxIndex()]
     * @param colorIndex Index of the color. Must be in range [1..familyIndex].
     *
     * @return A newly created color corresponding to the specification, or
     *         null.
     */
    public static GvColor create(SchemeFamily family,
                                 int familyIndex,
                                 int colorIndex) {
        if (familyIndex >= 3 && familyIndex <= family.getMaxIndex() && colorIndex >= 1 && colorIndex <= familyIndex) {
            return new GvColor(family.encode() + familyIndex, Integer.toString(colorIndex));
        } else {
            throw new IllegalArgumentException("create(" + family + ", " + familyIndex + ", " + colorIndex + ")");
        }
    }
}