package cdc.gv.colors;

/**
 * Definition of SVG colors.
 *
 * @author Damien Carbonne
 *
 */
public final class GvSvgColors {
    public static final String SCHEME = "SVG";

    public static final GvColor ALICEBLUE = new GvColor("aliceblue");
    public static final GvColor ANTIQUEWHITE = new GvColor("antiquewhite");
    public static final GvColor AQUA = new GvColor("aqua");
    public static final GvColor AQUAMARINE = new GvColor("aquamarine");
    public static final GvColor AZURE = new GvColor("azure");
    public static final GvColor BEIGE = new GvColor("beige");
    public static final GvColor BISQUE = new GvColor("bisque");
    public static final GvColor BLACK = new GvColor("black");
    public static final GvColor BLANCHEDALMOND = new GvColor("blanchedalmond");
    public static final GvColor BLUE = new GvColor("blue");
    public static final GvColor BLUEVIOLET = new GvColor("blueviolet");
    public static final GvColor BROWN = new GvColor("brown");
    public static final GvColor BURLYWOOD = new GvColor("burlywood");
    public static final GvColor CADETBLUE = new GvColor("cadetblue");
    public static final GvColor CHARTREUSE = new GvColor("chartreuse");
    public static final GvColor CHOCOLATE = new GvColor("chocolate");
    public static final GvColor CORAL = new GvColor("coral");
    public static final GvColor CORNFLOWERBLUE = new GvColor("cornflowerblue");
    public static final GvColor CORNSILK = new GvColor("cornsilk");
    public static final GvColor CRIMSON = new GvColor("crimson");
    public static final GvColor CYAN = new GvColor("cyan");
    public static final GvColor DARKBLUE = new GvColor("darkblue");
    public static final GvColor DARKCYAN = new GvColor("darkcyan");
    public static final GvColor DARKGOLDENROD = new GvColor("darkgoldenrod");
    public static final GvColor DARKGRAY = new GvColor("darkgray");
    public static final GvColor DARKGREEN = new GvColor("darkgreen");
    public static final GvColor DARKGREY = new GvColor("darkgrey");
    public static final GvColor DARKKHAKI = new GvColor("darkkhaki");
    public static final GvColor DARKMAGENTA = new GvColor("darkmagenta");
    public static final GvColor DARKOLIVEGREEN = new GvColor("darkolivegreen");
    public static final GvColor DARKORANGE = new GvColor("darkorange");
    public static final GvColor DARKORCHID = new GvColor("darkorchid");
    public static final GvColor DARKRED = new GvColor("darkred");
    public static final GvColor DARKSALMON = new GvColor("darksalmon");
    public static final GvColor DARKSEAGREEN = new GvColor("darkseagreen");
    public static final GvColor DARKSLATEBLUE = new GvColor("darkslateblue");
    public static final GvColor DARKSLATEGRAY = new GvColor("darkslategray");
    public static final GvColor DARKSLATEGREY = new GvColor("darkslategrey");
    public static final GvColor DARKTURQUOISE = new GvColor("darkturquoise");
    public static final GvColor DARKVIOLET = new GvColor("darkviolet");
    public static final GvColor DEEPPINK = new GvColor("deeppink");
    public static final GvColor DEEPSKYBLUE = new GvColor("deepskyblue");
    public static final GvColor DIMGRAY = new GvColor("dimgray");
    public static final GvColor DIMGREY = new GvColor("dimgrey");
    public static final GvColor DODGERBLUE = new GvColor("dodgerblue");
    public static final GvColor FIREBRICK = new GvColor("firebrick");
    public static final GvColor FLORALWHITE = new GvColor("floralwhite");
    public static final GvColor FORESTGREEN = new GvColor("forestgreen");
    public static final GvColor FUCHSIA = new GvColor("fuchsia");
    public static final GvColor GAINSBORO = new GvColor("gainsboro");
    public static final GvColor GHOSTWHITE = new GvColor("ghostwhite");
    public static final GvColor GOLD = new GvColor("gold");
    public static final GvColor GOLDENROD = new GvColor("goldenrod");
    public static final GvColor GRAY = new GvColor("gray");
    public static final GvColor GREY = new GvColor("grey");
    public static final GvColor GREEN = new GvColor("green");
    public static final GvColor GREENYELLOW = new GvColor("greenyellow");
    public static final GvColor HONEYDEW = new GvColor("honeydew");
    public static final GvColor HOTPINK = new GvColor("hotpink");
    public static final GvColor INDIANRED = new GvColor("indianred");
    public static final GvColor INDIGO = new GvColor("indigo");
    public static final GvColor IVORY = new GvColor("ivory");
    public static final GvColor KHAKI = new GvColor("khaki");
    public static final GvColor LAVENDER = new GvColor("lavender");
    public static final GvColor LAVENDERBLUSH = new GvColor("lavenderblush");
    public static final GvColor LAWNGREEN = new GvColor("lawngreen");
    public static final GvColor LEMONCHIFFON = new GvColor("lemonchiffon");
    public static final GvColor LIGHTBLUE = new GvColor("lightblue");
    public static final GvColor LIGHTCORAL = new GvColor("lightcoral");
    public static final GvColor LIGHTCYAN = new GvColor("lightcyan");
    public static final GvColor LIGHTGOLDENRODYELLOW = new GvColor("lightgoldenrodyellow");
    public static final GvColor LIGHTGRAY = new GvColor("lightgray");
    public static final GvColor LIGHTGREEN = new GvColor("lightgreen");
    public static final GvColor LIGHTGREY = new GvColor("lightgrey");
    public static final GvColor LIGHTPINK = new GvColor("lightpink");
    public static final GvColor LIGHTSALMON = new GvColor("lightsalmon");
    public static final GvColor LIGHTSEAGREEN = new GvColor("lightseagreen");
    public static final GvColor LIGHTSKYBLUE = new GvColor("lightskyblue");
    public static final GvColor LIGHTSLATEGRAY = new GvColor("lightslategray");
    public static final GvColor LIGHTSLATEGREY = new GvColor("lightslategrey");
    public static final GvColor LIGHTSTEELBLUE = new GvColor("lightsteelblue");
    public static final GvColor LIGHTYELLOW = new GvColor("lightyellow");
    public static final GvColor LIME = new GvColor("lime");
    public static final GvColor LIMEGREEN = new GvColor("limegreen");
    public static final GvColor LINEN = new GvColor("linen");
    public static final GvColor MAGENTA = new GvColor("magenta");
    public static final GvColor MAROON = new GvColor("maroon");
    public static final GvColor MEDIUMAQUAMARINE = new GvColor("mediumaquamarine");
    public static final GvColor MEDIUMBLUE = new GvColor("mediumblue");
    public static final GvColor MEDIUMORCHID = new GvColor("mediumorchid");
    public static final GvColor MEDIUMPURPLE = new GvColor("mediumpurple");
    public static final GvColor MEDIUMSEAGREEN = new GvColor("mediumseagreen");
    public static final GvColor MEDIUMSLATEBLUE = new GvColor("mediumslateblue");
    public static final GvColor MEDIUMSPRINGGREEN = new GvColor("mediumspringgreen");
    public static final GvColor MEDIUMTURQUOISE = new GvColor("mediumturquoise");
    public static final GvColor MEDIUMVIOLETRED = new GvColor("mediumvioletred");
    public static final GvColor MIDNIGHTBLUE = new GvColor("midnightblue");
    public static final GvColor MINTCREAM = new GvColor("mintcream");
    public static final GvColor MISTYROSE = new GvColor("mistyrose");
    public static final GvColor MOCCASIN = new GvColor("moccasin");
    public static final GvColor NAVAJOWHITE = new GvColor("navajowhite");
    public static final GvColor NAVY = new GvColor("navy");
    public static final GvColor OLDLACE = new GvColor("oldlace");
    public static final GvColor OLIVE = new GvColor("olive");
    public static final GvColor OLIVEDRAB = new GvColor("olivedrab");
    public static final GvColor ORANGE = new GvColor("orange");
    public static final GvColor ORANGERED = new GvColor("orangered");
    public static final GvColor ORCHID = new GvColor("orchid");
    public static final GvColor PALEGOLDENROD = new GvColor("palegoldenrod");
    public static final GvColor PALEGREEN = new GvColor("palegreen");
    public static final GvColor PALETURQUOISE = new GvColor("paleturquoise");
    public static final GvColor PALEVIOLETRED = new GvColor("palevioletred");
    public static final GvColor PAPAYAWHIP = new GvColor("papayawhip");
    public static final GvColor PEACHPUFF = new GvColor("peachpuff");
    public static final GvColor PERU = new GvColor("peru");
    public static final GvColor PINK = new GvColor("pink");
    public static final GvColor PLUM = new GvColor("plum");
    public static final GvColor POWDERBLUE = new GvColor("powderblue");
    public static final GvColor PURPLE = new GvColor("purple");
    public static final GvColor RED = new GvColor("red");
    public static final GvColor ROSYBROWN = new GvColor("rosybrown");
    public static final GvColor ROYALBLUE = new GvColor("royalblue");
    public static final GvColor SADDLEBROWN = new GvColor("saddlebrown");
    public static final GvColor SALMON = new GvColor("salmon");
    public static final GvColor SANDYBROWN = new GvColor("sandybrown");
    public static final GvColor SEAGREEN = new GvColor("seagreen");
    public static final GvColor SEASHELL = new GvColor("seashell");
    public static final GvColor SIENNA = new GvColor("sienna");
    public static final GvColor SILVER = new GvColor("silver");
    public static final GvColor SKYBLUE = new GvColor("skyblue");
    public static final GvColor SLATEBLUE = new GvColor("slateblue");
    public static final GvColor SLATEGRAY = new GvColor("slategray");
    public static final GvColor SLATEGREY = new GvColor("slategrey");
    public static final GvColor SNOW = new GvColor("snow");
    public static final GvColor SPRINGGREEN = new GvColor("springgreen");
    public static final GvColor STEELBLUE = new GvColor("steelblue");
    public static final GvColor TAN = new GvColor("tan ");
    public static final GvColor TEAL = new GvColor("teal");
    public static final GvColor THISTLE = new GvColor("thistle");
    public static final GvColor TOMATO = new GvColor("tomato");
    public static final GvColor TURQUOISE = new GvColor("turquoise");
    public static final GvColor VIOLET = new GvColor("violet");
    public static final GvColor WHEAT = new GvColor("wheat");
    public static final GvColor WHITE = new GvColor("white");
    public static final GvColor WHITESMOKE = new GvColor("whitesmoke");
    public static final GvColor YELLOW = new GvColor("yellow");
    public static final GvColor YELLOWGREEN = new GvColor("yellowgreen");

    private GvSvgColors() {
    }
}