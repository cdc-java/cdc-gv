package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

public enum GvArrowSide implements GvEncodable {
    L,
    R;

    @Override
    public String encode() {
        return this.name().toLowerCase();
    }
}