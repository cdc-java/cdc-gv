package cdc.gv.atts;

/**
 * Enumeration of possible attribute usages (graph, subgraph, cluster, node or
 * edge).
 *
 * @author Damien Carbonne
 *
 */
public enum GvAttributeUsage {
    /** The attribute can be used for graphs. */
    GRAPH,
    /** The attribute can be used for subgraphs. */
    SUBGRAPH,
    /** The attribute can be used for clusters. */
    CLUSTER,
    /** The attribute can be used for nodes. */
    NODE,
    /** The attribute can be used for edges. */
    EDGE;

    public static final GvAttributeUsage G = GRAPH;
    public static final GvAttributeUsage S = SUBGRAPH;
    public static final GvAttributeUsage C = CLUSTER;
    public static final GvAttributeUsage N = NODE;
    public static final GvAttributeUsage E = EDGE;
}