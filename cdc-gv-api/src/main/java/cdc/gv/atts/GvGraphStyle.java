package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible graph styles.
 *
 * @author Damien Carbonne
 *
 */
public enum GvGraphStyle implements GvEncodable {
    RADIAL;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}