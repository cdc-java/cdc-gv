package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible quad types.
 *
 * @author Damien Carbonne
 *
 */
public enum GvQuadType implements GvEncodable {
    NORMAL,
    FAST,
    NONE;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}