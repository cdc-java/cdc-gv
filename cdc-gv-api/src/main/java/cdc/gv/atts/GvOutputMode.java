package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible output orders.
 *
 * @author Damien Carbonne
 *
 */
public enum GvOutputMode implements GvEncodable {
    BREADTH_FIRST,
    NODES_FIRST,
    EDGES_FIRST;

    @Override
    public String encode() {
        return name().toLowerCase().replaceAll("-", "");
    }
}