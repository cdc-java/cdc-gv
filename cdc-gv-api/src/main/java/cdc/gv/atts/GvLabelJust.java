package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible label justifications.
 *
 * @author Damien Carbonne
 *
 */
public enum GvLabelJust implements GvEncodable {
    LEFT,
    CENTERED,
    RIGHT;

    @Override
    public String encode() {
        switch (this) {
        case LEFT:
            return "l";
        case RIGHT:
            return "r";
        default:
            return "";
        }
    }
}