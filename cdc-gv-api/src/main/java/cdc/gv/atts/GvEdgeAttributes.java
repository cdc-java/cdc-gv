package cdc.gv.atts;

import cdc.gv.colors.GvColor;
import cdc.gv.support.GvSupport;

/**
 * Definition of edge attributes.
 *
 * @author Damien Carbonne
 *
 */
public final class GvEdgeAttributes extends GvAttributes<GvEdgeAttributes> {
    public GvEdgeAttributes() {
        super(GvAttributeUsage.EDGE);
    }

    @Override
    protected GvEdgeAttributes self() {
        return this;
    }

    private static String encode(String name,
                                 GvCompassPoint compass) {
        final StringBuilder builder = new StringBuilder();
        if (name != null) {
            builder.append(name);
            if (compass != null) {
                builder.append(":");
                builder.append(compass.encode());
            }
        } else {
            assert compass != null;
            builder.append(compass.name().toLowerCase());
        }
        return builder.toString();
    }

    /**
     * Style of arrowhead on the head node of an edge. This will only appear if
     * the dir attribute is "forward" or "both".
     *
     * @param value The arrow type.
     * @return This object.
     */
    public GvEdgeAttributes setArrowHead(GvArrowType value) {
        return setValue(GvAttributeName.ARROW_HEAD, value.encode());
    }

    public GvEdgeAttributes setArrowHead(GvArrowShape value) {
        return setValue(GvAttributeName.ARROW_HEAD, value.encode());
    }

    /**
     * Multiplicative scale factor for arrowheads.
     *
     * @param value The arrow size.
     * @return This object.
     */
    public GvEdgeAttributes setArrowSize(double value) {
        return setValue(GvAttributeName.ARROW_SIZE, value);
    }

    /**
     * Style of arrowhead on the tail node of an edge. This will only appear if
     * the dir attribute is "back" or "both".
     *
     * @param value The arrow tail.
     * @return This object.
     */
    public GvEdgeAttributes setArrowTail(GvArrowType value) {
        return setValue(GvAttributeName.ARROW_TAIL, value.encode());
    }

    public GvEdgeAttributes setArrowTail(GvArrowShape value) {
        return setValue(GvAttributeName.ARROW_TAIL, value.encode());
    }

    @Override
    public GvEdgeAttributes setColor(GvColor value) {
        return super.setColor(value);
    }

    @Override
    public GvEdgeAttributes setColor(GvColor... values) {
        return super.setColor(values);
    }

    @Override
    public GvEdgeAttributes setColor(GvColorList value) {
        return super.setColor(value);
    }

    @Override
    public GvEdgeAttributes setColorScheme(String value) {
        return super.setColorScheme(value);
    }

    @Override
    public GvEdgeAttributes setComment(String value) {
        return super.setComment(value);
    }

    /**
     * If false, the edge is not used in ranking the nodes.
     *
     * @param value The constraint.
     * @return This object.
     */
    public GvEdgeAttributes setConstraint(boolean value) {
        return setValue(GvAttributeName.CONSTRAINT, value);
    }

    /**
     * If true, attach edge label to edge by a 2-segment polyline, underlining
     * the label, then going to the closest point of spline.
     *
     * @param value true to decorate.
     * @return This object.
     */
    public GvEdgeAttributes setDecorate(boolean value) {
        return setValue(GvAttributeName.DECORATE, value);
    }

    /**
     * Set edge type for drawing arrowheads. This indicates which ends of the
     * edge should be decorated with an arrowhead. The actual style of the
     * arrowhead can be specified using the arrowhead and arrowtail attributes.
     *
     * @param value The direction.
     * @return This object.
     */
    public GvEdgeAttributes setDir(GvDirType value) {
        return setValue(GvAttributeName.DIR, value.encode());
    }

    /**
     * Synonym for edgeURL.
     *
     * @param value The URL.
     * @return This object.
     */
    public GvEdgeAttributes setEdgeHRef(String value) {
        return setValue(GvAttributeName.EDGE_HREF, value);
    }

    /**
     * If the edge has a URL or edgeURL attribute, this attribute determines
     * which window of the browser is used for the URL attached to the non-label
     * part of the edge. Setting it to "_graphviz" will open a new window if it
     * doesn't already exist, or reuse it if it does. If undefined, the value of
     * the target is used.
     *
     * @param value The edge target.
     * @return This object.
     */
    public GvEdgeAttributes setEdgeTarget(String value) {
        return setValue(GvAttributeName.EDGE_TARGET, value);
    }

    /**
     * Tooltip annotation attached to the non-label part of an edge. This is
     * used only if the edge has a URL or edgeURL attribute.
     *
     * @param value The edge tooltip.
     * @return This object.
     */
    public GvEdgeAttributes setEdgeTooltip(String value) {
        return setValue(GvAttributeName.EDGE_TOOLTIP, value);
    }

    /**
     * If edgeURL is defined, this is the link used for the non-label parts of
     * an edge. This value overrides any URL defined for the edge. Also, this value
     * is used near the head or tail node unless overridden by a headURL or
     * tailURL value, respectively. See limitation.
     *
     * @param value The edge URL.
     * @return This object.
     */
    public GvEdgeAttributes setEdgeURL(String value) {
        return setValue(GvAttributeName.EDGE_URL, value);
    }

    @Override
    public GvEdgeAttributes setFillColor(GvColor value) {
        return super.setFillColor(value);
    }

    @Override
    public GvEdgeAttributes setFillColor(GvColor... values) {
        return super.setFillColor(values);
    }

    @Override
    public GvEdgeAttributes setFillColor(GvColorList value) {
        return super.setFillColor(value);
    }

    @Override
    public GvEdgeAttributes setFontColor(GvColor value) {
        return super.setFontColor(value);
    }

    @Override
    public GvEdgeAttributes setFontName(String value) {
        return super.setFontName(value);
    }

    @Override
    public GvEdgeAttributes setFontSize(double value) {
        return super.setFontSize(value);
    }

    /**
     * If true, the head of an edge is clipped to the boundary of the head node;
     * otherwise, the end of the edge goes to the center of the node, or the
     * center of a port, if applicable.
     *
     * @param value true to clip edge head.
     * @return This object.
     */
    public GvEdgeAttributes setHeadClip(boolean value) {
        return setValue(GvAttributeName.HEAD_CLIP, value);
    }

    /**
     * Synonym for headURL.
     *
     * @param value The URL.
     * @return This object.
     */
    public GvEdgeAttributes setHeadHRef(String value) {
        return setValue(GvAttributeName.HEAD_HREF, value);
    }

    /**
     * Text label to be placed near head of edge.
     *
     * @param value The head label.
     * @return This object.
     */
    public GvEdgeAttributes setHeadLabel(String value) {
        return setValue(GvAttributeName.HEAD_LABEL, value);
    }

    /**
     * Position of an edge's head label, in points. The position indicates the
     * center of the label.
     *
     * @param value The head label position.
     * @return This object.
     */
    public GvEdgeAttributes setHeadLabelPosition(GvPoint2 value) {
        return setValue(GvAttributeName.HEAD_LP, value.encode());
    }

    /**
     * Indicates where on the head node to attach the head of the edge. In the
     * default case, the edge is aimed towards the center of the node, and then
     * clipped at the node boundary.
     *
     * @param name The port name.
     * @param compass The compass point.
     * @return This object.
     */
    public GvEdgeAttributes setHeadPort(String name,
                                        GvCompassPoint compass) {
        return setValue(GvAttributeName.HEAD_PORT, encode(name, compass));
    }

    /**
     * If the edge has a headURL, this attribute determines which window of the
     * browser is used for the URL. Setting it to "_graphviz" will open a new
     * window if it doesn't already exist, or reuse it if it does. If undefined,
     * the value of the target is used.
     *
     * @param value The head target
     * @return This object.
     */
    public GvEdgeAttributes setHeadTarget(String value) {
        return setValue(GvAttributeName.HEAD_TARGET, value);
    }

    /**
     * Tooltip annotation attached to the head of an edge. This is used only if
     * the edge has a headURL attribute.
     *
     * @param value The head tooltip.
     * @return This object.
     */
    public GvEdgeAttributes setHeadTooltip(String value) {
        return setValue(GvAttributeName.HEAD_TOOLTIP, value);
    }

    /**
     * If headURL is defined, it is output as part of the head label of the
     * edge.
     * Also, this value is used near the head node, overriding any URL value.
     * See limitation.
     *
     * @param value The head URL.
     * @return This object.
     */
    public GvEdgeAttributes setHeadURL(String value) {
        return setValue(GvAttributeName.HEAD_URL, value);
    }

    @Override
    public GvEdgeAttributes setHRef(String value) {
        return super.setHRef(value);
    }

    @Override
    public GvEdgeAttributes setId(String value) {
        return super.setId(value);
    }

    @Override
    public GvEdgeAttributes setLabel(String value) {
        return super.setLabel(value);
    }

    /**
     * This, along with labeldistance, determine where the headlabel (taillabel)
     * are placed with respect to the head (tail) in polar coordinates. The
     * origin in the coordinate system is the point where the edge touches the
     * node. The ray of 0 degrees goes from the origin back along the edge,
     * parallel to the edge at the origin.
     * <p>
     * The angle, in degrees, specifies the rotation from the 0 degree ray, with
     * positive angles moving counterclockwise and negative angles moving
     * clockwise.
     *
     * @param value The label angle.
     * @return This object.
     */
    public GvEdgeAttributes setLabelAngle(double value) {
        return setValue(GvAttributeName.LABEL_ANGLE, value);
    }

    /**
     * Multiplicative scaling factor adjusting the distance that the
     * headlabel(taillabel) is from the head(tail) node. The default distance is
     * 10 points. See labelangle for more details.
     *
     * @param value The label distance.
     * @return This object.
     */
    public GvEdgeAttributes setLabelDistance(double value) {
        return setValue(GvAttributeName.LABEL_DISTANCE, value);
    }

    /**
     * If true, allows edge labels to be less constrained in position. In
     * particular, it may appear on top of other edges.
     *
     * @param value true to let the label float.
     * @return This object.
     */
    public GvEdgeAttributes setLabelFloat(boolean value) {
        return setValue(GvAttributeName.LABEL_FLOAT, value);
    }

    /**
     * Color used for headlabel and taillabel. If not set, defaults to edge's
     * fontcolor.
     *
     * @param value The label font color.
     * @return This object.
     */
    public GvEdgeAttributes setLabelFontColor(GvColor value) {
        return setValue(GvAttributeName.LABEL_FONT_COLOR, value.encode());
    }

    /**
     * Font used for headlabel and taillabel. If not set, defaults to edge's
     * fontname.
     *
     * @param value The label font name.
     * @return This object.
     */
    public GvEdgeAttributes setLabelFontName(String value) {
        return setValue(GvAttributeName.LABEL_FONT_NAME, value);
    }

    /**
     * Font size, in points, used for headlabel and taillabel. If not set,
     * defaults to edge's fontsize.
     *
     * @param value The label font size.
     * @return This object.
     */
    public GvEdgeAttributes setLabelFontSize(double value) {
        return setValue(GvAttributeName.LABEL_FONT_SIZE, value);
    }

    /**
     * Synonym for labelURL.
     *
     * @param value The label URL.
     * @return This object.
     */
    public GvEdgeAttributes setLabelHRef(String value) {
        return setValue(GvAttributeName.LABEL_HREF, value);
    }

    @Override
    public GvEdgeAttributes setLabelPosition(GvPoint2 value) {
        return super.setLabelPosition(value);
    }

    /**
     * If the edge has a URL or labelURL attribute, this attribute determines
     * which window of the browser is used for the URL attached to the label.
     * Setting it to "_graphviz" will open a new window if it doesn't already
     * exist, or reuse it if it does. If undefined, the value of the target is
     * used.
     *
     * @param value The label target.
     * @return This object.
     */
    public GvEdgeAttributes setLabelTarget(String value) {
        return setValue(GvAttributeName.LABEL_TARGET, value);
    }

    /**
     * Tooltip annotation attached to label of an edge. This is used only if the
     * edge has a URL or labelURL attribute.
     *
     * @param value The label tooltip.
     * @return This object.
     */
    public GvEdgeAttributes setLabelTooltip(String value) {
        return setValue(GvAttributeName.LABEL_TOOLTIP, value);
    }

    /**
     * If labelURL is defined, this is the link used for the label of an edge.
     * This value overrides any URL defined for the edge.
     *
     * @param value The label URL.
     * @return This object.
     */
    public GvEdgeAttributes setLabelURL(String value) {
        return setValue(GvAttributeName.LABEL_URL, value);
    }

    @Override
    public GvEdgeAttributes setLayer(String value) {
        return super.setLayer(value);
    }

    /**
     * Preferred edge length, in inches.
     *
     * @param value The edge length.
     * @return This object.
     */
    public GvEdgeAttributes setLen(double value) {
        return super.setValue(GvAttributeName.LEN, value);
    }

    /**
     * Logical head of an edge. When compound is true, if lhead is defined and
     * is the name of a cluster containing the real head, the edge is clipped to
     * the boundary of the cluster. See limitation.
     *
     * @param value Cluster name.
     * @return This object.
     */
    public GvEdgeAttributes setLogicalHead(String value) {
        return super.setValue(GvAttributeName.LHEAD, GvSupport.toClusterId(value));
    }

    /**
     * Logical tail of an edge. When compound is true, if ltail is defined and
     * is the name of a cluster containing the real tail, the edge is clipped to
     * the boundary of the cluster. See limitation.
     *
     * @param value Cluster name.
     * @return This object.
     */
    public GvEdgeAttributes setLogicalTail(String value) {
        return super.setValue(GvAttributeName.LTAIL, GvSupport.toClusterId(value));
    }

    /**
     * Minimum edge length (rank difference between head and tail).
     *
     * @param value The minimum edge length.
     * @return This object.
     */
    public GvEdgeAttributes setMinLen(int value) {
        return super.setValue(GvAttributeName.MIN_LEN, value);
    }

    @Override
    public GvEdgeAttributes setNoJustify(boolean value) {
        return super.setNoJustify(value);
    }

    @Override
    public GvEdgeAttributes setPenWidth(double value) {
        return super.setPenWidth(value);
    }

    /**
     * Edges with the same head and the same samehead value are aimed at the
     * same point on the head. This has no effect on loops. Each node can have at
     * most 5 unique samehead values. See limitation.
     *
     * @param value The head name.
     * @return This object.
     */
    public GvEdgeAttributes setSameHead(String value) {
        return setValue(GvAttributeName.SAME_HEAD, value);
    }

    /**
     * Edges with the same tail and the same sametail value are aimed at the
     * same point on the tail. This has no effect on loops. Each node can have at
     * most 5 unique sametail values. See limitation.
     *
     * @param value The tail name.
     * @return This object.
     */
    public GvEdgeAttributes setSameTail(String value) {
        return setValue(GvAttributeName.SAME_TAIL, value);
    }

    /**
     * Set style information for components of the graph. For cluster subgraphs,
     * if style="filled", the cluster box's background is filled.
     * <p>
     * If the default style attribute has been set for a component, an
     * individual
     * component can use style="" to revert to the normal default. For example,
     * if the graph has
     * <p>
     * <code>edge [style="invis"]</code>
     * <p>
     * making all edges invisible, a specific edge can overrride this via
     * <p>
     * <code>{@literal a -> b [style=""]}</code>
     * <p>
     * Of course, the component can also explicitly set its style attribute to
     * the desired value.
     *
     * @param values The edge styles.
     * @return This object.
     */
    public GvEdgeAttributes setStyle(GvEdgeStyle... values) {
        return setValue(GvAttributeName.STYLE, encode(",", values));
    }

    /**
     * If true, the tail of an edge is clipped to the boundary of the tail node;
     * otherwise, the end of the edge goes to the center of the node, or the
     * center of a port, if applicable.
     *
     * @param value true to clip tail.
     * @return This object.
     */
    public GvEdgeAttributes setTailClip(boolean value) {
        return setValue(GvAttributeName.TAIL_CLIP, value);
    }

    /**
     * Synonym for tailURL.
     *
     * @param value The tail URL.
     * @return This object.
     */
    public GvEdgeAttributes setTailHRef(String value) {
        return setValue(GvAttributeName.TAIL_HREF, value);
    }

    /**
     * Text label to be placed near tail of edge.
     *
     * @param value The tail label.
     * @return This object.
     */
    public GvEdgeAttributes setTailLabel(String value) {
        return setValue(GvAttributeName.TAIL_LABEL, value);
    }

    /**
     * Position of an edge's tail label, in points. The position indicates the
     * center of the label.
     *
     * @param value The tail label position.
     * @return This object.
     */
    public GvEdgeAttributes setTailLabelPosition(GvPoint2 value) {
        return setValue(GvAttributeName.TAIL_LP, value.encode());
    }

    /**
     * Indicates where on the tail node to attach the tail of the edge.
     * <p>
     * Associated to a String and a GvCompassPoint.
     *
     * @param name The port name.
     * @param compass The compass point.
     * @return This object.
     */
    public GvEdgeAttributes setTailPort(String name,
                                        GvCompassPoint compass) {
        return setValue(GvAttributeName.TAIL_PORT, encode(name, compass));
    }

    /**
     * If the edge has a tailURL, this attribute determines which window of the
     * browser is used for the URL. Setting it to "_graphviz" will open a new
     * window if it doesn't already exist, or reuse it if it does. If undefined,
     * the value of the target is used.
     *
     * @param value The tail target.
     * @return This object.
     */
    public GvEdgeAttributes setTailTarget(String value) {
        return setValue(GvAttributeName.TAIL_TARGET, value);
    }

    /**
     * Tooltip annotation attached to the tail of an edge. This is used only if
     * the edge has a tailURL attribute.
     *
     * @param value The tail tooltip.
     * @return This object.
     */
    public GvEdgeAttributes setTailTooltip(String value) {
        return setValue(GvAttributeName.TAIL_TOOLTIP, value);
    }

    /**
     * If tailURL is defined, it is output as part of the tail label of the
     * edge.
     * Also, this value is used near the tail node, overriding any URL value.
     * See limitation.
     *
     * @param value The tail URL.
     * @return This object.
     */
    public GvEdgeAttributes setTailURL(String value) {
        return setValue(GvAttributeName.TAIL_URL, value);
    }

    @Override
    public GvEdgeAttributes setTarget(String value) {
        return super.setTarget(value);
    }

    @Override
    public GvEdgeAttributes setTooltip(String value) {
        return super.setTooltip(value);
    }

    @Override
    public GvEdgeAttributes setURL(String value) {
        return super.setURL(value);
    }

    /**
     * Weight of edge. In dot, the heavier the weight, the shorter, straighter
     * and more vertical the edge is. N.B. Weights in dot must be integers. For
     * twopi, a weight of 0 indicates the edge should not be used in
     * constructing a spanning tree from the root. For other layouts, a larger weight
     * encourages the layout to make the edge length closer to that specified by
     * the len attribute.
     *
     * @param value The edge weight.
     * @return This object.
     */
    public GvEdgeAttributes setWeight(double value) {
        return setValue(GvAttributeName.WEIGHT, value);
    }

    @Override
    public GvEdgeAttributes setXLabel(String value) {
        return super.setXLabel(value);
    }

    @Override
    public GvEdgeAttributes setXLabelPosition(double value) {
        return super.setXLabelPosition(value);
    }
}