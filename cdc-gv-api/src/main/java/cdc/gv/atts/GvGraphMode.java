package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible graph modes.
 *
 * @author Damien Carbonne
 *
 */
public enum GvGraphMode implements GvEncodable {
    MAJOR,
    KK,
    HIER,
    IPSEP,
    SPRING,
    MAXENT;

    @Override
    public String encode() {
        if (this == KK) {
            return "KK";
        } else {
            return name().toLowerCase();
        }
    }
}