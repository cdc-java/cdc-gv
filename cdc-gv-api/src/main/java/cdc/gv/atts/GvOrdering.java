package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible orderings.
 *
 * @author Damien Carbonne
 *
 */
public enum GvOrdering implements GvEncodable {
    IN,
    OUT;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}