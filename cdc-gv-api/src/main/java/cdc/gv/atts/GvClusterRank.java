package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible cluster ranks.
 *
 * @author Damien Carbonne
 *
 */
public enum GvClusterRank implements GvEncodable {
    LOCAL,
    GLOBAL,
    NONE;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}