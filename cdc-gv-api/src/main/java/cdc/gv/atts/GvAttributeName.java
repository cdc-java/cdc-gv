package cdc.gv.atts;

import static cdc.gv.atts.GvAttributeUsage.C;
import static cdc.gv.atts.GvAttributeUsage.E;
import static cdc.gv.atts.GvAttributeUsage.G;
import static cdc.gv.atts.GvAttributeUsage.N;
import static cdc.gv.atts.GvAttributeUsage.S;

import cdc.gv.support.GvEncodable;
import cdc.util.lang.IntMasks;

/**
 * Enumeration of possible attribute names. This enumeration is incomplete. It
 * should be completed as necessary. It is however possible to use attribute
 * that are not defined here by using the extension mechanism. Attributes can be
 * set for certain targets only.
 *
 * @author Damien Carbonne
 */
public enum GvAttributeName implements GvEncodable {
    AREA(N, C),
    ARROW_HEAD(E),
    ARROW_SIZE(E),
    ARROW_TAIL(E),
    BACKGROUND("_background", G),
    BB(G),
    BG_COLOR(G, C),
    CENTER(G),
    CHARSET(G),
    CLUSTER_RANK(G),
    COLOR(E, N, C),
    COLOR_SCHEME(E, N, C, G),
    COMMENT(E, N, G),
    COMPOUND(G),
    CONCENTRATE(G),
    CONSTRAINT(E),
    DAMPING("Damping", G),
    DECORATE(E),
    DEFAULT_DIST(G),
    DIM(G),
    DIMEN(G),
    DIR(E),
    DIR_EDGE_CONSTRAINT(G),
    DISTORTION(N),
    DPI(G),
    EDGE_URL("edgeURL", E),
    EDGE_HREF(E),
    EDGE_TARGET(E),
    EDGE_TOOLTIP(E),
    EPSILON(G),
    ESEP(G),
    FILL_COLOR(N, E, C),
    FIXED_SIZE(N),
    FONT_COLOR(N, E, G, C),
    FONT_NAME(N, E, G, C),
    FONT_NAMES(G),
    FONT_PATH(G),
    FONT_SIZE(N, E, G, C),
    FORCE_LABELS(G),
    GRADIENT_ANGLE(N, C, G),
    GROUP(N),
    HEAD_CLIP(E),
    HEAD_HREF(E),
    HEAD_LABEL(E),
    HEAD_LP("head_lp", E),
    HEAD_PORT(E),
    HEAD_TARGET(E),
    HEAD_TOOLTIP(E),
    HEAD_URL("headURL", E),
    HEIGHT(N),
    HREF(G, C, N, E),
    ID(G, C, N, E),
    IMAGE(N),
    IMAGE_PATH(G),
    IMAGE_SCALE(N),
    INPUT_SCALE(G),
    K("K", G, C),
    LABEL(N, E, G, C),
    LABEL_ANGLE(E),
    LABEL_DISTANCE(E),
    LABEL_FLOAT(E),
    LABEL_FONT_COLOR(E),
    LABEL_FONT_NAME(E),
    LABEL_FONT_SIZE(E),
    LABEL_HREF(E),
    LABEL_JUST(G, C),
    LABEL_LOC(N, G, C),
    LABEL_SCHEME("label_scheme", G),
    LABEL_TARGET(E),
    LABEL_TOOLTIP(E),
    LABEL_URL("labelURL", E),
    LANDSCAPE(G),
    LAYER(E, N, C),
    LAYER_LIST_SEP(G),
    LAYER_SELECT(G),
    LAYER_SEP(G),
    LAYERS(G),
    LAYOUT(G),
    LEN(E),
    LEVELS(G),
    LEVELS_GAP(G),
    LHEAD(E),
    LHEIGHT(G, C),
    LP(E, G, C),
    LTAIL(E),
    LWIDTH(G, C),
    MARGIN(N, C, G),

    /**
     * Sets the number of iterations used.
     */
    MAX_ITER(N, G, C), // TODO

    MC_LIMIT(G),
    MIN_DIST(G),
    MIN_LEN(E),
    MODE(G),
    MODEL(G),
    MOSEK(G),
    NODE_SEP(G),
    NO_JUSTIFY(G, C, N, E),
    NORMALIZE(G),
    NO_TRANSLATE(G),
    NS_LIMIT(G),
    NS_LIMIT1(G),
    ORDERING(G, N),
    ORIENTATION(G, N),
    OUTPUT_ORDER(G),
    OVERLAP(G),
    OVERLAP_SCALING(G),
    OVERLAP_SHRINK(G),
    PACK(G),
    PACK_MODE("packMode", G),
    PAD(G),
    PAGE(G),
    PAGE_DIR(G),
    PEN_COLOR(C),
    PEN_WIDTH(C, N, E),
    PERIPHERIES(N, C),

    /**
     * If true and the node has a pos attribute on input, neato or fdp prevents
     * the node from moving from the input position. This property can also be
     * specified in the pos attribute itself (cf. the point type).
     * <p>
     * Note: Due to an artifact of the implementation, previous to 27 Feb 2014,
     * final coordinates are translated to the origin. Thus, if you look at the
     * output coordinates given in the (x)dot or plain format, pinned nodes will
     * not have the same output coordinates as were given on input. If this is
     * important, a simple workaround is to maintain the coordinates of a pinned
     * node. The vector difference between the old and new coordinates will give
     * the translation, which can then be subtracted from all of the appropriate
     * coordinates.
     * <p>
     * After 27 Feb 2014, this translation can be avoided in neato by setting
     * the notranslate to TRUE. However, if the graph specifies node overlap removal
     * or a change in aspect ratio, node coordinates may still change.
     */
    PIN(N), // TODO

    /**
     * Position of node, or spline control points. For nodes, the position
     * indicates the center of the node. On output, the coordinates are in
     * points.
     * <p>
     * In neato and fdp, pos can be used to set the initial position of a node.
     * By default, the coordinates are assumed to be in inches. However, the -s
     * command line flag can be used to specify different units. As the output
     * coordinates are in points, feeding the output of a graph laid out by a
     * Graphviz program into neato or fdp will almost always require the -s
     * flag.
     * <p>
     * When the -n command line flag is used with neato, it is assumed the
     * positions have been set by one of the layout programs, and are therefore
     * in points. Thus, neato -n can accept input correctly without requiring a
     * -s flag and, in fact, ignores any such flag.
     */
    POS(E, N), // TODO

    QUAD_TREE(G),
    QUANTUM(G),
    RANK(S, C),
    RANK_DIR(G),
    RANK_SEP(G),
    RATIO(G),

    /**
     * Rectangles for fields of records, in points.
     */
    RECTS(N), // TODO

    REGULAR(N),
    REMIN_CROSS(G),
    REPULSIVE_FORCE(G),

    /**
     * This is a synonym for the dpi attribute.
     */
    RESOLUTION(G), // TODO

    /**
     * This specifies nodes to be used as the center of the layout and the root
     * of the generated spanning tree. As a graph attribute, this gives the name
     * of the node. As a node attribute, it specifies that the node should be
     * used as a central node. In twopi, this will actually be the central node.
     * In circo, the block containing the node will be central in the drawing of
     * its connected component. If not defined, twopi will pick a most central
     * node, and circo will pick a random node.
     * <p>
     * If the root attribute is defined as the empty string, twopi will reset it
     * to name of the node picked as the root node.
     * <p>
     * For twopi, it is possible to have multiple roots, presumably one for each
     * component. If more than one node in a component is marked as the root,
     * twopi will pick one.
     */
    ROOT(G, N), // TODO

    /**
     * If 90, set drawing orientation to landscape.
     */
    ROTATE(G), // TODO

    /**
     * Causes the final layout to be rotated counter-clockwise by the specified
     * number of degrees.
     */
    ROTATION(G), // TODO

    SAME_HEAD(E),
    SAME_TAIL(E),

    /**
     * If the input graph defines the vertices attribute, and output is dot or
     * xdot, this gives the number of points used for a node whose shape is a
     * circle or ellipse. It plays the same role in neato, when adjusting the
     * layout to avoid overlapping nodes, and in image maps.
     */
    SAMPLE_POINTS(N), // TODO

    SCALE(G),

    /**
     * During network simplex, maximum number of edges with negative cut values
     * to search when looking for one with minimum cut value.
     */
    SEARCH_SIZE(G), // TODO

    SEP(G),
    SHAPE(N),
    SHAPE_FILE(N),

    /**
     * Print guide boxes in PostScript at the beginning of routesplines if 1, or
     * at the end if 2. (Debugging)
     */
    SHOW_BOXES(E, N, G), // TODO

    SIDES(N),
    SIZE(G),
    SKEW(N),
    SMOOTHING(G),

    /**
     * If packmode indicates an array packing, this attribute specifies an
     * insertion order among the components, with smaller values inserted first.
     */
    SORTV(G, C, N), // TODO

    SPLINES(G),

    /**
     * Parameter used to determine the initial layout of nodes. If unset, the
     * nodes are randomly placed in a unit square with the same seed is always
     * used for the random number generator, so the initial placement is
     * repeatable.
     */
    START(G), // TODO

    STYLE(E, N, G, C),
    STYLE_SHEET(G),
    TAIL_CLIP(E),
    TAIL_HREF(E),
    TAIL_LABEL(E),
    TAIL_LP(E),
    TAIL_PORT(E),
    TAIL_TARGET(E),
    TAIL_TOOLTIP(E),
    TAIL_URL("tailURL", E),
    TARGET(E, N, G, C),
    TOOLTIP(N, E, C),
    TRUE_COLOR(G),
    URL("URL", E, N, G, C),

    /**
     * If the input graph defines this attribute, the node is polygonal, and
     * output is dot or xdot, this attribute provides the coordinates of the
     * vertices of the node's polygon, in inches. If the node is an ellipse or
     * circle, the samplepoints attribute affects the output.
     */
    VERTICES(N), // TODO

    /**
     * Clipping window on final drawing. Note that this attribute supersedes any
     * size attribute. The width and height of the viewport specify precisely
     * the final size of the output.
     */
    VIEWPORT(G), // TODO

    /**
     * Factor to scale up drawing to allow margin for expansion in Voronoi
     * technique. dim' = (1+2*margin)*dim.
     */
    VORO_MARGIN("voro_margin", G), // TODO

    WEIGHT(E),
    WIDTH(N),

    /**
     * For xdot output, if this attribute is set, this determines the version of
     * xdot used in output. If not set, the attribute will be set to the xdot
     * version used for output.
     */
    XDOT_VERSION(G), // TODO

    XLABEL(E, N),
    XLP(N, E)

    // Z(N) Deprecated
    ;

    private final String code;
    private final int usages;

    private GvAttributeName(GvAttributeUsage... usages) {
        this.code = name().toLowerCase().replace("_", "");
        this.usages = IntMasks.toMask(usages);
    }

    private GvAttributeName(String code,
                            GvAttributeUsage... usages) {
        this.code = code;
        this.usages = IntMasks.toMask(usages);
    }

    /**
     * Returns whether the attribute can be used for a given usage.
     *
     * @param usage The tested usage.
     * @return whether the attribute can be used for usage.
     */
    public final boolean isSupportedFor(GvAttributeUsage usage) {
        return IntMasks.isEnabled(usages, usage);
    }

    /**
     * Returns the delimiter to be used for the attribute name and a value.
     * <p>
     * For a LABEL and when value starts with {@literal "<<"}, returns "".<br>
     * Otherwise returns "\"".
     *
     * @param value The attribute value.
     * @return The delimiter to be used around value.
     */
    public String getDelimiter(String value) {
        if (this == LABEL) {
            if (value.startsWith("<<")) {
                return "";
            } else {
                return "\"";
            }
        } else {
            return "\"";
        }
    }

    @Override
    public String encode() {
        return code;
    }
}