package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible compass points.
 *
 * @author Damien Carbonne
 *
 */
public enum GvCompassPoint implements GvEncodable {
   N,
   E,
   S,
   W,
   NE,
   NW,
   SE,
   SW,
   C;

   @Override
   public String encode() {
      return name().toLowerCase();
   }
}