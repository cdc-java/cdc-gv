package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible pack modes.
 *
 * @author Damien Carbonne
 *
 */
public enum GvPackMode implements GvEncodable {
    NODE,
    CLUST,
    GRAPH,
    ARRAY;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}