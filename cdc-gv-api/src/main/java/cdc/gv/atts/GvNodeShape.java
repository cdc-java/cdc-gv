package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible node shapes.
 *
 * @author Damien Carbonne
 *
 */
public enum GvNodeShape implements GvEncodable {
    BOX,
    POLYGON,
    ELLIPSE,
    OVAL,
    CIRCLE,
    POINT,
    EGG,
    TRIANGLE,
    PLAIN_TEXT,
    DIAMOND,
    TRAPEZIUM,
    PARALLELOGRAM,
    HOUSE,
    PENTAGON,
    HEXAGON,
    SEPTAGON,
    OCTAGON,
    DOUBLE_CIRCLE,
    DOUBLE_OCTAGON,
    TRIPLE_OCTAGON,
    INV_TRIANGLE,
    INV_TRAPEZIUM,
    INV_HOUSE,
    M_DIAMOND,
    M_SQUARE,
    M_CIRCLE,
    RECT,
    RECTANGLE,
    SQUARE,
    NONE,
    UNDERLINE,
    NOTE,
    TAB,
    FOLDER,
    BOX3D,
    COMPONENT,
    PROMOTER,
    CDS,
    TERMINATOR,
    UTR,
    PRIMER_SITE,
    RESTRICTION_SITE,
    FIVE_POVER_HANG,
    THREE_POVER_HANG,
    N_OVER_HANG,
    ASSEMBLY,
    SIGNATURE,
    INSULATOR,
    RIBOSITE,
    RNA_STAB,
    PROTEASE_SITE,
    PROTEIN_STAB,
    R_PROMOTER,
    R_ARROW,
    L_ARROW,
    L_PROMOTER,
    RECORD,
    M_RECORD;

    @Override
    public String encode() {
        switch (this) {
        case M_CIRCLE:
            return "Mcircle";
        case M_DIAMOND:
            return "Mdiamond";
        case M_SQUARE:
            return "Msquare";
        case M_RECORD:
            return "Mrecord";
        default:
            return name().toLowerCase().replaceAll("_", "");
        }
    }
}