package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible subgraph rank types.
 *
 * @author Damien Carbonne
 *
 */
public enum GvRankType implements GvEncodable {
    SAME,
    MIN,
    SOURCE,
    MAX,
    SINK;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}