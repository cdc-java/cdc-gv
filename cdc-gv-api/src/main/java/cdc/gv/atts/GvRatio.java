package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible graph ratios.
 *
 * @author Damien Carbonne
 *
 */
public enum GvRatio implements GvEncodable {
    FILL,
    COMPRESS,
    EXPAND,
    AUTO;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}