package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible cluster styles.
 *
 * @author Damien Carbonne
 *
 */
public enum GvClusterStyle implements GvEncodable {
    FILLED,
    RADIAL,
    ROUNDED,
    STRIPED;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}