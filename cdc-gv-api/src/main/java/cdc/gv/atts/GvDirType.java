package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible edge direction types.
 *
 * @author Damien Carbonne
 *
 */
public enum GvDirType implements GvEncodable {
    BACK,
    BOTH,
    FORWARD,
    NONE;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}