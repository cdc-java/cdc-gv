package cdc.gv.atts;

/**
 * Definition of subgraph attributes.
 *
 * @author Damien Carbonne
 *
 */
public final class GvSubgraphAttributes extends GvAttributes<GvSubgraphAttributes> {
    public GvSubgraphAttributes() {
        super(GvAttributeUsage.SUBGRAPH);
    }

    @Override
    protected GvSubgraphAttributes self() {
        return this;
    }

    @Override
    public GvSubgraphAttributes setRank(GvRankType value) {
        return super.setRank(value);
    }
}