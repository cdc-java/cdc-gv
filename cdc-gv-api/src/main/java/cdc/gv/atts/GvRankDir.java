package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible rank directions.
 *
 * @author Damien Carbonne
 *
 */
public enum GvRankDir implements GvEncodable {
    TB,
    LR,
    BT,
    RL;

    @Override
    public String encode() {
        return name();
    }
}