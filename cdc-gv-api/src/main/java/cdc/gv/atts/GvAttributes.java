package cdc.gv.atts;

import cdc.gv.colors.GvColor;
import cdc.gv.support.GvBaseAttributes;

/**
 * Base class used to define a set of attributes (for graphs, clusters, nodes,
 * ...).
 * <p>
 * This class contains setters for attributes that have at least 2 usages.
 * Attributes that have only one usage are handled directly in
 * the appropriate class (subclass of GvAttributes).
 *
 * @author Damien Carbonne
 * @param <A> Concrete implementation.
 */
public abstract class GvAttributes<A extends GvAttributes<A>> extends GvBaseAttributes<A, GvAttributeName, GvAttributeUsage> {

    /**
     * Create a set of GvAttributes for a given usage.
     *
     * @param usage The usage for which this set of attributes is intended.
     */
    protected GvAttributes(GvAttributeUsage usage) {
        super(GvAttributeName.class, GvAttributeUsage.class, usage);
    }

    @Override
    protected final boolean isSupported(GvAttributeName name,
                                        GvAttributeUsage usage) {
        return name.isSupportedFor(usage);
    }

    /**
     * Indicates the preferred area for a node or empty cluster when laid out by
     * patchwork.
     *
     * @param value The area.
     * @return This object.
     */
    protected A setArea(double value) {
        return setValue(GvAttributeName.AREA, value);
    }

    /**
     * When attached to the root graph, this color is used as the background for
     * entire canvas. When a cluster attribute, it is used as the initial
     * background for the cluster.
     * <p>
     * If a cluster has a filled style, the cluster's fillcolor will overlay the
     * background color. If the value is a colorList, a gradient fill is used.
     * By
     * default, this is a linear fill; setting style=radial will cause a radial
     * fill. At present, only two colors are used. If the second color (after a
     * colon) is missing, the default color is used for it. See also the
     * gradientangle attribute for setting the gradient angle.
     * <p>
     * For certain output formats, such as PostScript, no fill is done for the
     * root graph unless bgcolor is explicitly set. For bitmap formats, however,
     * the bits need to be initialized to something, so the canvas is filled
     * with
     * white by default. This means that if the bitmap output is included in
     * some
     * other document, all of the bits within the bitmap's bounding box will be
     * set, overwriting whatever color or graphics were already on the page. If
     * this effect is not desired, and you only want to set bits explicitly
     * assigned in drawing the graph, set bgcolor="transparent".
     *
     * @param value The color.
     * @return This object.
     */
    protected A setBgColor(GvColor value) {
        return setValue(GvAttributeName.BG_COLOR, value == null ? null : value.encode());
    }

    /**
     * Basic drawing color for graphics, not text. For the latter, use the
     * fontcolor attribute.
     * <p>
     * For edges, the value can either be a single color or a colorList. In the
     * latter case, if colorList has no fractions, the edge is drawn using
     * parallel splines or lines, one for each color in the list, in the order
     * given. The head arrow, if any, is drawn using the first color in the
     * list,
     * and the tail arrow, if any, the second color. This supports the common
     * case of drawing opposing edges, but using parallel splines instead of
     * separately routed multiedges. If any fraction is used, the colors are
     * drawn in series, with each color being given roughly its specified
     * fraction of the edge.
     *
     * @param value The color.
     * @return This object.
     */
    protected A setColor(GvColor value) {
        return setValue(GvAttributeName.COLOR, value == null ? null : value.encode());
    }

    /**
     * Basic drawing color for graphics, not text. For the latter, use the
     * fontcolor attribute.
     * <p>
     * For edges, the value can either be a single color or a colorList. In the
     * latter case, if colorList has no fractions, the edge is drawn using
     * parallel splines or lines, one for each color in the list, in the order
     * given. The head arrow, if any, is drawn using the first color in the
     * list,
     * and the tail arrow, if any, the second color. This supports the common
     * case of drawing opposing edges, but using parallel splines instead of
     * separately routed multiedges. If any fraction is used, the colors are
     * drawn in series, with each color being given roughly its specified
     * fraction of the edge.
     *
     * @param values The colors.
     * @return This object.
     */
    protected A setColor(GvColor... values) {
        return setValue(GvAttributeName.COLOR, encode(":", values));
    }

    protected A setColor(GvColorList value) {
        return setValue(GvAttributeName.COLOR, value.encode());
    }

    /**
     * This attribute specifies a color scheme namespace. If defined, it
     * specifies the context for interpreting color names. In particular, if a
     * color value has form "xxx" or "//xxx", then the color xxx will be
     * evaluated according to the current color scheme. If no color scheme is
     * set, the standard X11 naming is used. For example, if colorscheme=bugn9,
     * then color=7 is interpreted as "/bugn9/7".
     *
     * @param value The color scheme.
     * @return This object.
     */
    protected A setColorScheme(String value) {
        return setValue(GvAttributeName.COLOR_SCHEME, value);
    }

    /**
     * Comments are inserted into output. Device-dependent
     *
     * @param value The comment.
     * @return This object.
     */
    protected A setComment(String value) {
        return setValue(GvAttributeName.COMMENT, value);
    }

    /**
     * Color used to fill the background of a node or cluster assuming
     * style=filled, or a filled arrowhead. If fillcolor is not defined, color
     * is
     * used. (For clusters, if color is not defined, bgcolor is used.) If this
     * is
     * not defined, the default is used, except for shape=point or when the
     * output format is MIF, which use black by default.
     * <p>
     * If the value is a colorList, a gradient fill is used. By default, this is
     * a linear fill; setting style=radial will cause a radial fill. At present,
     * only two colors are used. If the second color (after a colon) is missing,
     * the default color is used for it. See also the gradientangle attribute
     * for
     * setting the gradient angle.
     * <p>
     * Note that a cluster inherits the root graph's attributes if defined.
     * Thus,
     * if the root graph has defined a fillcolor, this will override a color or
     * bgcolor attribute set for the cluster.
     *
     * @param value The color.
     * @return This object.
     */
    protected A setFillColor(GvColor value) {
        return setValue(GvAttributeName.FILL_COLOR, value == null ? null : value.encode());
    }

    /**
     * Color used to fill the background of a node or cluster assuming
     * style=filled, or a filled arrowhead. If fillcolor is not defined, color
     * is
     * used. (For clusters, if color is not defined, bgcolor is used.) If this
     * is
     * not defined, the default is used, except for shape=point or when the
     * output format is MIF, which use black by default.
     * <p>
     * If the value is a colorList, a gradient fill is used. By default, this is
     * a linear fill; setting style=radial will cause a radial fill. At present,
     * only two colors are used. If the second color (after a colon) is missing,
     * the default color is used for it. See also the gradientangle attribute
     * for
     * setting the gradient angle.
     * <p>
     * Note that a cluster inherits the root graph's attributes if defined.
     * Thus,
     * if the root graph has defined a fillcolor, this will override a color or
     * bgcolor attribute set for the cluster.
     *
     * @param values The colors.
     * @return This object.
     */
    protected A setFillColor(GvColor... values) {
        return setValue(GvAttributeName.FILL_COLOR, encode(":", values));
    }

    protected A setFillColor(GvColorList value) {
        return setValue(GvAttributeName.FILL_COLOR, value.encode());
    }

    /**
     * Color used for text.
     *
     * @param value The color.
     * @return This object.
     */
    protected A setFontColor(GvColor value) {
        return setValue(GvAttributeName.FONT_COLOR, value == null ? null : value.encode());
    }

    /**
     * Font used for text. This very much depends on the output format and, for
     * non-bitmap output such as PostScript or SVG, the availability of the font
     * when the graph is displayed or printed. As such, it is best to rely on
     * font faces that are generally available, such as Times-Roman, Helvetica
     * or
     * Courier.
     * <p>
     * How font names are resolved also depends on the underlying library that
     * handles font name resolution. If Graphviz was built using the fontconfig
     * library, the latter library will be used to search for the font. See the
     * commands fc-list, fc-match and the other fontconfig commands for how
     * names
     * are resolved and which fonts are available. Other systems may provide
     * their own font package, such as Quartz for OS X.
     * <p>
     * Note that various font attributes, such as weight and slant, can be built
     * into the font name. Unfortunately, the syntax varies depending on which
     * font system is dominant. Thus, using fontname="times bold italic" will
     * produce a bold, slanted Times font using Pango, the usual main font
     * library. Alternatively, fontname="times:italic" will produce a slanted
     * Times font from fontconfig, while fontname="times-bold" will resolve to a
     * bold Times using Quartz. You will need to ascertain which package is used
     * by your Graphviz system and refer to the relevant documentation.
     * <p>
     * If Graphviz is not built with a high-level font library, fontname will be
     * considered the name of a Type 1 or True Type font file. If you specify
     * fontname=schlbk, the tool will look for a file named schlbk.ttf or
     * schlbk.pfa or schlbk.pfb in one of the directories specified by the
     * fontpath attribute. The lookup does support various aliases for the
     * common fonts.
     *
     * @param value The font name.
     * @return This object.
     */
    protected A setFontName(String value) {
        return setValue(GvAttributeName.FONT_NAME, value);
    }

    /**
     * Font size, in points, used for text.
     *
     * @param value The font size.
     * @return This object.
     */
    protected A setFontSize(double value) {
        return setValue(GvAttributeName.FONT_SIZE, value);
    }

    /**
     * If a gradient fill is being used, this determines the angle of the fill.
     * For linear fills, the colors transform along a line specified by the
     * angle
     * and the center of the object. For radial fills, a value of zero causes
     * the
     * colors to transform radially from the center; for non-zero values, the
     * colors transform from a point near the object's periphery as specified by
     * the value.
     *
     * If unset, the default angle is 0.
     *
     * @param value The gradient angle.
     * @return This object.
     */
    protected A setGradiantAngle(int value) {
        return setValue(GvAttributeName.GRADIENT_ANGLE, value);
    }

    /**
     * Synonym for URL.
     *
     * @param value The URL.
     * @return This object.
     */
    protected A setHRef(String value) {
        return setValue(GvAttributeName.HREF, value);
    }

    /**
     * Allows the graph author to provide an id for graph objects which is to be
     * included in the output. Normal "\N", "\E", "\G" substitutions are
     * applied.
     * <p>
     * If provided, it is the responsibility of the provider to keep its values
     * sufficiently unique for its intended downstream use. Note, in particular,
     * that "\E" does not provide a unique id for multi-edges.
     * <p>
     * If no id attribute is provided, then a unique internal id is used. However,
     * this value is unpredictable by the graph writer. An externally provided id
     * is not used internally.
     * <p>
     * If the graph provides an id attribute, this will be used as a prefix for
     * internally generated attributes. By making these distinct, the user can
     * include multiple image maps in the same document.
     *
     * @param value The id.
     * @return This object.
     */
    protected A setId(String value) {
        return setValue(GvAttributeName.ID, escapeString(value, EscapeContext.STANDARD));
    }

    /**
     * Spring constant used in virtual physical model. It roughly corresponds to
     * an ideal edge length (in inches), in that increasing K tends to increase
     * the distance between nodes. Note that the edge attribute len can be used
     * to override this value for adjacent nodes.
     *
     * @param value K.
     * @return This object.
     */
    protected A setK(double value) {
        return setValue(GvAttributeName.K, value);
    }

    /**
     * Text label attached to objects. If a node's shape is record, then the
     * label can have a special format which describes the record layout.
     * <p>
     * Note that a node's default label is "\N", so the node's name or ID
     * becomes
     * its label. Technically, a node's name can be an HTML string but this will
     * not mean that the node's label will be interpreted as an HTML-like label.
     * This is because the node's actual label is an ordinary string, which will
     * be replaced by the raw bytes stored in the node's name. To get an
     * HTML-like label, the label attribute value itself must be an HTML string.
     *
     * @param value The label.
     * @return This object.
     */
    protected A setLabel(String value) {
        return setValue(GvAttributeName.LABEL, escapeString(value, EscapeContext.STANDARD));
    }

    /**
     * Height of graph or cluster label, in inches.
     *
     * @param value The label height.
     * @return This object.
     */
    protected A setLabelHeight(double value) {
        return setValue(GvAttributeName.LHEIGHT, value);
    }

    /**
     * Justification for cluster labels. If "r", the label is right-justified
     * within bounding rectangle; if "l", left-justified; else the label is
     * centered. Note that a subgraph inherits attributes from its parent. Thus,
     * if the root graph sets labeljust to "l", the subgraph inherits this
     * value.
     *
     * @param value The label justification.
     * @return This object.
     */
    protected A setLabelJust(GvLabelJust value) {
        return setValue(GvAttributeName.LABEL_JUST, value.encode());
    }

    /**
     * Vertical placement of labels for nodes, root graphs and clusters.
     * <p>
     * For graphs and clusters, only "t" and "b" are allowed, corresponding to
     * placement at the top and bottom, respectively. By default, root graph
     * labels go on the bottom and cluster labels go on the top. Note that a
     * subgraph inherits attributes from its parent. Thus, if the root graph
     * sets labelloc to "b", the subgraph inherits this value.
     * <p>
     * For nodes, this attribute is used only when the height of the node is
     * larger than the height of its label. If labelloc is set to "t", "c", or
     * "b", the label is aligned with the top, centered, or aligned with the
     * bottom of the node, respectively. In the default case, the label is
     * vertically centered.
     *
     * @param value The label location.
     * @return This object.
     */
    protected A setLabelLoc(GvLabelLoc value) {
        return setValue(GvAttributeName.LABEL_LOC, value.encode());
    }

    /**
     * Label position, in points. The position indicates the center of the
     * label.
     *
     * @param value The label position.
     * @return This object.
     */
    protected A setLabelPosition(GvPoint2 value) {
        return setValue(GvAttributeName.LP, value.encode());
    }

    /**
     * Width of graph or cluster label, in inches.
     *
     * @param value The label width.
     * @return This object.
     */
    protected A setLabelWidth(double value) {
        return setValue(GvAttributeName.LWIDTH, value);
    }

    /**
     * Specifies layers in which the node, edge or cluster is present.
     *
     * @param value The layer.
     * @return This object.
     */
    protected A setLayer(String value) {
        return setValue(GvAttributeName.LAYER, value);
    }

    /**
     * For graphs, this sets x and y margins of canvas, in inches. If the margin
     * is a single double, both margins are set equal to the given value.
     * <p>
     * Note that the margin is not part of the drawing but just empty space left
     * around the drawing. It basically corresponds to a translation of drawing,
     * as would be necessary to center a drawing on a page. Nothing is actually
     * drawn in the margin. To actually extend the background of a drawing, see
     * the pad attribute.
     * <p>
     * For clusters, this specifies the space between the nodes in the cluster
     * and the cluster bounding box. By default, this is 8 points.
     * <p>
     * For nodes, this attribute specifies space left around the node's label.
     * By default, the value is 0.11,0.055.
     *
     * @param value The margin.
     * @return This object.
     */
    protected A setMargin(double value) {
        return setValue(GvAttributeName.MARGIN, value);
    }

    protected A setMargin(double x,
                          double y) {
        return setValue(GvAttributeName.MARGIN, x, y);
    }

    /**
     * By default, the justification of multi-line labels is done within the
     * largest context that makes sense. Thus, in the label of a polygonal node,
     * a left-justified line will align with the left side of the node (shifted
     * by the prescribed margin). In record nodes, left-justified line will line
     * up with the left side of the enclosing column of fields. If nojustify is
     * "true", multi-line labels will be justified in the context of itself. For
     * example, if the attribute is set, the first label line is long, and the
     * second is shorter and left-justified, the second will align with the
     * left-most character in the first line, regardless of how large the node
     * might be.
     *
     * @param value True if no justify.
     * @return This object.
     */
    protected A setNoJustify(boolean value) {
        return setValue(GvAttributeName.NO_JUSTIFY, value);
    }

    /**
     * If the value of the attribute is "out", then the outedges of a node, that
     * is, edges with the node as its tail node, must appear left-to-right in
     * the
     * same order in which they are defined in the input. If the value of the
     * attribute is "in", then the inedges of a node must appear left-to-right
     * in
     * the same order in which they are defined in the input. If defined as a
     * graph or subgraph attribute, the value is applied to all nodes in the
     * graph or subgraph. Note that the graph attribute takes precedence over
     * the node attribute.
     *
     * @param value The ordering.
     * @return This object.
     */
    protected A setOrdering(GvOrdering value) {
        return setValue(GvAttributeName.ORDERING, value.encode());
    }

    /**
     * Angle, in degrees, used to rotate polygon node shapes. For any number of
     * polygon sides, 0 degrees rotation results in a flat base.
     *
     * @param value The orientation.
     * @return This object.
     */
    protected A setOrientation(double value) {
        return setValue(GvAttributeName.ORIENTATION, value);
    }

    /**
     * Specifies the width of the pen, in points, used to draw lines and curves,
     * including the boundaries of edges and clusters. The value is inherited by
     * subclusters. It has no effect on text.
     * <p>
     * Previous to 31 January 2008, the effect of penwidth=W was achieved by
     * including setlinewidth(W) as part of a style specification. If both are
     * used, penwidth will be used.
     *
     * @param value The pen width.
     * @return This object.
     */
    protected A setPenWidth(double value) {
        return setValue(GvAttributeName.PEN_WIDTH, value);
    }

    /**
     * Set number of peripheries used in polygonal shapes and cluster
     * boundaries.
     * Note that user-defined shapes are treated as a form of box shape, so the
     * default peripheries value is 1 and the user-defined shape will be drawn
     * in a bounding rectangle. Setting peripheries=0 will turn this off. Also, 1
     * is the maximum peripheries value for clusters.
     *
     * @param value The number of peripheries.
     * @return This object.
     */
    protected A setPeripheries(int value) {
        return setValue(GvAttributeName.PERIPHERIES, value);
    }

    /**
     * Rank constraints on the nodes in a subgraph. If rank="same", all nodes
     * are placed on the same rank. If rank="min", all nodes are placed on the
     * minimum rank. If rank="source", all nodes are placed on the minimum rank,
     * and the only nodes on the minimum rank belong to some subgraph whose rank
     * attribute is "source" or "min". Analogous criteria hold for rank="max"
     * and rank="sink". (Note: the minimum rank is topmost or leftmost, and the
     * maximum rank is bottommost or rightmost.)
     *
     * @param value The rank.
     * @return This object.
     */
    protected A setRank(GvRankType value) {
        return setValue(GvAttributeName.RANK, value.encode());
    }

    /**
     * If the object has a URL, this attribute determines which window of the
     * browser is used for the URL. See W3C documentation.
     *
     * @param value The target.
     * @return This object.
     */
    protected A setTarget(String value) {
        return setValue(GvAttributeName.TARGET, value);
    }

    /**
     * Tooltip annotation attached to the node or edge. If unset, Graphviz will
     * use the object's label if defined. Note that if the label is a record
     * specification or an HTML-like label, the resulting tooltip may be
     * unhelpful. In this case, if tooltips will be generated, the user should
     * set a tooltip attribute explicitly.
     *
     * @param value The tooltip.
     * @return This object.
     */
    protected A setTooltip(String value) {
        return setValue(GvAttributeName.TOOLTIP, escapeString(value, EscapeContext.TOOLTIP));
    }

    /**
     * Hyperlinks incorporated into device-dependent output. At present, used in
     * ps2, cmap, i*map and svg formats. For all these formats, URLs can be
     * attached to nodes, edges and clusters. URL attributes can also be
     * attached to the root graph in ps2, cmap and i*map formats. This serves as
     * the base URL for relative URLs in the former, and as the default image map
     * file in the latter.
     * <p>
     * For svg, cmapx and imap output, the active area for a node is its visible
     * image. For example, an unfilled node with no drawn boundary will only be
     * active on its label. For other output, the active area is its bounding
     * box. The active area for a cluster is its bounding box. For edges, the
     * active areas are small circles where the edge contacts its head and tail
     * nodes. In addition, for svg, cmapx and imap, the active area includes a
     * thin polygon approximating the edge. The circles may overlap the related
     * node, and the edge URL dominates. If the edge has a label, this will also
     * be active. Finally, if the edge has a head or tail label, this will also
     * be active.
     * <p>
     * Note that, for edges, the attributes headURL, tailURL, labelURL and
     * edgeURL allow control of various parts of an edge. Also note that, if
     * active areas of two edges overlap, it is unspecified which area
     * dominates.
     *
     * @param value The URL.
     * @return This object.
     */
    protected A setURL(String value) {
        return setValue(GvAttributeName.URL, value);
    }

    /**
     * External label for a node or edge. For nodes, the label will be placed
     * outside of the node but near it. For edges, the label will be placed near
     * the center of the edge. This can be useful in dot to avoid the occasional
     * problem when the use of edge labels distorts the layout. For other
     * layouts, the xlabel attribute can be viewed as a synonym for the label
     * attribute.
     * <p>
     * These labels are added after all nodes and edges have been placed. The
     * labels will be placed so that they do not overlap any node or label. This
     * means it may not be possible to place all of them. To force placing all
     * of them, use the forcelabels attribute.
     *
     * @param value The external label.
     * @return This object.
     */
    protected A setXLabel(String value) {
        return setValue(GvAttributeName.XLABEL, escapeString(value, EscapeContext.STANDARD));
    }

    /**
     * Position of an exterior label, in points. The position indicates the
     * center of the label.
     *
     * @param value The external label position.
     * @return This object.
     */
    protected A setXLabelPosition(double value) {
        return setValue(GvAttributeName.XLP, value);
    }
}