package cdc.gv.atts;

import cdc.gv.colors.GvColor;

/**
 * Definition of node attributes.
 *
 * @author Damien Carbonne
 *
 */
public final class GvNodeAttributes extends GvAttributes<GvNodeAttributes> {
    public GvNodeAttributes() {
        super(GvAttributeUsage.NODE);
    }

    @Override
    protected GvNodeAttributes self() {
        return this;
    }

    @Override
    public GvNodeAttributes setArea(double value) {
        return super.setArea(value);
    }

    @Override
    public GvNodeAttributes setColor(GvColor value) {
        return super.setColor(value);
    }

    @Override
    public GvNodeAttributes setColor(GvColor... values) {
        return super.setColor(values);
    }

    @Override
    public GvNodeAttributes setColor(GvColorList value) {
        return super.setColor(value);
    }

    @Override
    public GvNodeAttributes setColorScheme(String value) {
        return super.setColorScheme(value);
    }

    @Override
    public GvNodeAttributes setComment(String value) {
        return super.setComment(value);
    }

    /**
     * Distortion factor for shape=polygon. Positive values cause top part to be
     * larger than bottom; negative values do the opposite.
     *
     * @param value The distorsion.
     * @return This object.
     */
    public GvNodeAttributes setDistorsion(double value) {
        return setValue(GvAttributeName.DISTORTION, value);
    }

    @Override
    public GvNodeAttributes setFillColor(GvColor value) {
        return super.setFillColor(value);
    }

    @Override
    public GvNodeAttributes setFillColor(GvColor... values) {
        return super.setFillColor(values);
    }

    @Override
    public GvNodeAttributes setFillColor(GvColorList value) {
        return super.setFillColor(value);
    }

    /**
     * If false, the size of a node is determined by smallest width and height
     * needed to contain its label and image, if any, with a margin specified by
     * the margin attribute. The width and height must also be at least as large
     * as the sizes specified by the width and height attributes, which specify
     * the minimum values for these parameters.
     * <p>
     * If true, the node size is specified by the values of the width and height
     * attributes only and is not expanded to contain the text label. There will
     * be a warning if the label (with margin) cannot fit within these limits.
     * <p>
     * If the fixedsize attribute is set to shape, the width and height
     * attributes also determine the size of the node shape, but the label can
     * be much larger. Both the label and shape sizes are used when avoiding node
     * overlap, but all edges to the node ignore the label and only contact the
     * node shape. No warning is given if the label is too large.
     *
     * @param value The fixed size hint.
     * @return This object.
     */
    public GvNodeAttributes setFixedSize(GvFixedSize value) {
        return setValue(GvAttributeName.FIXED_SIZE, value.encode());
    }

    @Override
    public GvNodeAttributes setFontColor(GvColor value) {
        return super.setFontColor(value);
    }

    @Override
    public GvNodeAttributes setFontName(String value) {
        return super.setFontName(value);
    }

    @Override
    public GvNodeAttributes setFontSize(double value) {
        return super.setFontSize(value);
    }

    @Override
    public GvNodeAttributes setGradiantAngle(int value) {
        return super.setGradiantAngle(value);
    }

    /**
     * If the end points of an edge belong to the same group, i.e., have the
     * same group attribute, parameters are set to avoid crossings and keep the edges
     * straight.
     *
     * @param value The group.
     * @return This object.
     */
    public GvNodeAttributes setGroup(String value) {
        return setValue(GvAttributeName.GROUP, value);
    }

    /**
     * Height of node, in inches. This is taken as the initial, minimum height
     * of the node. If fixedsize is true, this will be the final height of the
     * node.<br>
     * Otherwise, if the node label requires more height to fit, the node's
     * height will be increased to contain the label. Note also that, if the
     * output format is dot, the value given to height will be the final value.
     * <p>
     * If the node shape is regular, the width and height are made identical. In
     * this case, if either the width or the height is set explicitly, that
     * value is used. In this case, if both the width or the height are set
     * explicitly, the maximum of the two values is used. If neither is set
     * explicitly, the minimum of the two default values is used.
     *
     * @param value The height.
     * @return This object.
     */
    public GvNodeAttributes setHeight(double value) {
        return setValue(GvAttributeName.HEIGHT, value);
    }

    @Override
    public GvNodeAttributes setHRef(String value) {
        return super.setHRef(value);
    }

    @Override
    public GvNodeAttributes setId(String value) {
        return super.setId(value);
    }

    /**
     * Gives the name of a file containing an image to be displayed inside a
     * node. The image file must be in one of the recognized formats, typically
     * JPEG, PNG, GIF, BMP, SVG or Postscript, and be able to be converted into
     * the desired output format.
     * <p>
     * The file must contain the image size information. This is usually
     * trivially true for the bitmap formats. For PostScript, the file must
     * contain a line starting with %%BoundingBox: followed by four integers
     * specifying the lower left x and y coordinates and the upper right x and y
     * coordinates of the bounding box for the image, the coordinates being in
     * points. An SVG image file must contain width and height attributes,
     * typically as part of the svg element. The values for these should have
     * the
     * form of a floating point number, followed by optional units, e.g.,
     * width="76pt". Recognized units are in, px, pc, pt, cm and mm for inches,
     * pixels, picas, points, centimeters and millimeters, respectively. The
     * default unit is points.
     * <p>
     * Unlike with the shapefile attribute, the image is treated as node content
     * rather than the entire node. In particular, an image can be contained in
     * a node of any shape, not just a rectangle.
     *
     * @param value The image filename.
     * @return This object.
     */
    public GvNodeAttributes setImage(String value) {
        return setValue(GvAttributeName.IMAGE, value);
    }

    /**
     * Attribute controlling how an image fills its containing node. In general,
     * the image is given its natural size, (cf. dpi), and the node size is made
     * large enough to contain its image, its label, its margin, and its
     * peripheries. Its width and height will also be at least as large as its
     * minimum width and height. If, however, fixedsize=true, the width and
     * height attributes specify the exact size of the node.
     * <p>
     * During rendering, in the default case (imagescale=false), the image
     * retains its natural size. If imagescale=true, the image is uniformly
     * scaled (i.e., its aspect ratio is preserved) to fit inside the node. At
     * least one dimension of the image will be as large as possible given the
     * size of the node. When imagescale=width, the width of the image is scaled
     * to fill the node width. The corresponding property holds when
     * imagescale=height. When imagescale=both, both the height and the width
     * are scaled separately to fill the node.
     * <p>
     * In all cases, if a dimension of the image is larger than the
     * corresponding dimension of the node, that dimension of the image is scaled
     * down to fit the node. As with the case of expansion, if imagescale=true,
     * width and height are scaled uniformly.
     *
     * @param value The image scale hint.
     * @return This object.
     */
    // TODO String variant ???
    public GvNodeAttributes setImageScale(boolean value) {
        return setValue(GvAttributeName.IMAGE, value);
    }

    @Override
    public GvNodeAttributes setLabel(String value) {
        return super.setLabel(value);
    }

    @Override
    public GvNodeAttributes setLabelLoc(GvLabelLoc value) {
        return super.setLabelLoc(value);
    }

    @Override
    public GvNodeAttributes setLayer(String value) {
        return super.setLayer(value);
    }

    @Override
    public GvNodeAttributes setMargin(double value) {
        return super.setMargin(value);
    }

    @Override
    public GvNodeAttributes setMargin(double x,
                                      double y) {
        return super.setMargin(x, y);
    }

    @Override
    public GvNodeAttributes setNoJustify(boolean value) {
        return super.setNoJustify(value);
    }

    @Override
    public GvNodeAttributes setOrdering(GvOrdering value) {
        return super.setOrdering(value);
    }

    @Override
    public GvNodeAttributes setOrientation(double value) {
        return super.setOrientation(value);
    }

    @Override
    public GvNodeAttributes setPenWidth(double value) {
        return super.setPenWidth(value);
    }

    @Override
    public GvNodeAttributes setPeripheries(int value) {
        return super.setPeripheries(value);
    }

    /**
     * If true, force polygon to be regular, i.e., the vertices of the polygon
     * will lie on a circle whose center is the center of the node.
     *
     * @param value The regular hint.
     * @return This object.
     */
    public GvNodeAttributes setRegular(boolean value) {
        return setValue(GvAttributeName.REGULAR, value);
    }

    /**
     * Set the shape of a node.
     *
     * @param value The node shape.
     * @return This object.
     */
    public GvNodeAttributes setShape(GvNodeShape value) {
        return setValue(GvAttributeName.SHAPE, value.encode());
    }

    /**
     * If defined, shapefile specifies a file containing user-supplied node
     * content. The shape of the node is set to box. The image in the shapefile
     * must be rectangular. The image formats supported as well as the precise
     * semantics of how the file is used depends on the output format. For
     * further details, see Image Formats and External PostScript files.
     * <p>
     * There is one exception to this usage. If shape is set to "epsf",
     * shapefile gives a filename containing a definition of the node in PostScript. The
     * graphics defined must be contain all of the node content, including any
     * desired boundaries. For further details, see External PostScript files.
     *
     * @param value The shape filename.
     * @return This object.
     *
     * @deprecated As indicated by graphviz.
     */
    @Deprecated
    public GvNodeAttributes setShapeFile(String value) {
        return setValue(GvAttributeName.SHAPE_FILE, value);
    }

    /**
     * Associated to a boolean or an integer.
     *
     * @param value The sides hint.
     * @return This object.
     */
    public GvNodeAttributes setSides(int value) {
        return setValue(GvAttributeName.SIDES, value);
    }

    /**
     * Skew factor for shape=polygon. Positive values skew top of polygon to
     * right; negative to left.
     *
     * @param value The skew value.
     * @return This object.
     */
    public GvNodeAttributes setSkew(double value) {
        return setValue(GvAttributeName.SKEW, value);
    }

    /**
     * Set style information for components of the graph. For cluster subgraphs,
     * if style="filled", the cluster box's background is filled.
     * <p>
     * If the default style attribute has been set for a component, an
     * individual component can use style="" to revert to the normal default. For example,
     * if the graph has {@code edge [style="invis"]}
     * making all edges invisible, a specific edge can overrride this via
     * {@code a -> b [style=""]}<br>
     * Of course, the component can also explicitly set its style attribute to
     * the desired value.
     *
     * @param values The styles.
     * @return This object.
     */
    public GvNodeAttributes setStyle(GvNodeStyle... values) {
        return setValue(GvAttributeName.STYLE, encode(", ", values));
    }

    /**
     * Width of node, in inches. This is taken as the initial, minimum width of
     * the node. If fixedsize is true, this will be the final width of the node.
     * Otherwise, if the node label requires more width to fit, the node's width
     * will be increased to contain the label. Note also that, if the output
     * format is dot, the value given to width will be the final value.
     * <p>
     * If the node shape is regular, the width and height are made identical. In
     * this case, if either the width or the height is set explicitly, that
     * value is used. In this case, if both the width or the height are set
     * explicitly, the maximum of the two values is used. If neither is set
     * explicitly, the minimum of the two default values is used.
     *
     * @param value The width.
     * @return This object.
     */
    public GvNodeAttributes setWidth(double value) {
        return setValue(GvAttributeName.WIDTH, value);
    }

    @Override
    public GvNodeAttributes setTarget(String value) {
        return super.setTarget(value);
    }

    @Override
    public GvNodeAttributes setTooltip(String value) {
        return super.setTooltip(value);
    }

    @Override
    public GvNodeAttributes setURL(String value) {
        return super.setURL(value);
    }

    @Override
    public GvNodeAttributes setXLabel(String value) {
        return super.setXLabel(value);
    }

    @Override
    public GvNodeAttributes setXLabelPosition(double value) {
        return super.setXLabelPosition(value);
    }
}