package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

public class GvPoint2 implements GvEncodable {
    double x;
    double y;

    public GvPoint2(double x,
                    double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String encode() {
        return x + "," + y;
    }
}