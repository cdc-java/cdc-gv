package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible Graph overlap options.
 *
 * @author Damien Carbonne
 */
public enum GvOverlap implements GvEncodable {
    /**
     * The layout will be scaled down as much as possible without introducing any overlaps,
     * obviously assuming there are none to begin with.
     */
    COMPRESS,

    /**
     * If the layout is done by neato with mode="ipsep", then one can use
     * overlap=ipsep. In this case, the overlap removal constraints are
     * incorporated into the layout algorithm itself.<br>
     * <b>N.B.</b> At present, this only supports one level of clustering.
     */
    IPSEP,

    ORTHO,

    /**
     * Overlaps are moved by optimizing two constraint problems, one for the x axis and one for the y.<br>
     * The suffix indicates which axis is processed first.
     */
    ORTHOXY,

    /**
     * Overlaps are moved by optimizing two constraint problems, one for the x axis and one for the y.<br>
     * The suffix indicates which axis is processed first.
     */
    ORTHOYX,

    /**
     * Prism, is a proximity graph-based algorithm, is used to remove node overlaps.
     */
    PRISM,

    SCALE,

    /**
     * x and y are separately scaled to remove overlaps.
     */
    SCALEXY,

    VORONOI,

    /**
     * Overlap removal is done as a quadratic optimization to minimize node
     * displacement while removing node overlaps
     */
    VPSC;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}