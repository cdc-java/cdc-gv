package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible standard arrow types.
 *
 * @author Damien Carbonne
 *
 */
public enum GvPrimitiveArrowType implements GvEncodable {
    BOX(true, true),
    CROW(false, true),
    CURVE(false, true),
    DIAMOND(true, true),
    DOT(true, false),
    ICURVE(false, true),
    INV(true, true),
    NONE(false, false),
    NORMAL(true, true),
    TEE(false, true),
    VEE(false, true);

    private final boolean supportsModifier;
    private final boolean supportsSide;

    private GvPrimitiveArrowType(boolean supportsModifier,
                                 boolean supportsSide) {
        this.supportsModifier = supportsModifier;
        this.supportsSide = supportsSide;
    }

    public boolean supportsModifier() {
        return supportsModifier;
    }

    public boolean supportsSide() {
        return supportsSide;
    }

    @Override
    public String encode() {
        return name().toLowerCase();
    }

}