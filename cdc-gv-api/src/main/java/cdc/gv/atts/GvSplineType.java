package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible spline types.
 *
 * @author Damien Carbonne
 *
 */
public enum GvSplineType implements GvEncodable {
    NONE,
    LINE,
    POLYLINE,
    SPLINE,
    ORTHO,
    CURVED;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}