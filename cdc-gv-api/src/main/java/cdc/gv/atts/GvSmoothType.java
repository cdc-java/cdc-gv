package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible smooth types.
 *
 * @author Damien Carbonne
 *
 */
public enum GvSmoothType implements GvEncodable {
    NONE,
    AVG_DIST,
    GRAPH_DIST,
    POWER_DIST,
    RNG,
    SPRING,
    TRIANGLE;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}