package cdc.gv.atts;

import cdc.gv.colors.GvColor;

/**
 * Definition of graph attributes.
 *
 * @author Damien Carbonne
 *
 */
// TODO apply them to cluster and subgraph attributes ?
public final class GvGraphAttributes extends GvAttributes<GvGraphAttributes> {
    public GvGraphAttributes() {
        super(GvAttributeUsage.GRAPH);
    }

    @Override
    protected GvGraphAttributes self() {
        return this;
    }

    /**
     * A string in the xdot format specifying an arbitrary background. During
     * rendering, the canvas is first filled as described in the bgcolor
     * attribute. Then, if _background is defined, the graphics operations
     * described in the string are performed on the canvas.
     *
     * @param value The color.
     * @return This object.
     */
    public GvGraphAttributes setBackground(GvColor value) {
        return super.setValue(GvAttributeName.BACKGROUND, value.encode());
    }

    /**
     * Bounding box of drawing in points.
     *
     * @param value The bounding box.
     * @return This object.
     */
    public GvGraphAttributes setBoundingBox(GvRect value) {
        return super.setValue(GvAttributeName.BB, value.encode());
    }

    @Override
    public GvGraphAttributes setBgColor(GvColor value) {
        return super.setBgColor(value);
    }

    /**
     * If true, the drawing is centered in the output canvas.
     *
     * @param value True to center.
     * @return This object.
     */
    public GvGraphAttributes setCenter(boolean value) {
        return setValue(GvAttributeName.CENTER, value);
    }

    /**
     * Specifies the character encoding used when interpreting string input as a
     * text label. The default value is "UTF-8". The other legal value is
     * "iso-8859-1" or, equivalently, "Latin1". The charset attribute is
     * case-insensitive. Note that if the character encoding used in the input
     * does not match the charset value, the resulting output may be very
     * strange.
     *
     * @param value The charset.
     * @return This object.
     */
    public GvGraphAttributes setCharset(String value) {
        return setValue(GvAttributeName.CHARSET, value);
    }

    /**
     * Mode used for handling clusters. If clusterrank is "local", a subgraph
     * whose name begins with "cluster" is given special treatment. The subgraph
     * is laid out separately, and then integrated as a unit into its parent
     * graph, with a bounding rectangle drawn about it. If the cluster has a
     * label parameter, this label is displayed within the rectangle. Note also
     * that there can be clusters within clusters. At present, the modes
     * "global"
     * and "none" appear to be identical, both turning off the special cluster
     * processing.
     *
     * @param value The cluster rank.
     * @return This object.
     */
    public GvGraphAttributes setClusterRank(GvClusterRank value) {
        return setValue(GvAttributeName.CLUSTER_RANK, value.encode());
    }

    @Override
    public GvGraphAttributes setColorScheme(String value) {
        return super.setColorScheme(value);
    }

    @Override
    public GvGraphAttributes setComment(String value) {
        return super.setComment(value);
    }

    /**
     * If true, allow edges between clusters. (See lhead and ltail below.)
     *
     * @param value True to allow edges between clusters.
     * @return This object.
     */
    public GvGraphAttributes setCompound(boolean value) {
        return setValue(GvAttributeName.COMPOUND, value);
    }

    /**
     * If true, use edge concentrators. This merges multiedges into a single
     * edge
     * and causes partially parallel edges to share part of their paths. The
     * latter feature is not yet available outside of dot.
     *
     * @param value True to use concentrators.
     * @return This object.
     */
    public GvGraphAttributes setConcentrate(boolean value) {
        return setValue(GvAttributeName.CONCENTRATE, value);
    }

    /**
     * Factor damping force motions. On each iteration, a nodes movement is
     * limited to this factor of its potential motion. By being less than 1.0,
     * the system tends to ``cool'', thereby preventing cycling.
     * <p>
     * Associated to a double.
     *
     * @param value The damping.
     * @return This object.
     */
    public GvGraphAttributes setDamping(double value) {
        return setValue(GvAttributeName.DAMPING, value);
    }

    /**
     * This specifies the distance between nodes in separate connected
     * components. If set too small, connected components may overlap. Only
     * applicable if pack=false.
     *
     * @param value The default distance.
     * @return This object.
     */
    public GvGraphAttributes setDefaultDist(double value) {
        return setValue(GvAttributeName.DEFAULT_DIST, value);
    }

    /**
     * Set the number of dimensions used for the layout. The maximum value
     * allowed is 10.
     *
     * @param value The number of dimensions.
     * @return This object.
     */
    public GvGraphAttributes setDim(int value) {
        return setValue(GvAttributeName.DIM, value);
    }

    /**
     * Set the number of dimensions used for rendering. The maximum value
     * allowed
     * is 10. If both dimen and dim are set, the latter specifies the dimension
     * used for layout, and the former for rendering. If only dimen is set, this
     * is used for both layout and rendering dimensions.
     * <p>
     * Note that, at present, all aspects of rendering are 2D. This includes the
     * shape and size of nodes, overlap removal, and edge routing. Thus, for
     * dimen {@literal > 2}, the only valid information is the pos attribute of the nodes.
     * All other coordinates will be 2D and, at best, will reflect a projection
     * of a higher-dimensional point onto the plane.
     *
     * @param value The number of dimensions.
     * @return This object.
     */
    public GvGraphAttributes setDimen(int value) {
        return setValue(GvAttributeName.DIMEN, value);
    }

    /**
     * Only valid when mode="ipsep". If true, constraints are generated for each
     * edge in the largest (heuristic) directed acyclic subgraph such that the
     * edge must point downwards. If "hier", generates level constraints similar
     * to those used with mode="hier". The main difference is that, in the
     * latter
     * case, only these constraints are involved, so a faster solver can be
     * used.
     *
     * @param value The constraint.
     * @return This object.
     */
    public GvGraphAttributes setDirEdgeConstraint(GvDirEdgeConstraint value) {
        return setValue(GvAttributeName.DIR_EDGE_CONSTRAINT, value.encode());
    }

    /**
     * This specifies the expected number of pixels per inch on a display
     * device.
     * For bitmap output, this guarantees that text rendering will be done more
     * accurately, both in size and in placement. For SVG output, it is used to
     * guarantee that the dimensions in the output correspond to the correct
     * number of points or inches.
     *
     * @param value The DPI.
     * @return This object.
     */
    public GvGraphAttributes setDpi(double value) {
        return setValue(GvAttributeName.DPI, value);
    }

    /**
     * Terminating condition. If the length squared of all energy gradients are
     * {@literal <} epsilon, the algorithm stops.
     *
     * @param value The espilon.
     * @return This object.
     */
    public GvGraphAttributes setEpsilon(double value) {
        return setValue(GvAttributeName.EPSILON, value);
    }

    /**
     * Margin used around polygons for purposes of spline edge routing. The
     * interpretation is the same as given for sep. This should normally be
     * strictly less than sep.
     *
     * @param value The esep value.
     * @param additive If true, prepend "+".
     * @return This object.
     */
    public GvGraphAttributes setEsep(double value,
                                     boolean additive) {
        if (additive) {
            return setValue(GvAttributeName.ESEP, "+" + value);
        } else {
            return setValue(GvAttributeName.ESEP, value);
        }
    }

    /**
     * Margin used around polygons for purposes of spline edge routing. The
     * interpretation is the same as given for sep. This should normally be
     * strictly less than sep.
     *
     * @param value The esep values.
     * @param additive If true, prepend "+".
     * @return This object.
     */
    public GvGraphAttributes setEsep(GvPoint2 value,
                                     boolean additive) {
        if (additive) {
            return setValue(GvAttributeName.ESEP, "+" + value.encode());
        } else {
            return setValue(GvAttributeName.ESEP, value.encode());
        }
    }

    @Override
    public GvGraphAttributes setFontColor(GvColor value) {
        return super.setFontColor(value);
    }

    @Override
    public GvGraphAttributes setFontName(String value) {
        return super.setFontName(value);
    }

    /**
     * Allows user control of how basic fontnames are represented in SVG output.
     * If fontnames is undefined or "svg", the output will try to use known SVG
     * fontnames. For example, the default font "Times-Roman" will be mapped to
     * the basic SVG font "serif". This can be overridden by setting fontnames
     * to "ps" or "gd". In the former case, known PostScript font names such as
     * "Times-Roman" will be used in the output. In the latter case, the
     * fontconfig font conventions are used. Thus, "Times-Roman" would be
     * treated as "Nimbus Roman No9 L". These last two options are useful with SVG
     * viewers that support these richer fontname spaces.
     *
     * @param value The font name.
     * @return This object.
     */
    public GvGraphAttributes setFontNames(String value) {
        return setValue(GvAttributeName.FONT_NAMES, value);
    }

    /**
     * Directory list used by libgd to search for bitmap fonts if Graphviz was
     * not built with the fontconfig library. If fontpath is not set, the
     * environment variable DOTFONTPATH is checked. If that is not set,
     * GDFONTPATH is checked. If not set, libgd uses its compiled-in font path.
     * Note that fontpath is an attribute of the root graph.
     *
     * @param value The font path.
     * @return This object.
     */
    public GvGraphAttributes setFontPath(String value) {
        return setValue(GvAttributeName.FONT_PATH, value);
    }

    @Override
    public GvGraphAttributes setFontSize(double value) {
        return super.setFontSize(value);
    }

    /**
     * If true, all xlabel attributes are placed, even if there is some overlap
     * with nodes or other labels.
     *
     * @param value True to force label.
     * @return This object.
     */
    public GvGraphAttributes setForceLabels(boolean value) {
        return setValue(GvAttributeName.FORCE_LABELS, value);
    }

    @Override
    public GvGraphAttributes setGradiantAngle(int value) {
        return super.setGradiantAngle(value);
    }

    @Override
    public GvGraphAttributes setHRef(String value) {
        return super.setHRef(value);
    }

    @Override
    public GvGraphAttributes setId(String value) {
        return super.setId(value);
    }

    /**
     * Specifies a list of directories in which to look for image files as
     * specified by the image attribute or using the IMG element in HTML-like
     * labels. The string should be a list of (absolute or relative) pathnames,
     * each separated by a semicolon (for Windows) or a colon (all other OS).
     * The first directory in which a file of the given name is found will be used
     * to load the image. If imagepath is not set, relative pathnames for the image
     * file will be interpreted with respect to the current working directory.
     *
     * @param value The image path.
     * @return This object.
     */
    public GvGraphAttributes setImagePath(String value) {
        return setValue(GvAttributeName.IMAGE_PATH, value);
    }

    /**
     * For layout algorithms that support initial input positions (specified by
     * the pos attribute), this attribute can be used to appropriately scale the
     * values. By default, fdp and neato interpret the x and y values of pos as
     * being in inches. (NOTE: neato -n(2) treats the coordinates as being in
     * points, being the unit used by the layout algorithms for the pos
     * attribute.) Thus, if the graph has pos attributes in points, one should
     * set inputscale=72. This can also be set on the command line using the -s
     * flag flag.
     * <p>
     * If not set, no scaling is done and the units on input are treated as
     * inches. A value of 0 is equivalent to inputscale=72.
     *
     * @param value The input scale.
     * @return This object.
     */
    public GvGraphAttributes setInputScale(double value) {
        return setValue(GvAttributeName.INPUT_SCALE, value);
    }

    @Override
    public GvGraphAttributes setK(double value) {
        return super.setK(value);
    }

    @Override
    public GvGraphAttributes setLabel(String value) {
        return super.setLabel(value);
    }

    @Override
    public GvGraphAttributes setLabelHeight(double value) {
        return super.setLabelHeight(value);
    }

    @Override
    public GvGraphAttributes setLabelJust(GvLabelJust value) {
        return super.setLabelJust(value);
    }

    @Override
    public GvGraphAttributes setLabelLoc(GvLabelLoc value) {
        return super.setLabelLoc(value);
    }

    @Override
    public GvGraphAttributes setLabelPosition(GvPoint2 value) {
        return super.setLabelPosition(value);
    }

    /**
     * The value indicates whether to treat a node whose name has the form
     * |edgelabel|* as a special node representing an edge label. The default
     * (0) produces no effect. If the attribute is set to 1, sfdp uses a
     * penalty-based method to make that kind of node close to the center of its
     * neighbor. With a value of 2, sfdp uses a penalty-based method to make
     * that kind of node close to the old center of its neighbor. Finally, a value of
     * 3 invokes a two-step process of overlap removal and straightening.
     *
     * @param value The label scheme.
     * @return This object.
     */
    public GvGraphAttributes setLabelScheme(String value) {
        return setValue(GvAttributeName.LABEL_SCHEME, value);
    }

    @Override
    public GvGraphAttributes setLabelWidth(double value) {
        return super.setLabelWidth(value);
    }

    /**
     * If true, the graph is rendered in landscape mode. Synonymous with
     * rotate=90 or orientation=landscape.
     *
     * @param value True for landscape.
     * @return This object.
     */
    public GvGraphAttributes setLandscape(boolean value) {
        return setValue(GvAttributeName.LANDSCAPE, value);
    }

    /**
     * Specifies the separator characters used to split an attribute of type
     * layerRange into a list of ranges.
     *
     * @param value The layer list separator.
     * @return This object.
     */
    public GvGraphAttributes seLayerListeSep(String value) {
        return setValue(GvAttributeName.LAYER_LIST_SEP, value);
    }

    /**
     * Selects a list of layers to be emitted.
     *
     * @param value The layer names.
     * @return This object.
     */
    public GvGraphAttributes seLayerSelect(String value) {
        return setValue(GvAttributeName.LAYER_SELECT, value);
    }

    /**
     * Specifies the separator characters used to split the layers attribute
     * into a list of layer names.
     *
     * @param value The layer names separator.
     * @return This object.
     */
    public GvGraphAttributes seLayerSep(String value) {
        return setValue(GvAttributeName.LAYER_SEP, value);
    }

    /**
     * Specifies a linearly ordered list of layer names attached to the graph
     * The graph is then output in separate layers. Only those components belonging
     * to the current output layer appear. For more information, see the page
     * How to use drawing layers (overlays).
     *
     * @param value The layers.
     * @return This object.
     */
    public GvGraphAttributes seLayers(String value) {
        return setValue(GvAttributeName.LAYERS, value);
    }

    /**
     * Specifies the name of the layout algorithm to use, such as "dot" or
     * "neato". Normally, graphs should be kept independent of a type of layout.
     * In some cases, however, it can be convenient to embed the type of layout
     * desired within the graph. For example, a graph containing position
     * information from a layout might want to record what the associated layout
     * algorithm was.
     * <p>
     * This attribute takes precedence over the -K flag or the actual command
     * name used.
     *
     * @param value The layout algorithm.
     * @return This object.
     */
    public GvGraphAttributes setLayout(String value) {
        return setValue(GvAttributeName.LAYOUT, value);
    }

    /**
     * Number of levels allowed in the multilevel scheme.
     *
     * @param value The number of levels allowed.
     * @return This object.
     */
    public GvGraphAttributes setLevels(int value) {
        return setValue(GvAttributeName.LEVELS, value);
    }

    /**
     * Specifies strictness of level constraints in neato when mode="ipsep" or
     * "hier". Larger positive values mean stricter constraints, which demand
     * more separation between levels. On the other hand, negative values will
     * relax the constraints by allowing some overlap between the levels.
     *
     * @param value The levels gap.
     * @return This object.
     */
    public GvGraphAttributes setLevelsGap(double value) {
        return setValue(GvAttributeName.LEVELS_GAP, value);
    }

    @Override
    public GvGraphAttributes setMargin(double value) {
        return super.setMargin(value);
    }

    @Override
    public GvGraphAttributes setMargin(double x,
                                       double y) {
        return super.setMargin(x, y);
    }

    /**
     * Multiplicative scale factor used to alter the MinQuit (default = 8) and
     * MaxIter (default = 24) parameters used during crossing minimization.
     * These correspond to the number of tries without improvement before
     * quitting and the maximum number of iterations in each pass.
     *
     * @param value The scale factor.
     * @return This object.
     */
    public GvGraphAttributes setMCLimit(double value) {
        return setValue(GvAttributeName.MC_LIMIT, value);
    }

    /**
     * Specifies the minimum separation between all nodes.
     *
     * @param value The min distance.
     * @return This object.
     */
    public GvGraphAttributes setMinDist(double value) {
        return setValue(GvAttributeName.MIN_DIST, value);
    }

    /**
     * Technique for optimizing the layout. For neato, if mode is "major", neato
     * uses stress majorization. If mode is "KK", neato uses a version of the
     * gradient descent method. The only advantage to the latter technique is
     * that it is sometimes appreciably faster for small (number of nodes {@literal < 100})
     * graphs. A significant disadvantage is that it may cycle.
     * <p>
     * There are two experimental modes in neato, "hier", which adds a top-down
     * directionality similar to the layout used in dot, and "ipsep", which
     * allows the graph to specify minimum vertical and horizontal distances
     * between nodes. (See the sep attribute.)
     * <p>
     * For sfdp, the default mode is "spring", which corresponds to using a
     * spring-electrical model. Setting mode to "maxent" causes a similar model
     * to be run but one that also takes into account edge lengths specified by
     * the "len" attribute.
     *
     * @param value The graph mode.
     * @return This object.
     */
    public GvGraphAttributes setMode(GvGraphMode value) {
        return setValue(GvAttributeName.MODE, value.encode());
    }

    /**
     * This value specifies how the distance matrix is computed for the input
     * graph. The distance matrix specifies the ideal distance between every
     * pair
     * of nodes. neato attemps to find a layout which best achieves these
     * distances. By default, it uses the length of the shortest path, where the
     * length of each edge is given by its len attribute. If model is "circuit",
     * neato uses the circuit resistance model to compute the distances. This
     * tends to emphasize clusters. If model is "subset", neato uses the subset
     * model. This sets the edge length to be the number of nodes that are
     * neighbors of exactly one of the end points, and then calculates the
     * shortest paths. This helps to separate nodes with high degree.
     * <p>
     * For more control of distances, one can use model=mds. In this case, the
     * len of an edge is used as the ideal distance between its vertices. A
     * shortest path calculation is only used for pairs of nodes not connected
     * by an edge. Thus, by supplying a complete graph, the input can specify all
     * of the relevant distances.
     *
     * @param value The graph model.
     * @return This object.
     */
    public GvGraphAttributes setModel(GvGraphModel value) {
        return setValue(GvAttributeName.MODEL, value.encode());
    }

    /**
     * If Graphviz is built with MOSEK defined, mode=ipsep and mosek=true, the
     * Mosek software (www.mosek.com) is use to solve the ipsep constraints.
     *
     * @param value True to use mosek.
     * @return This object.
     */
    public GvGraphAttributes setMosek(boolean value) {
        return setValue(GvAttributeName.MOSEK, value);
    }

    /**
     * In dot, this specifies the minimum space between two adjacent nodes in
     * the same rank, in inches.
     * <p>
     * For other layouts, this affects the spacing between loops on a single
     * node, or multiedges between a pair of nodes.
     *
     * @param value The Node separation distance.
     * @return This object.
     */
    public GvGraphAttributes setNodeSep(double value) {
        return setValue(GvAttributeName.NODE_SEP, value);
    }

    @Override
    public GvGraphAttributes setNoJustify(boolean value) {
        return super.setNoJustify(value);
    }

    /**
     * By default, the final layout is translated so that the lower-left corner
     * of the bounding box is at the origin. This can be annoying if some nodes
     * are pinned or if the user runs neato -n. To avoid this translation, set
     * notranslate to true.
     *
     * @param value True to avoid translation.
     * @return This object.
     */
    public GvGraphAttributes setNoTranslate(boolean value) {
        return setValue(GvAttributeName.NO_TRANSLATE, value);
    }

    /**
     * If set, normalize coordinates of final layout so that the first point is
     * at the origin, and then rotate the layout so that the angle of the first
     * edge is specified by the value of normalize in degrees. If normalize is
     * not a number, it is evaluated as a bool, with true corresponding to 0
     * degrees. NOTE: Since the attribute is evaluated first as a number, 0 and
     * 1 cannot be used for false and true.
     *
     * @param value True to normalize.
     * @return This object.
     */
    public GvGraphAttributes setNormalize(double value) {
        return setValue(GvAttributeName.NORMALIZE, value);
    }

    public GvGraphAttributes setNormalize(boolean value) {
        return setValue(GvAttributeName.NORMALIZE, value);
    }

    /**
     * Used to set number of iterations in network simplex applications. nslimit
     * is used in computing node x coordinates, nslimit1 for ranking nodes. If
     * defined, # iterations = nslimit(1) * # nodes; otherwise, # iterations =
     * MAXINT.
     *
     * @param value The NS limit.
     * @return This object.
     */
    public GvGraphAttributes setNSLimit(double value) {
        return setValue(GvAttributeName.NS_LIMIT, value);
    }

    /**
     * Used to set number of iterations in network simplex applications. nslimit
     * is used in computing node x coordinates, nslimit1 for ranking nodes. If
     * defined, # iterations = nslimit(1) * # nodes; otherwise, # iterations =
     * MAXINT.
     *
     * @param value The NS limit 1.
     * @return This object.
     */
    public GvGraphAttributes setNSLimit1(double value) {
        return setValue(GvAttributeName.NS_LIMIT1, value);
    }

    @Override
    public GvGraphAttributes setOrdering(GvOrdering value) {
        return super.setOrdering(value);
    }

    @Override
    public GvGraphAttributes setOrientation(double value) {
        return super.setOrientation(value);
    }

    /**
     * Specify order in which nodes and edges are drawn.
     *
     * @param value The output mode.
     * @return This object.
     */
    public GvGraphAttributes setOutputOrder(GvOutputMode value) {
        return setValue(GvAttributeName.OUTPUT_ORDER, value.encode());
    }

    /**
     * Determines if and how node overlaps should be removed. Nodes are first
     * enlarged using the sep attribute. If "true" , overlaps are retained. If
     * the value is "scale", overlaps are removed by uniformly scaling in x and
     * y. If the value converts to "false", and it is available, Prism, a
     * proximity graph-based algorithm, is used to remove node overlaps. This
     * can also be invoked explicitly with "overlap=prism". This technique starts
     * with a small scaling up, controlled by the overlap_scaling attribute,
     * which can remove a significant portion of the overlap. The prism option
     * also accepts an optional non-negative integer suffix. This can be used to
     * control the number of attempts made at overlap removal. By default,
     * overlap="prism" is equivalent to overlap="prism1000". Setting
     * overlap="prism0" causes only the scaling phase to be run.
     * <p>
     * If Prism is not available, or the version of Graphviz is earlier than
     * 2.28, "overlap=false" uses a Voronoi-based technique. This can always be
     * invoked explicitly with "overlap=voronoi".
     * <p>
     * If the value is "scalexy", x and y are separately scaled to remove
     * overlaps.
     * <p>
     * If the value is "compress", the layout will be scaled down as much as
     * possible without introducing any overlaps, obviously assuming there are
     * none to begin with.
     * <p>
     * N.B.The remaining allowed values of overlap correspond to algorithms
     * which, at present, can produce bad aspect ratios. In addition, we
     * deprecate the use of the "ortho*" and "portho*".
     * <p>
     * If the value is "vpsc", overlap removal is done as a quadratic
     * optimization to minimize node displacement while removing node overlaps.
     * <p>
     * If the value is "orthoxy" or "orthoyx", overlaps are moved by optimizing
     * two constraint problems, one for the x axis and one for the y. The suffix
     * indicates which axis is processed first. If the value is "ortho", the
     * technique is similar to "orthoxy" except a heuristic is used to reduce
     * the bias between the two passes. If the value is "ortho_yx", the technique is
     * the same as "ortho", except the roles of x and y are reversed. The values
     * "portho", "porthoxy", "porthoxy", and "portho_yx" are similar to the
     * previous four, except only pseudo-orthogonal ordering is enforced.
     * <p>
     * If the layout is done by neato with mode="ipsep", then one can use
     * overlap=ipsep. In this case, the overlap removal constraints are
     * incorporated into the layout algorithm itself. N.B. At present, this only
     * supports one level of clustering.
     * <p>
     * Except for fdp and sfdp, the layouts assume overlap="true" as the
     * default.<br>
     * Fdp first uses a number of passes using a built-in, force-directed
     * technique to try to remove overlaps. Thus, fdp accepts overlap with an
     * integer prefix followed by a colon, specifying the number of tries. If
     * there is no prefix, no initial tries will be performed. If there is
     * nothing following a colon, none of the above methods will be attempted.
     * By default, fdp uses overlap="9:prism". Note that overlap="true",
     * overlap="0:true" and overlap="0:" all turn off all overlap removal.
     * <p>
     * By default, sfdp uses overlap="prism0".
     * <p>
     * Except for the Voronoi and prism methods, all of these transforms
     * preserve the orthogonal ordering of the original layout. That is, if the x
     * coordinates of two nodes are originally the same, they will remain the
     * same, and if the x coordinate of one node is originally less than the x
     * coordinate of another, this relation will still hold in the transformed
     * layout. The similar properties hold for the y coordinates. This is not
     * quite true for the "porth*" cases. For these, orthogonal ordering is only
     * preserved among nodes related by an edge.
     *
     * @param value {@code true} to remove overlaps.
     * @return This object.
     */
    public GvGraphAttributes setOverlap(boolean value) {
        return setValue(GvAttributeName.OVERLAP, value);
    }

    /**
     * Determines if and how node overlaps should be removed. Nodes are first
     * enlarged using the sep attribute. If "true" , overlaps are retained. If
     * the value is "scale", overlaps are removed by uniformly scaling in x and
     * y. If the value converts to "false", and it is available, Prism, a
     * proximity graph-based algorithm, is used to remove node overlaps. This
     * can also be invoked explicitly with "overlap=prism". This technique starts
     * with a small scaling up, controlled by the overlap_scaling attribute,
     * which can remove a significant portion of the overlap. The prism option
     * also accepts an optional non-negative integer suffix. This can be used to
     * control the number of attempts made at overlap removal. By default,
     * overlap="prism" is equivalent to overlap="prism1000". Setting
     * overlap="prism0" causes only the scaling phase to be run.
     * <p>
     * If Prism is not available, or the version of Graphviz is earlier than
     * 2.28, "overlap=false" uses a Voronoi-based technique. This can always be
     * invoked explicitly with "overlap=voronoi".
     * <p>
     * If the value is "scalexy", x and y are separately scaled to remove
     * overlaps.
     * <p>
     * If the value is "compress", the layout will be scaled down as much as
     * possible without introducing any overlaps, obviously assuming there are
     * none to begin with.
     * <p>
     * N.B.The remaining allowed values of overlap correspond to algorithms
     * which, at present, can produce bad aspect ratios. In addition, we
     * deprecate the use of the "ortho*" and "portho*".
     * <p>
     * If the value is "vpsc", overlap removal is done as a quadratic
     * optimization to minimize node displacement while removing node overlaps.
     * <p>
     * If the value is "orthoxy" or "orthoyx", overlaps are moved by optimizing
     * two constraint problems, one for the x axis and one for the y. The suffix
     * indicates which axis is processed first. If the value is "ortho", the
     * technique is similar to "orthoxy" except a heuristic is used to reduce
     * the bias between the two passes. If the value is "ortho_yx", the technique is
     * the same as "ortho", except the roles of x and y are reversed. The values
     * "portho", "porthoxy", "porthoxy", and "portho_yx" are similar to the
     * previous four, except only pseudo-orthogonal ordering is enforced.
     * <p>
     * If the layout is done by neato with mode="ipsep", then one can use
     * overlap=ipsep. In this case, the overlap removal constraints are
     * incorporated into the layout algorithm itself. N.B. At present, this only
     * supports one level of clustering.
     * <p>
     * Except for fdp and sfdp, the layouts assume overlap="true" as the
     * default.<br>
     * Fdp first uses a number of passes using a built-in, force-directed
     * technique to try to remove overlaps. Thus, fdp accepts overlap with an
     * integer prefix followed by a colon, specifying the number of tries. If
     * there is no prefix, no initial tries will be performed. If there is
     * nothing following a colon, none of the above methods will be attempted.
     * By default, fdp uses overlap="9:prism". Note that overlap="true",
     * overlap="0:true" and overlap="0:" all turn off all overlap removal.
     * <p>
     * By default, sfdp uses overlap="prism0".
     * <p>
     * Except for the Voronoi and prism methods, all of these transforms
     * preserve the orthogonal ordering of the original layout. That is, if the x
     * coordinates of two nodes are originally the same, they will remain the
     * same, and if the x coordinate of one node is originally less than the x
     * coordinate of another, this relation will still hold in the transformed
     * layout. The similar properties hold for the y coordinates. This is not
     * quite true for the "porth*" cases. For these, orthogonal ordering is only
     * preserved among nodes related by an edge.
     *
     * @param value The value.
     * @return This object.
     */
    public GvGraphAttributes setOverlap(GvOverlap value) {
        return setValue(GvAttributeName.OVERLAP, value.encode());
    }

    /**
     * Determines if and how node overlaps should be removed. Nodes are first
     * enlarged using the sep attribute. If "true" , overlaps are retained. If
     * the value is "scale", overlaps are removed by uniformly scaling in x and
     * y. If the value converts to "false", and it is available, Prism, a
     * proximity graph-based algorithm, is used to remove node overlaps. This
     * can also be invoked explicitly with "overlap=prism". This technique starts
     * with a small scaling up, controlled by the overlap_scaling attribute,
     * which can remove a significant portion of the overlap. The prism option
     * also accepts an optional non-negative integer suffix. This can be used to
     * control the number of attempts made at overlap removal. By default,
     * overlap="prism" is equivalent to overlap="prism1000". Setting
     * overlap="prism0" causes only the scaling phase to be run.
     * <p>
     * If Prism is not available, or the version of Graphviz is earlier than
     * 2.28, "overlap=false" uses a Voronoi-based technique. This can always be
     * invoked explicitly with "overlap=voronoi".
     * <p>
     * If the value is "scalexy", x and y are separately scaled to remove
     * overlaps.
     * <p>
     * If the value is "compress", the layout will be scaled down as much as
     * possible without introducing any overlaps, obviously assuming there are
     * none to begin with.
     * <p>
     * N.B.The remaining allowed values of overlap correspond to algorithms
     * which, at present, can produce bad aspect ratios. In addition, we
     * deprecate the use of the "ortho*" and "portho*".
     * <p>
     * If the value is "vpsc", overlap removal is done as a quadratic
     * optimization to minimize node displacement while removing node overlaps.
     * <p>
     * If the value is "orthoxy" or "orthoyx", overlaps are moved by optimizing
     * two constraint problems, one for the x axis and one for the y. The suffix
     * indicates which axis is processed first. If the value is "ortho", the
     * technique is similar to "orthoxy" except a heuristic is used to reduce
     * the bias between the two passes. If the value is "ortho_yx", the technique is
     * the same as "ortho", except the roles of x and y are reversed. The values
     * "portho", "porthoxy", "porthoxy", and "portho_yx" are similar to the
     * previous four, except only pseudo-orthogonal ordering is enforced.
     * <p>
     * If the layout is done by neato with mode="ipsep", then one can use
     * overlap=ipsep. In this case, the overlap removal constraints are
     * incorporated into the layout algorithm itself. N.B. At present, this only
     * supports one level of clustering.
     * <p>
     * Except for fdp and sfdp, the layouts assume overlap="true" as the
     * default.<br>
     * Fdp first uses a number of passes using a built-in, force-directed
     * technique to try to remove overlaps. Thus, fdp accepts overlap with an
     * integer prefix followed by a colon, specifying the number of tries. If
     * there is no prefix, no initial tries will be performed. If there is
     * nothing following a colon, none of the above methods will be attempted.
     * By default, fdp uses overlap="9:prism". Note that overlap="true",
     * overlap="0:true" and overlap="0:" all turn off all overlap removal.
     * <p>
     * By default, sfdp uses overlap="prism0".
     * <p>
     * Except for the Voronoi and prism methods, all of these transforms
     * preserve the orthogonal ordering of the original layout. That is, if the x
     * coordinates of two nodes are originally the same, they will remain the
     * same, and if the x coordinate of one node is originally less than the x
     * coordinate of another, this relation will still hold in the transformed
     * layout. The similar properties hold for the y coordinates. This is not
     * quite true for the "porth*" cases. For these, orthogonal ordering is only
     * preserved among nodes related by an edge.
     *
     * @param value The value.
     * @param suffix The suffix.
     * @return This object.
     */
    public GvGraphAttributes setOverlap(GvOverlap value,
                                        int suffix) {
        return setValue(GvAttributeName.OVERLAP,
                        value.encode() + suffix);
    }

    /**
     * Determines if and how node overlaps should be removed. Nodes are first
     * enlarged using the sep attribute. If "true" , overlaps are retained. If
     * the value is "scale", overlaps are removed by uniformly scaling in x and
     * y. If the value converts to "false", and it is available, Prism, a
     * proximity graph-based algorithm, is used to remove node overlaps. This
     * can also be invoked explicitly with "overlap=prism". This technique starts
     * with a small scaling up, controlled by the overlap_scaling attribute,
     * which can remove a significant portion of the overlap. The prism option
     * also accepts an optional non-negative integer suffix. This can be used to
     * control the number of attempts made at overlap removal. By default,
     * overlap="prism" is equivalent to overlap="prism1000". Setting
     * overlap="prism0" causes only the scaling phase to be run.
     * <p>
     * If Prism is not available, or the version of Graphviz is earlier than
     * 2.28, "overlap=false" uses a Voronoi-based technique. This can always be
     * invoked explicitly with "overlap=voronoi".
     * <p>
     * If the value is "scalexy", x and y are separately scaled to remove
     * overlaps.
     * <p>
     * If the value is "compress", the layout will be scaled down as much as
     * possible without introducing any overlaps, obviously assuming there are
     * none to begin with.
     * <p>
     * N.B.The remaining allowed values of overlap correspond to algorithms
     * which, at present, can produce bad aspect ratios. In addition, we
     * deprecate the use of the "ortho*" and "portho*".
     * <p>
     * If the value is "vpsc", overlap removal is done as a quadratic
     * optimization to minimize node displacement while removing node overlaps.
     * <p>
     * If the value is "orthoxy" or "orthoyx", overlaps are moved by optimizing
     * two constraint problems, one for the x axis and one for the y. The suffix
     * indicates which axis is processed first. If the value is "ortho", the
     * technique is similar to "orthoxy" except a heuristic is used to reduce
     * the bias between the two passes. If the value is "ortho_yx", the technique is
     * the same as "ortho", except the roles of x and y are reversed. The values
     * "portho", "porthoxy", "porthoxy", and "portho_yx" are similar to the
     * previous four, except only pseudo-orthogonal ordering is enforced.
     * <p>
     * If the layout is done by neato with mode="ipsep", then one can use
     * overlap=ipsep. In this case, the overlap removal constraints are
     * incorporated into the layout algorithm itself. N.B. At present, this only
     * supports one level of clustering.
     * <p>
     * Except for fdp and sfdp, the layouts assume overlap="true" as the
     * default.<br>
     * Fdp first uses a number of passes using a built-in, force-directed
     * technique to try to remove overlaps. Thus, fdp accepts overlap with an
     * integer prefix followed by a colon, specifying the number of tries. If
     * there is no prefix, no initial tries will be performed. If there is
     * nothing following a colon, none of the above methods will be attempted.
     * By default, fdp uses overlap="9:prism". Note that overlap="true",
     * overlap="0:true" and overlap="0:" all turn off all overlap removal.
     * <p>
     * By default, sfdp uses overlap="prism0".
     * <p>
     * Except for the Voronoi and prism methods, all of these transforms
     * preserve the orthogonal ordering of the original layout. That is, if the x
     * coordinates of two nodes are originally the same, they will remain the
     * same, and if the x coordinate of one node is originally less than the x
     * coordinate of another, this relation will still hold in the transformed
     * layout. The similar properties hold for the y coordinates. This is not
     * quite true for the "porth*" cases. For these, orthogonal ordering is only
     * preserved among nodes related by an edge.
     *
     * @param prefix The prefix.
     * @param value The value.
     * @return This object.
     */
    public GvGraphAttributes setOverlap(int prefix,
                                        GvOverlap value) {
        return setValue(GvAttributeName.OVERLAP,
                        prefix + ":" + (value == null ? "" : value.encode()));
    }

    /**
     * When overlap=prism, the layout is scaled by this factor, thereby removing
     * a fair amount of node overlap, and making node overlap removal faster and
     * better able to retain the graph's shape.
     * <p>
     * If overlap_scaling is negative, the layout is scaled by
     * -1*overlap_scaling times the average label size.<br>
     * If overlap_scaling is positive, the layout is scaled by
     * overlap_scaling.<br>
     * If overlap_scaling is zero, no scaling is done.
     *
     * @param value The overlap scaling.
     * @return This object.
     */
    public GvGraphAttributes setOverlapScaling(double value) {
        return setValue(GvAttributeName.OVERLAP_SCALING, value);
    }

    /**
     * If true, the overlap removal algorithm will perform a compression pass to
     * reduce the size of the layout.
     *
     * @param value The overlap shrink.
     * @return This object.
     */
    public GvGraphAttributes setOverlapShrink(boolean value) {
        return setValue(GvAttributeName.OVERLAP_SHRINK, value);
    }

    /**
     * This is true if the value of pack is "true" (case-insensitive) or a
     * non-negative integer. If true, each connected component of the graph is
     * laid out separately, and then the graphs are packed together. If pack has
     * an integral value, this is used as the size, in points, of a margin
     * around
     * each part; otherwise, a default margin of 8 is used. If pack is
     * interpreted as false, the entire graph is laid out together. The
     * granularity and method of packing is influenced by the packmode
     * attribute.
     * <p>
     * For layouts which always do packing, such a twopi, the pack attribute is
     * just used to set the margin.
     *
     * @param value True to pack.
     * @return This object.
     */
    public GvGraphAttributes setPack(boolean value) {
        return setValue(GvAttributeName.PACK, value);
    }

    public GvGraphAttributes setPack(int value) {
        return setValue(GvAttributeName.PACK, value);
    }

    /**
     * This indicates how connected components should be packed (cf. packMode).
     * Note that defining packmode will automatically turn on packing as though
     * one had set pack=true.
     *
     * @param value The pack mode.
     * @return This object.
     */
    public GvGraphAttributes setPackMode(GvPackMode value) {
        return setValue(GvAttributeName.PACK_MODE, value.encode());
    }

    /**
     * To be used with ARRAY PackMode.
     *
     * @param value The pack mode.
     * @param flags F*C? where F is t, b, l, r, u, c and C is a number.
     * @return This object.
     */
    public GvGraphAttributes setPackMode(GvPackMode value,
                                         String flags) {
        return setValue(GvAttributeName.PACK_MODE, value.encode() + '_' + flags);
    }

    /**
     * The pad attribute specifies how much, in inches, to extend the drawing
     * area around the minimal area needed to draw the graph. If the pad is a
     * single double, both the x and y pad values are set equal to the given
     * value. This area is part of the drawing and will be filled with the
     * background color, if appropriate.
     * <p>
     * Normally, a small pad is used for aesthetic reasons, especially when a
     * background color is used, to avoid having nodes and edges abutting the
     * boundary of the drawn region.
     *
     * @param value The pad value.
     * @return This object.
     */
    public GvGraphAttributes setPad(double value) {
        return setValue(GvAttributeName.PAD, value);
    }

    public GvGraphAttributes setPad(double x,
                                    double y) {
        return setValue(GvAttributeName.PAD, x, y);
    }

    /**
     * Width and height of output pages, in inches. If only a single value is
     * given, this is used for both the width and height.
     * <p>
     * If this is set and is smaller than the size of the layout, a rectangular
     * array of pages of the specified page size is overlaid on the layout, with
     * origins aligned in the lower-left corner, thereby partitioning the layout
     * into pages. The pages are then produced one at a time, in pagedir order.
     * <p>
     * At present, this only works for PostScript output. For other types of
     * output, one should use another tool to split the output into multiple
     * output files. Or use the viewport to generate multiple files.
     *
     * @param value The page size.
     * @return This object.
     */
    public GvGraphAttributes setPage(double value) {
        return setValue(GvAttributeName.PAGE, value);
    }

    public GvGraphAttributes setPage(double x,
                                     double y) {
        return setValue(GvAttributeName.PAGE, x, y);
    }

    /**
     * If the page attribute is set and applicable, this attribute specifies the
     * order in which the pages are emitted. This is limited to one of the 8 row
     * or column major orders.
     *
     * @param value The page direction.
     * @return This object.
     */
    public GvGraphAttributes setPageDir(GvPageDir value) {
        return setValue(GvAttributeName.PAGE_DIR, value.encode());
    }

    /**
     * Quadtree scheme to use.
     * <p>
     * A TRUE bool value corresponds to "normal"; a FALSE bool value corresponds
     * to "none". As a slight exception to the normal interpretation of bool, a
     * value of "2" corresponds to "fast".
     *
     * @param value The quadtree type.
     * @return This object.
     */
    public GvGraphAttributes setQuadTree(GvQuadType value) {
        return setValue(GvAttributeName.QUAD_TREE, value.encode());
    }

    /**
     * If {@literal quantum  > 0.0}, node label dimensions will be rounded to integral
     * multiples of the quantum.
     *
     * @param value The quantum.
     * @return This object.
     */
    public GvGraphAttributes setQuantum(double value) {
        return setValue(GvAttributeName.QUANTUM, value);
    }

    /**
     * Sets direction of graph layout. For example, if rankdir="LR", and barring
     * cycles, an edge {@literal T -> H}; will go from left to right. By default, graphs
     * are laid out from top to bottom.
     * <p>
     * This attribute also has a side-effect in determining how record nodes are
     * interpreted. See record shapes.
     *
     * @param value The rank direction.
     * @return This object.
     */
    public GvGraphAttributes setRankDir(GvRankDir value) {
        return setValue(GvAttributeName.RANK_DIR, value.encode());
    }

    /**
     * In dot, this gives the desired rank separation, in inches. This is the
     * minimum vertical distance between the bottom of the nodes in one rank and
     * the tops of nodes in the next. If the value contains "equally", the
     * centers of all ranks are spaced equally apart. Note that both settings
     * are possible, e.g., ranksep = "1.2 equally".
     * <p>
     * In twopi, this attribute specifies the radial separation of concentric
     * circles. For twopi, ranksep can also be a list of doubles. The first
     * double specifies the radius of the inner circle; the second double
     * specifies the increase in radius from the first circle to the second;
     * etc.<br>
     * If there are more circles than numbers, the last number is used as the
     * increment for the remainder.
     *
     * @param value The rank separation.
     * @param equally If true, equally rank separation is applied.
     * @return This object.
     */
    public GvGraphAttributes setRankSep(double value,
                                        boolean equally) {
        if (equally) {
            return setValue(GvAttributeName.RANK_SEP, value + " equally");
        } else {
            return setValue(GvAttributeName.RANK_SEP, value);
        }
    }

    public GvGraphAttributes setRankSep(double value) {
        return setRankSep(value, false);
    }

    public GvGraphAttributes setRankSep(double... values) {
        return setValue(GvAttributeName.RANK_SEP, encode(":", values));
    }

    /**
     * Sets the aspect ratio (drawing height/drawing width) for the drawing.
     * Note that this is adjusted before the size attribute constraints are enforced.
     * In addition, the calculations usually ignore the node sizes, so the final
     * drawing size may only approximate what is desired.
     * <p>
     * If ratio is numeric, it is taken as the desired aspect ratio. Then, if
     * the actual aspect ratio is less than the desired ratio, the drawing height is
     * scaled up to achieve the desired ratio; if the actual ratio is greater
     * than that desired ratio, the drawing width is scaled up.
     * <p>
     * If ratio = "fill" and the size attribute is set, node positions are
     * scaled, separately in both x and y, so that the final drawing exactly
     * fills the specified size. If both size values exceed the width and height
     * of the drawing, then both coordinate values of each node are scaled up
     * accordingly. However, if either size dimension is smaller than the
     * corresponding dimension in the drawing, one dimension is scaled up so
     * that the final drawing has the same aspect ratio as specified by size. Then,
     * when rendered, the layout will be scaled down uniformly in both
     * dimensions to fit the given size, which may cause nodes and text to shrink as well.
     * This may not be what the user wants, but it avoids the hard problem of
     * how to reposition the nodes in an acceptable fashion to reduce the drawing
     * size.
     * <p>
     * If ratio = "compress" and the size attribute is set, dot attempts to
     * compress the initial layout to fit in the given size. This achieves a
     * tighter packing of nodes but reduces the balance and symmetry. This
     * feature only works in dot.
     * <p>
     * If ratio = "expand", the size attribute is set, and both the width and
     * the height of the graph are less than the value in size, node positions are
     * scaled uniformly until at least one dimension fits size exactly. Note
     * that this is distinct from using size as the desired size, as here the drawing
     * is expanded before edges are generated and all node and text sizes remain
     * unchanged.
     * <p>
     * If ratio = "auto", the page attribute is set and the graph cannot be
     * drawn on a single page, then size is set to an ``ideal'' value. In particular,
     * the size in a given dimension will be the smallest integral multiple of
     * the page size in that dimension which is at least half the current size.
     * The two dimensions are then scaled independently to the new size. This
     * feature only works in dot.
     *
     * @param value The ratio value.
     * @return This object.
     */
    public GvGraphAttributes setRatio(double value) {
        return setValue(GvAttributeName.RATIO, value);
    }

    public GvGraphAttributes setRatio(GvRatio value) {
        return setValue(GvAttributeName.RATIO, value.encode());
    }

    /**
     * If true and there are multiple clusters, run crossing minimization a
     * second time.
     *
     * @param value True to run crossing minimization a second time.
     * @return This object.
     */
    public GvGraphAttributes setReMinCross(boolean value) {
        return setValue(GvAttributeName.REMIN_CROSS, value);
    }

    /**
     * The power of the repulsive force used in an extended Fruchterman-Reingold
     * force directed model. Values larger than 1 tend to reduce the warping
     * effect at the expense of less clustering.
     *
     * @param value The repulsive force.
     * @return This object.
     */
    public GvGraphAttributes setRepulsiveForce(double value) {
        return setValue(GvAttributeName.REPULSIVE_FORCE, value);
    }

    /**
     * Maximum width and height of drawing, in inches. If only a single number
     * is given, this is used for both the width and the height.
     * <p>
     * If defined and the drawing is larger than the given size, the drawing is
     * uniformly scaled down so that it fits within the given size.
     * <p>
     * If size ends in an exclamation point (!), then it is taken to be the
     * desired size. In this case, if both dimensions of the drawing are less
     * than size, the drawing is scaled up uniformly until at least one
     * dimension equals its dimension in size.
     * <p>
     * Note that there is some interaction between the size and ratio
     * attributes.
     * <p>
     * Associated to 2 real values.
     *
     * @param width The width.
     * @param height The height.
     * @return This object.
     */
    public GvGraphAttributes setMaximumSize(double width,
                                            double height) {
        return setValue(GvAttributeName.SIZE, width + "," + height);
    }

    public GvGraphAttributes setDesiredSize(double width,
                                            double height) {
        return setValue(GvAttributeName.SIZE, width + "," + height + "!");
    }

    /**
     * If set, after the initial layout, the layout is scaled by the given
     * factors. If only a single number is given, this is used for both factors.
     *
     * @param value The scale value.
     * @return This object.
     */
    public GvGraphAttributes setScale(double value) {
        return setValue(GvAttributeName.SCALE, value);
    }

    public GvGraphAttributes setScale(double value1,
                                      double value2) {
        return setValue(GvAttributeName.SCALE, value1, value2);
    }

    /**
     * Specifies margin to leave around nodes when removing node overlap. This
     * guarantees a minimal non-zero distance between nodes.
     * <p>
     * If the attribute begins with a plus sign '+', an additive margin is
     * specified. That is, "+w,h" causes the node's bounding box to be increased
     * by w points on the left and right sides, and by h points on the top and
     * bottom. Without a plus sign, the node is scaled by 1 + w in the x
     * coordinate and 1 + h in the y coordinate.
     * <p>
     * If only a single number is given, this is used for both dimensions.
     * <p>
     * If unset but esep is defined, the sep values will be set to the esep
     * values divided by 0.8. If esep is unset, the default value is used.
     *
     * @param value The margin value.
     * @param additive If true, prepend "+".
     * @return This object.
     */
    public GvGraphAttributes setSep(double value,
                                    boolean additive) {
        if (additive) {
            return setValue(GvAttributeName.SEP, "+" + value);
        } else {
            return setValue(GvAttributeName.SEP, value);
        }
    }

    /**
     * Specifies margin to leave around nodes when removing node overlap. This
     * guarantees a minimal non-zero distance between nodes.
     * <p>
     * If the attribute begins with a plus sign '+', an additive margin is
     * specified. That is, "+w,h" causes the node's bounding box to be increased
     * by w points on the left and right sides, and by h points on the top and
     * bottom. Without a plus sign, the node is scaled by 1 + w in the x
     * coordinate and 1 + h in the y coordinate.
     * <p>
     * If only a single number is given, this is used for both dimensions.
     * <p>
     * If unset but esep is defined, the sep values will be set to the esep
     * values divided by 0.8. If esep is unset, the default value is used.
     *
     * @param value The margin value.
     * @param additive If true, prepend "+".
     * @return This object.
     */
    public GvGraphAttributes setSep(GvPoint2 value,
                                    boolean additive) {
        if (additive) {
            return setValue(GvAttributeName.SEP, "+" + value.encode());
        } else {
            return setValue(GvAttributeName.SEP, value.encode());
        }
    }

    /**
     * Specifies a post-processing step used to smooth out an uneven
     * distribution
     * of nodes.
     * <p>
     * Associated to GvSmoothType.
     *
     * @param value The smoothing type.
     * @return This object.
     */
    public GvGraphAttributes setSmooting(GvSmoothType value) {
        return setValue(GvAttributeName.SMOOTHING, value.encode());
    }

    /**
     * Controls how, and if, edges are represented. If true, edges are drawn as
     * splines routed around nodes; if false, edges are drawn as line segments.
     * If set to none or "", no edges are drawn at all.
     * <p>
     * (1 March 2007) The values line and spline can be used as synonyms for
     * false and true, respectively. In addition, the value polyline specifies
     * that edges should be drawn as polylines.
     * <p>
     * (28 Sep 2010) The value ortho specifies edges should be routed as
     * polylines of axis-aligned segments. Currently, the routing does not
     * handle ports or, in dot, edge labels.
     * <p>
     * (25 Sep 2012) The value curved specifies edges should be drawn as curved
     * arcs.
     * <p>
     * By default, the attribute is unset. How this is interpreted depends on
     * the layout. For dot, the default is to draw edges as splines. For all other
     * layouts, the default is to draw edges as line segments. Note that for
     * these latter layouts, if splines="true", this requires non-overlapping
     * nodes (cf. overlap). If fdp is used for layout and splines="compound",
     * then the edges are drawn to avoid clusters as well as nodes.
     *
     * @param value If true, draw edges as splines. If false, as line segments.
     * @return This object.
     */
    public GvGraphAttributes setSplines(boolean value) {
        return setValue(GvAttributeName.SPLINES, value);
    }

    public GvGraphAttributes setSplines(GvSplineType value) {
        return setValue(GvAttributeName.SPLINES, value.encode());
    }

    /**
     * Set style information for components of the graph. For cluster subgraphs,
     * if style="filled", the cluster box's background is filled.
     * <p>
     * If the default style attribute has been set for a component, an
     * individual component can use style="" to revert to the normal default. For example,
     * if the graph has {@code <code>edge [style="invis"]} making all edges invisible, a
     * specific edge can overrride this via
     * {@code literal a -> b [style=""]}<br>
     * Of course, the component can also explicitly set its style attribute to
     * the desired value.
     *
     * @param values The styles.
     * @return This object.
     */
    public GvGraphAttributes setStyle(GvGraphStyle... values) {
        return setValue(GvAttributeName.STYLE, encode(",", values));
    }

    /**
     * A URL or pathname specifying an XML style sheet, used in SVG output.
     *
     * @param value The style sheet.
     * @return This object.
     */
    public GvGraphAttributes setStyleSheet(String value) {
        return setValue(GvAttributeName.STYLE_SHEET, value);
    }

    @Override
    public GvGraphAttributes setTarget(String value) {
        return super.setTarget(value);
    }

    /**
     * If set explicitly to true or false, the value determines whether or not
     * internal bitmap rendering relies on a truecolor color model or uses a
     * color palette. If the attribute is unset, truecolor is not used unless
     * there is a shapefile property for some node in the graph. The output
     * model
     * will use the input model when possible.
     * <p>
     * Use of color palettes results in less memory usage during creation of the
     * bitmaps and smaller output files.
     * <p>
     * Usually, the only time it is necessary to specify the truecolor model is
     * if the graph uses more than 256 colors. However, if one uses
     * bgcolor=transparent with a color palette, font antialiasing can show up
     * as
     * a fuzzy white area around characters. Using truecolor=true avoids this
     * problem.
     *
     * @param value The value.
     * @return This object.
     */
    public GvGraphAttributes setTrueColor(boolean value) {
        return setValue(GvAttributeName.TRUE_COLOR, value);
    }

    @Override
    public GvGraphAttributes setURL(String value) {
        return super.setURL(value);
    }
}