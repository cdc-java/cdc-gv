package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible graph models.
 *
 * @author Damien Carbonne
 *
 */
public enum GvGraphModel implements GvEncodable {
    SUBSET,
    MDS,
    SHORTPATH,
    CIRCUIT;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}