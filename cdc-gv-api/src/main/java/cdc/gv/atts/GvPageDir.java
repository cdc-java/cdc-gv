package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible page directions.
 *
 * @author Damien Carbonne
 *
 */
public enum GvPageDir implements GvEncodable {
    BL,
    BR,
    TL,
    TR,
    RB,
    RT,
    LB,
    LT;

    @Override
    public String encode() {
        return name();
    }
}