package cdc.gv.atts;

import cdc.gv.colors.GvColor;

/**
 * Definition of cluster attributes.
 *
 * @author Damien Carbonne
 *
 */
public final class GvClusterAttributes extends GvAttributes<GvClusterAttributes> {
    public GvClusterAttributes() {
        super(GvAttributeUsage.CLUSTER);
    }

    @Override
    protected GvClusterAttributes self() {
        return this;
    }

    @Override
    public GvClusterAttributes setArea(double value) {
        return super.setArea(value);
    }

    @Override
    public GvClusterAttributes setBgColor(GvColor value) {
        return super.setBgColor(value);
    }

    @Override
    public GvClusterAttributes setColor(GvColor value) {
        return super.setColor(value);
    }

    @Override
    public GvClusterAttributes setColor(GvColor... values) {
        return super.setColor(values);
    }

    @Override
    public GvClusterAttributes setColor(GvColorList value) {
        return super.setColor(value);
    }

    @Override
    public GvClusterAttributes setColorScheme(String value) {
        return super.setColorScheme(value);
    }

    @Override
    public GvClusterAttributes setFillColor(GvColor value) {
        return super.setFillColor(value);
    }

    @Override
    public GvClusterAttributes setFillColor(GvColor... values) {
        return super.setFillColor(values);
    }

    @Override
    public GvClusterAttributes setFillColor(GvColorList value) {
        return super.setFillColor(value);
    }

    @Override
    public GvClusterAttributes setFontColor(GvColor value) {
        return super.setFontColor(value);
    }

    @Override
    public GvClusterAttributes setFontName(String value) {
        return super.setFontName(value);
    }

    @Override
    public GvClusterAttributes setFontSize(double value) {
        return super.setFontSize(value);
    }

    @Override
    public GvClusterAttributes setGradiantAngle(int value) {
        return super.setGradiantAngle(value);
    }

    @Override
    public GvClusterAttributes setHRef(String value) {
        return super.setHRef(value);
    }

    @Override
    public GvClusterAttributes setId(String value) {
        return super.setId(value);
    }

    @Override
    public GvClusterAttributes setK(double value) {
        return super.setK(value);
    }

    @Override
    public GvClusterAttributes setLabel(String value) {
        return super.setLabel(value);
    }

    @Override
    public GvClusterAttributes setLabelHeight(double value) {
        return super.setLabelHeight(value);
    }

    @Override
    public GvClusterAttributes setLabelJust(GvLabelJust value) {
        return super.setLabelJust(value);
    }

    @Override
    public GvClusterAttributes setLabelWidth(double value) {
        return super.setLabelWidth(value);
    }

    @Override
    public GvClusterAttributes setLabelLoc(GvLabelLoc value) {
        return super.setLabelLoc(value);
    }

    @Override
    public GvClusterAttributes setLabelPosition(GvPoint2 value) {
        return super.setLabelPosition(value);
    }

    @Override
    public GvClusterAttributes setLayer(String value) {
        return super.setLayer(value);
    }

    @Override
    public GvClusterAttributes setMargin(double value) {
        return super.setMargin(value);
    }

    @Override
    public GvClusterAttributes setMargin(double x,
                                         double y) {
        return super.setMargin(x, y);
    }

    @Override
    public GvClusterAttributes setNoJustify(boolean value) {
        return super.setNoJustify(value);
    }

    @Override
    public GvClusterAttributes setPenWidth(double value) {
        return super.setPenWidth(value);
    }

    /**
     * Color used to draw the bounding box around a cluster. If pencolor is not
     * defined, color is used. If this is not defined, bgcolor is used. If this
     * is not defined, the default is used.
     * <p>
     * Note that a cluster inherits the root graph's attributes if defined.
     * Thus,
     * if the root graph has defined a pencolor, this will override a color or
     * bgcolor attribute set for the cluster.
     *
     * @param value The color.
     * @return This object.
     */
    public GvClusterAttributes setPenColor(GvColor value) {
        return setValue(GvAttributeName.PEN_COLOR, value == null ? null : value.encode());
    }

    @Override
    public GvClusterAttributes setPeripheries(int value) {
        return super.setPeripheries(value);
    }

    @Override
    public GvClusterAttributes setRank(GvRankType value) {
        return super.setRank(value);
    }

    /**
     * Set style information for components of the graph. For cluster subgraphs,
     * if style="filled", the cluster box's background is filled.
     * <p>
     * If the default style attribute has been set for a component, an
     * individual
     * component can use style="" to revert to the normal default. For example,
     * if the graph has
     * <p>
     * <code>edge [style="invis"]</code>
     * <p>
     * making all edges invisible, a specific edge can overrride this via
     * <p>
     * <code>{@literal a -> b [style=""]}</code>
     * <p>
     * Of course, the component can also explicitly set its style attribute to
     * the desired value.
     *
     * @param values The cluster styles
     * @return This object.
     */
    public GvClusterAttributes setStyle(GvClusterStyle... values) {
        return setValue(GvAttributeName.STYLE, encode(",", values));
    }

    @Override
    public GvClusterAttributes setTarget(String value) {
        return super.setTarget(value);
    }

    @Override
    public GvClusterAttributes setTooltip(String value) {
        return super.setTooltip(value);
    }

    @Override
    public GvClusterAttributes setURL(String value) {
        return super.setURL(value);
    }
}