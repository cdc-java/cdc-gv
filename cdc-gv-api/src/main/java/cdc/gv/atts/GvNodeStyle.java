package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible node styles.
 *
 * @author Damien Carbonne
 *
 */
public enum GvNodeStyle implements GvEncodable {
    BOLD,
    DASHED,
    DIAGONALS,
    DOTTED,
    FILLED,
    INVIS,
    RADIAL,
    ROUNDED,
    SOLID,
    STRIPED,
    WEDGED;

    public boolean supportsColorList() {
        switch (this) {
        case FILLED:
        case RADIAL:
        case STRIPED:
        case WEDGED:
            return true;
        default:
            return false;
        }
    }

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}