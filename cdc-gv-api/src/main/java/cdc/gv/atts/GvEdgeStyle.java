package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible edge styles.
 *
 * @author Damien Carbonne
 *
 */
public enum GvEdgeStyle implements GvEncodable {
    BOLD,
    DASHED,
    DOTTED,
    INVIS,
    SOLID,
    TAPERED;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}