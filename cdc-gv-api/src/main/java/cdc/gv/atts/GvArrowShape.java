package cdc.gv.atts;

import java.util.ArrayList;
import java.util.List;

import cdc.gv.support.GvEncodable;

public class GvArrowShape implements GvEncodable {
    private static class Entry {
        final GvPrimitiveArrowType type;
        final boolean modifier;
        final GvArrowSide side;

        Entry(GvPrimitiveArrowType type,
              boolean modifier,
              GvArrowSide side) {
            this.type = type;
            this.modifier = modifier;
            this.side = side;
            if (modifier && !type.supportsModifier()) {
                throw new IllegalArgumentException(type + " does not support modifier");
            }
            if (side != null && !type.supportsSide()) {
                throw new IllegalArgumentException(type + " does not support side");
            }
        }

    }

    private final List<Entry> entries = new ArrayList<>();

    public GvArrowShape(GvPrimitiveArrowType type,
                        boolean modifier,
                        GvArrowSide side) {
        entries.add(new Entry(type, modifier, side));
    }

    public GvArrowShape add(GvPrimitiveArrowType type,
                            boolean modifier,
                            GvArrowSide side) {
        if (entries.size() >= 4) {
            assert false;
        } else {
            entries.add(new Entry(type, modifier, side));
        }
        return this;
    }

    @Override
    public String encode() {
        final StringBuilder builder = new StringBuilder();
        for (final Entry entry : entries) {
            if (entry.modifier) {
                builder.append('o');
            }
            if (entry.side != null) {
                builder.append(entry.side.encode());
            }
            builder.append(entry.type.encode());
        }
        return builder.toString();
    }
}