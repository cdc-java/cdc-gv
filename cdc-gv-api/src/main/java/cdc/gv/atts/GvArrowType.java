package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible standard arrow types.
 *
 * @author Damien Carbonne
 *
 */
public enum GvArrowType implements GvEncodable {
    NORMAL,
    INV,
    DOT,
    INVDOT,
    ODOT,
    INVODOT,
    NONE,
    TEE,
    EMPTY,
    INVEMPTY,
    DIAMOND,
    ODIAMOND,
    EDIAMOND,
    CROW,
    BOX,
    OBOX,
    OPEN,
    HALFOPEN,
    VEE,
    CIRCLE;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}