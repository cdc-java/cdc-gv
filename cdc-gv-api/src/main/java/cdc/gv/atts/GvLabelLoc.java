package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible label locations.
 *
 * @author Damien Carbonne
 *
 */
public enum GvLabelLoc implements GvEncodable {
    TOP,
    CENTERED,
    BOTTOM;

    @Override
    public String encode() {
        switch (this) {
        case TOP:
            return "t";
        case BOTTOM:
            return "b";
        case CENTERED:
            return "c";
        default:
            return "";
        }
    }
}