package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible fixed size values.
 *
 * @author Damien Carbonne
 *
 */
public enum GvFixedSize implements GvEncodable {
    FALSE,
    TRUE,
    SHAPE;

    @Override
    public String encode() {
        switch (this) {
        case FALSE:
            return "false";
        case TRUE:
            return "true";
        case SHAPE:
            return "shape";
        default:
            return "";
        }
    }
}