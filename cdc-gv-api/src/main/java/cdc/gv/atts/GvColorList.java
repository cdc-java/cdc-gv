package cdc.gv.atts;

import java.util.ArrayList;
import java.util.List;

import cdc.gv.colors.GvColor;
import cdc.gv.support.GvEncodable;

public class GvColorList implements GvEncodable {
    private static class Entry {
        double weigth;
        GvColor color;

        Entry(double weight,
              GvColor color) {
            this.weigth = weight;
            this.color = color;
        }
    }

    private final List<Entry> entries = new ArrayList<>();

    public GvColorList(GvColor color) {
        entries.add(new Entry(-1.0, color));
    }

    public GvColorList add(GvColor color) {
        entries.add(new Entry(-1.0, color));
        return this;
    }

    public GvColorList add(double weight,
                           GvColor color) {
        entries.add(new Entry(weight, color));
        return this;
    }

    @Override
    public String encode() {
        double total = 0.0;
        for (final Entry entry : entries) {
            if (entry.weigth >= 0.0) {
                total += entry.weigth;
            }
        }
        if (total <= 0.0) {
            total = 1.0;
        }

        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final Entry entry : entries) {
            if (!first) {
                builder.append(":");
            }
            builder.append(entry.color.encode());
            if (entry.weigth >= 0.0) {
                builder.append(";");
                builder.append(entry.weigth / total);
            }
            first = false;
        }
        return builder.toString();
    }
}