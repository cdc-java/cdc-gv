package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

/**
 * Enumeration of possible dir edge conbstraints.
 *
 * @author Damien Carbonne
 *
 */
public enum GvDirEdgeConstraint implements GvEncodable {
    FALSE,
    TRUE,
    HIER;

    @Override
    public String encode() {
        return name().toLowerCase();
    }
}