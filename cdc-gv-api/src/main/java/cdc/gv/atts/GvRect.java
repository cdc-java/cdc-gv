package cdc.gv.atts;

import cdc.gv.support.GvEncodable;

public class GvRect implements GvEncodable {
    double llx;
    double lly;
    double urx;
    double ury;

    public GvRect(double llx,
                  double lly,
                  double urx,
                  double ury) {
        this.llx = llx;
        this.lly = lly;
        this.urx = urx;
        this.ury = ury;
    }

    @Override
    public String encode() {
        return llx + "," + lly + "," + urx + "," + ury;
    }
}