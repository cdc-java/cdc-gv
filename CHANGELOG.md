# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Changed
- Updated dependencies:
    - cdc-io-0.53.2
    - org.junit-5.12.0


## [0.100.3] - 2025-01-03
### Changed
- Updated dependencies:
    - cdc-io-0.53.0
    - cdc-util-0.54.0
    - org.junit-5.11.4
    - org.apache.log4j-2.24.3


## [0.100.2] - 2024-09-28
### Changed
- Updated dependencies:
    - cdc-io-0.52.1
    - cdc-util-0.53.0
    - commons-cli-1.9.0
    - org.junit-5.11.1


## [0.100.1] - 2024-05-18
### Changed
- Updated dependencies:
    - cdc-io-0.52.0
    - cdc-util-0.52.1
    - commons-cli-1.7.0
    - org.apache.log4j-2.23.1
    - org.junit-5.10.2
- Updated maven plugins.


## [0.100.0] - 2024-01-01
### Changed
- Updated dependencies:
    - cdc-io-0.51.0
    - cdc-util-0.52.0
- Added `exec()` and call `System.exit()` in `GvToAny`.
- Updated maven plugins.


## [0.99.0] - 2023-11-25
### Changed
- Moved to Java 17
- Updated dependencies:
    - cdc-io-0.50.0
    - cdc-util-0.50.0


## [0.96.9] - 2023-11-18
### Changed
- Updated dependencies:
    - cdc-io-0.27.3
    - cdc-util-0.33.2
    - org.junit-5.10.1


## [0.96.8] - 2023-10-21
### Changed
- Updated dependencies:
    - cdc-io-0.27.2
    - cdc-util-0.33.1
    - org.junit-5.10.0
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom. 


## [0.96.7] - 2023-07-15
### Changed
- Updated dependencies:
    - cdc-io-0.27.0
    - cdc-util-0.33.0


## [0.96.6] - 2023-05-01
### Changed
- Updated dependencies:
    - cdc-io-0.26.1
    - cdc-util-0.32.0
    - org.junit-5.9.3


## [0.96.5] - 2023-04-15
### Changed
- Updated dependencies:
    - cdc-io-0.26.0


## [0.96.4] - 2023-02-25
### Changed
- Updated dependencies:
    - cdc-io-0.25.1
    - cdc-util-0.31.0
    - org.apache.log4j-2.20.0


## [0.96.3] - 2023-01-28
### Changed
- Updated dependencies:
    - cdc-io-0.24.0
    - cdc-util-0.29.0
    - org.junit-5.9.2


## [0.96.2] - 2023-01-02
### Changed
- Updated dependencies:
    - cdc-io-0.23.2
    - cdc-util-0.28.2


## [0.96.1] - 2022-11-09
### Changed
- Updated dependencies:
    - cdc-io-0.23.1
    - cdc-util-0.28.1
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1


## [0.96.0] - 2022-08-23
### Added
- Added new methods to GvWriter.
- Added new methods to GvHtmlLabel.

### Changed
- Updated dependencies:
    - cdc-io-0.23.0
    - cdc-util-0.28.0
    - org.junit-5.9.0


## [0.95.5] - 2022-07-07
### Changed
- Updated dependencies:
    - cdc-io-0.22.0
    - cdc-util-0.27.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1


## [0.95.4] - 2022-06-18
### Changed
- Updated dependencies:
    - cdc-io-0.21.3
    - cdc-util-0.26.0


## [0.95.3] - 2022-05-19
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-io-0.21.2
    - cdc-util-0.25.0


## [0.95.2] - 2022-03-11
### Changed
- Updated dependencies:
    - cdc-util-0.23.0
    - cdc-io-0.21.1
    - org.apache.log4j-2.17.2
- `Config` data is now retrieved from Manifest.


## [0.95.1] - 2022-02-13
### Changed
- Updated dependencies
    - cdc-io-0.21.0
    - cdc-util-0.20.0


## [0.95.0] - 2022-02-04
### Changed
- Updated maven plugins
- Upgraded to Java 11
- Updated dependencies
    - cdc-io-0.20.0


## [0.92.2] - 2022-01-03
### Security
- Updated dependencies:
    - cdc-util-0.14.2
    - cdc-io-0.13.2
    - org.apache.log4j-2.17.1. #2


## [0.92.1] - 2021-12-28
### Security
- Updated dependencies:
    - cdc-util-0.14.1
    - cdc-io-0.13.1
    - org.apache.log4j-2.17.0. #2


## [0.92.0] - 2021-12-14
### Security
- Updated dependencies:
    - cdc-util-0.14.0
    - cdc-io-0.13.0
    - org.apache.log4j-2.16.0. #2


## [0.91.2] - 2021-10-02
### Changed
- Updated dependencies


## [0.91.1] - 2021-07-23
### Changed
- Updated dependencies


## [0.91.0] - 2021-05-03
### Changed
- Renamed `cdc.gv.api` to `cdc.gv`. #1
- Updated dependencies.


## [0.90.0] - 2021-04-11
### Added
- First release, extracted from [cdc-utils](https://gitlab.com/cdc-java/cdc-util). cdc-java/cdc-util#38