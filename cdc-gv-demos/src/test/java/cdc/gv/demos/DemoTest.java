package cdc.gv.demos;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import cdc.gv.demo.GvBrewerColorsDemo;
import cdc.gv.demo.GvEdgesDemo;
import cdc.gv.demo.GvHtmlLabelDemo;
import cdc.gv.demo.GvHtmlLabelDemo1;
import cdc.gv.demo.GvWriterDemo;
import cdc.gv.demo.GvWriterLabelDemo;
import cdc.gv.demo.GvX11ColorsDemo;

class DemoTest {
    @Test
    void testGvBrewerColorsDemo() {
        GvBrewerColorsDemo.main(null);
        assertTrue(true);
    }

    @Test
    void testGvEdgesDemo() throws IOException {
        GvEdgesDemo.main(null);
        assertTrue(true);
    }

    @Test
    void testGvHtmlLabelDemo() throws IOException {
        GvHtmlLabelDemo.main(null);
        assertTrue(true);
    }

    @Test
    void testGvHtmlLabelDemo1() throws IOException {
        GvHtmlLabelDemo1.main(null);
        assertTrue(true);
    }

    @Test
    void testGvWriterDemo() throws IOException {
        GvWriterDemo.main(null);
        assertTrue(true);
    }

    @Test
    void testGvWriterLabelDemo() throws IOException {
        GvWriterLabelDemo.main(null);
        assertTrue(true);
    }

    @Test
    void testGvX11ColorsDemo() throws IOException {
        GvX11ColorsDemo.main(null);
        assertTrue(true);
    }
}