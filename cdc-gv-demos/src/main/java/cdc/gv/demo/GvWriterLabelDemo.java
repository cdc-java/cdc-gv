package cdc.gv.demo;

import java.io.File;
import java.io.IOException;

import cdc.gv.GvWriter;
import cdc.gv.atts.GvEdgeAttributes;
import cdc.gv.atts.GvGraphAttributes;
import cdc.gv.atts.GvNodeAttributes;
import cdc.gv.tools.GvEngine;
import cdc.gv.tools.GvFormat;
import cdc.gv.tools.GvToAny;

public class GvWriterLabelDemo {
    private static String getLabel() {
        return "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n"
                + "abcdefghijklmnopqrstuvwxyz\n"
                + "0123456789\n"
                + "<>{}[]()&|~#_\"':;!?§%µ$£\\\\@+=+-*/\n"
                + "äëïöüÿàèìòùỳçãẽĩõũỹ";
    }

    public static void main(String[] args) throws IOException {
        final File outputDir = new File("target/output");
        outputDir.mkdirs();
        final File gvFile = new File(outputDir, GvWriterLabelDemo.class.getSimpleName() + ".gv");
        try (final GvWriter w = new GvWriter(gvFile)) {
            w.beginGraph("G1", true, new GvGraphAttributes().setLabel(getLabel()));
            w.addNode("N1", new GvNodeAttributes().setLabel(getLabel()));
            w.addEdge("N1", "N1", new GvEdgeAttributes().setLabel(getLabel()));
            w.endGraph();
            w.flush();
        }
        final GvToAny.MainArgs margs = new GvToAny.MainArgs();
        margs.input = gvFile;
        margs.outputDir = outputDir;
        margs.engine = GvEngine.DOT;
        margs.formats.add(GvFormat.PDF);
        GvToAny.execute(margs);
    }
}