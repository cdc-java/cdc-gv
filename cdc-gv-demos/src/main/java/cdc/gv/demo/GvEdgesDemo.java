package cdc.gv.demo;

import java.io.File;
import java.io.IOException;

import cdc.gv.GvWriter;
import cdc.gv.atts.GvArrowShape;
import cdc.gv.atts.GvArrowSide;
import cdc.gv.atts.GvArrowType;
import cdc.gv.atts.GvClusterAttributes;
import cdc.gv.atts.GvDirType;
import cdc.gv.atts.GvEdgeAttributes;
import cdc.gv.atts.GvEdgeStyle;
import cdc.gv.atts.GvGraphAttributes;
import cdc.gv.atts.GvLabelLoc;
import cdc.gv.atts.GvNodeAttributes;
import cdc.gv.atts.GvNodeShape;
import cdc.gv.atts.GvNodeStyle;
import cdc.gv.atts.GvPrimitiveArrowType;
import cdc.gv.atts.GvRankDir;
import cdc.gv.colors.GvX11Colors;
import cdc.gv.tools.GvEngine;
import cdc.gv.tools.GvFormat;
import cdc.gv.tools.GvToAny;
import cdc.util.files.Files;

public final class GvEdgesDemo {
    private GvEdgesDemo() {
    }

    private static String getId(String prefix,
                                int index) {
        return prefix + index;
    }

    public static void main(String[] args) throws IOException {
        final String odir = "target";
        final String output = odir + "/" + GvEdgesDemo.class.getSimpleName() + ".gv";
        Files.mkdir(odir);
        final GvWriter writer = new GvWriter(output);
        try {
            writer.addComment("Beginning of graph");
            final GvGraphAttributes gatts = new GvGraphAttributes();
            gatts.setRatio(1.0).setMaximumSize(10.0, 10.0).setRankDir(GvRankDir.LR).setNodeSep(0.0);
            writer.beginGraph("test-edges", true, gatts);
            writer.println();

            {
                final String prefix = "style";
                final GvClusterAttributes sgatts = new GvClusterAttributes();
                sgatts.setLabel("Edge Styles").setFontSize(60.0);
                writer.beginCluster("styles", sgatts);
                int index = 0;
                for (final GvEdgeStyle style : GvEdgeStyle.values()) {
                    index++;
                    final GvClusterAttributes catts = new GvClusterAttributes();
                    catts.setLabel(style.name()).setLabelLoc(GvLabelLoc.BOTTOM).setMargin(0.2).setFontSize(12.0);
                    writer.beginCluster(getId(prefix, index), catts);
                    final GvNodeAttributes natts = new GvNodeAttributes()
                                                                         .setShape(GvNodeShape.BOX)
                                                                         .setStyle(GvNodeStyle.FILLED)
                                                                         .setLabel("")
                                                                         .setColor(GvX11Colors.BLACK)
                                                                         .setFillColor(GvX11Colors.GRAY);
                    writer.addNode(getId("SStyle", index), natts);
                    writer.addNode(getId("TStyle", index), natts);
                    final GvEdgeAttributes eatts = new GvEdgeAttributes();
                    eatts.setStyle(style).setColor(GvX11Colors.BLACK).setArrowHead(GvArrowType.NONE);
                    writer.addEdge(getId("SStyle", index), getId("TStyle", index), eatts);
                    writer.endCluster();
                }
                writer.endCluster();
            }

            {
                final String prefix = "arrows";
                final GvClusterAttributes sgatts = new GvClusterAttributes();
                sgatts.setLabel("Legacy Arrow Types").setFontSize(60.0);
                writer.beginCluster("arrows", sgatts);
                int index = 0;
                for (final GvArrowType type : GvArrowType.values()) {
                    index++;
                    final GvClusterAttributes catts = new GvClusterAttributes();
                    catts.setLabel(type.name()).setLabelLoc(GvLabelLoc.BOTTOM).setMargin(0.2).setFontSize(12.0);
                    writer.beginCluster(getId(prefix, index), catts);
                    final GvNodeAttributes natts = new GvNodeAttributes()
                                                                         .setShape(GvNodeShape.BOX)
                                                                         .setStyle(GvNodeStyle.FILLED)
                                                                         .setLabel("")
                                                                         .setColor(GvX11Colors.BLACK)
                                                                         .setFillColor(GvX11Colors.GRAY);
                    writer.addNode(getId("SType", index), natts);
                    writer.addNode(getId("TType", index), natts);
                    final GvEdgeAttributes eatts = new GvEdgeAttributes()
                                                                         .setStyle(GvEdgeStyle.SOLID)
                                                                         .setColor(GvX11Colors.BLACK)
                                                                         .setArrowHead(type)
                                                                         .setArrowTail(type)
                                                                         .setArrowSize(2.0)
                                                                         .setDir(GvDirType.BOTH);
                    writer.addEdge(getId("SType", index), getId("TType", index), eatts);
                    writer.endCluster();
                }
                writer.endCluster();
            }

            {
                final String prefix = "arrows";
                final GvClusterAttributes sgatts = new GvClusterAttributes();
                sgatts.setLabel("Advanced Arrow Types").setFontSize(60.0);
                writer.beginCluster("advanced-arrows", sgatts);
                int index = 0;
                final boolean[] modifiers = { false, true };
                final GvArrowSide[] sides = { null, GvArrowSide.L, GvArrowSide.R };
                for (final GvPrimitiveArrowType type : GvPrimitiveArrowType.values()) {
                    final GvClusterAttributes catts = new GvClusterAttributes()
                                                                               .setLabel(type.name())
                                                                               .setLabelLoc(GvLabelLoc.BOTTOM)
                                                                               .setMargin(0.2)
                                                                               .setFontSize(12.0);
                    writer.beginCluster(getId(prefix, index), catts);
                    for (final boolean modifier : modifiers) {
                        if (!modifier || type.supportsModifier()) {
                            for (final GvArrowSide side : sides) {
                                if (side == null || type.supportsSide()) {
                                    index++;
                                    final GvNodeAttributes natts = new GvNodeAttributes()
                                                                                         .setShape(GvNodeShape.BOX)
                                                                                         .setStyle(GvNodeStyle.FILLED)
                                                                                         .setLabel("")
                                                                                         .setColor(GvX11Colors.BLACK)
                                                                                         .setFillColor(GvX11Colors.GRAY);
                                    writer.addNode(getId("SAType", index), natts);
                                    writer.addNode(getId("TAType", index), natts);
                                    final GvArrowShape shape = new GvArrowShape(type, modifier, side);
                                    final GvEdgeAttributes eatts = new GvEdgeAttributes()
                                                                                         .setStyle(GvEdgeStyle.SOLID)
                                                                                         .setColor(GvX11Colors.BLACK)
                                                                                         .setArrowHead(shape)
                                                                                         .setArrowTail(shape)
                                                                                         .setArrowSize(2.0)
                                                                                         .setDir(GvDirType.BOTH)
                                                                                         .setLabel(shape
                                                                                                        .encode());
                                    writer.addEdge(getId("SAType", index), getId("TAType", index), eatts);
                                }
                            }
                        }
                    }
                    writer.endCluster();
                }
                writer.endCluster();
            }

            writer.endGraph();
            writer.addComment("End of graph");
            writer.flush();
            writer.close();

            final GvToAny.MainArgs margs = new GvToAny.MainArgs();
            margs.setEnabled(GvToAny.MainArgs.Feature.VERBOSE, true);
            margs.input = new File(output);
            margs.outputDir = new File(odir);
            margs.engine = GvEngine.DOT;
            margs.formats.add(GvFormat.PDF);
            GvToAny.execute(margs);
            System.out.println("Done");

        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}