package cdc.gv.demo;

import cdc.gv.labels.GvHtmlLabel;
import cdc.gv.labels.GvTextModifier;

public final class GvHtmlLabelDemo1 {
    private GvHtmlLabelDemo1() {
    }

    public static void main(String[] args) {
        final GvHtmlLabel label = new GvHtmlLabel();
        System.out.println("label:" + label);

        label.addLineBreak(null);
        label.addText("Hello");
        label.beginTextModifier(GvTextModifier.BOLD);
        label.addText("<World>");
        label.endTextModifier();
        label.beginTextModifier(GvTextModifier.BOLD);
        label.beginTextModifier(GvTextModifier.ITALIC);
        label.addText("How are you?");
        label.endTextModifier();
        label.endTextModifier();

        System.out.println("label:" + label);

        label.clear();
        System.out.println("label:" + label);
        label.beginFont("RED", null, -1);
        label.addText("Hello");
        label.beginTextModifier(GvTextModifier.BOLD);
        label.addText("Hello");
        label.endTextModifier();
        label.endFont();
        label.beginFont("BLUE", null, -1);
        label.addText("Hello");
        label.beginTextModifier(GvTextModifier.BOLD);
        label.addText("Hello");
        label.endTextModifier();
        label.endFont();
        System.out.println("label:" + label);

        label.clear();
        label.beginTable(null);
        label.endTable();
        System.out.println("label:" + label);

        label.clear();
        label.beginFont("RED", null, -1);
        label.beginTable(null);
        label.beginRow();
        label.endRow();
        label.beginRow();
        label.beginCell(null);
        label.addText("Hello");
        label.endCell();
        label.endRow();
        label.endTable();
        label.endFont();
        System.out.println("label:" + label);

    }
}